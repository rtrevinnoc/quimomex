<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />
  <link type="text/css" rel="stylesheet" href="assets/css/jquery.fancybox.css" />
  <link type="text/css" rel="stylesheet" href="assets/css/slick.css" />
  <link type="text/css" rel="stylesheet" href="assets/css/dropkick.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63501207-1', 'auto');
    ga('send', 'pageview');
  </script>

</head>
<body>

	<?php include('header.php'); ?>

	<section id="home-stage">

    <div id="slider">

      <div class="img-bg" data-bg="assets/img/stages/ecox.jpg"><article><div class="col10 off1 sm-col12">

        <h1 class="bco"><img src="assets/img/stages/ecox.png" style="
    margin-top: -10%;
" ></h1>

<!--       
      </div></article></div>

      <div class="img-bg" data-bg="assets/img/stages/ecoo.jpg"><article><div class="col10 off1 sm-col12">

        <h1 class="bco"><img src="assets/img/stages/ecoo.png" style="
    margin-top: -10%;
" ></h1>
-->
       
      </div></article></div>

      <div class="img-bg" data-bg="assets/img/stages/excelencia-ambiental2.jpg"><article><div class="col10 off1 sm-col12">

        <h1 class="bco">SOMOS EXCELENCIA AMBIENTAL</h1>

        <p class="h2 bco"><em>La eficiencia de nuestros procesos asegura el bienestar y la conservación del medio ambiente</em> </p>
      </div></article></div>

      <div class="img-bg" data-bg="assets/img/stages/socio-comercial2.jpg"><article><div class="col10 off1 sm-col12">

        <h1 class="bco">SIEMPRE CERCA DE TI</h1>

        <p class="h2 bco"><em>Contamos con la red más grande de distribuidores de todo México </em> </p>
      </div></article></div>

      <div class="img-bg" data-bg="assets/img/stages/dist.jpg"><article><div class="col10 off1 sm-col12">

        <h1 class="bco">TU MEJOR SOCIO COMERCIAL</h1>

        <p class="h2 bco"><em>Nuestro portafolio de productos da solución a tus necesidades presentes y futuras de manera responsable </em> </p>

      </div></article></div>

    </div>

    <div id="bloques"><article>

      <div class="row"><div class="tbl">

        <div class="col4"><div class="home-block xs-bgverde"><div class="cont">
          <a href="mercados.php" class="btn full bgvd">MERCADOS</a>
          <hr>
          <p>Somos el proveedor de servicio completo para la industria nacional e internacional.</p>
        </div></div></div>

        <div class="col4"><div class="home-block xs-bgnaranja"><div class="cont">
          <a href="soluciones.php" class="btn full bg-naranja">SOLUCIONES</a>
          <hr>
          <p>Nuestra amplia oferta de soluciones cubre tus necesidades actuales y futuras.</p>
        </div></div></div>

        <div class="col4"><div class="home-block xs-bgazulmed"><div class="cont">
          <a href="productos.php?cat=RC" class="btn full bg-azulmed">PRODUCTOS</a>
          <hr>
          <p>Los gases refrigerantes Genetron® brindan alternativas para casi cualquier aplicación.</p>
        </div></div></div>

      </div></div>

    </article></div>

    <div class="trans-bco" style="background-color: #3a445e !important;"><article>

      <div class="row mt10">

        <div class="col4 hide-sm" align="center"><img class="block" src="assets/img/photos/tanquesz3.png" alt=""></div>
        <div class="col8 sm-col12 p20 clearfix">

          <div class="left"><p class="h2 azul ls40" style="color: #fff !important;">RED DE DISTRIBUIDORES</p></div>
          <div class="right busqueda"><form action="ubica.php" method="post">
            <!-- <input id="edo" type="text" placeholder="Buscar por Estado"> -->
            <!-- <select name="edo" class="drop">
              <option value="AGS">Aguascalientes</option>
              <option value="BC">Baja California</option>
              <option value="BCS">Baja California Sur</option>
              <option value="CAMP">Campeche</option>
              <option value="CHIS">Chiapas</option>
              <option value="CHIH">Chihuahua</option>
              <option value="CDMX">Ciudad de México</option>
              <option value="COAH">Coahuila</option>
              <option value="COL">Colima</option>
              <option value="DGO">Durango</option>
              <option value="GTO">Guanajuato</option>
              <option value="GRO">Guerrero</option>
              <option value="HGO">Hidalgo</option>
              <option value="JAL">Jalisco</option>
              <option value="MEX">Estado de México</option>
              <option value="MICH">Michoacán</option>
              <option value="MOR">Morelos</option>
              <option value="NAY">Nayarit</option>
              <option selected value="NL">Nuevo León</option>
              <option value="OAX">Oaxaca</option>
              <option value="PUE">Puebla</option>
              <option value="QRO">Querétaro</option>
              <option value="QR">Quintana Roo</option>
              <option value="SLP">San Luis Potosí</option>
              <option value="SIN">Sinaloa</option>
              <option value="SON">Sonora</option>
              <option value="TAB">Tabasco</option>
              <option value="TAMPS">Tamaulipas</option>
              <option value="TLAX">Tlaxcala</option>
              <option value="VER">Veracruz</option>
              <option value="YUC">Yucatán</option>
              <option value="ZAC">Zacatecas</option>
            </select> -->
          
          <div class="left"><p class="h2 azul ls40" style="color: #fff !important;"><a href="ubica.php"><span style="color:white !important;">¡CLICK AQUÍ!</span></a></p></div> 
           <button class="bg-gris" style="background-color: red !important;"><span class="icon icon-zoom"></span></button>
          </form></div>

        </div>

      </div>

    </article></div>

	</section>

  <section class="img-bg circ" data-bg="assets/img/stages/bloque-home.jpg"><article>

    <div class="va-p"><div class="va" align="center">
      <h1 class="bco mb30"><b>CUIDEMOS NUESTRO PLANETA</b></h1>
      <a href="http://www.youtube.com/embed/mEMwbtTpYrc?autoplay=1" class="fancybox fancybox.iframe btn bg-azulmed">VE EL VIDEO</a>
    </div></div>

  </article></section>

  <section class="bgf1 p60"><article><div class="tbl">

    <div class="col3 xs-mb20"><div class="prefoot" data-href="premios.php" >
      <div class="img-bg" data-bg="assets/img/bg/premios.jpg"><div class="va-p"><div class="va p10" align="center"><p class="h2 bco"><strong>PREMIOS</strong></p></div></div></div>
      <div class="p20">
        <p class="mb20">Algunos reconocimientos a nuestra labor</p>
        <div align="center"><a href="premios.php" class="btn bg-azul" target="_blank">LEER MÁS</a></div>
      </div>
    </div></div>

    <div class="col3 xs-mb20"><div class="prefoot" data-href="asesoria-tecnica.php" >
      <div class="img-bg" data-bg="assets/img/bg/asesoria.jpg"><div class="va-p"><div class="va p10" align="center"><p class="h2 bco"><strong>ASESORÍA</strong></p></div></div></div>
      <div class="p20">
        <p class="mb20">Solicita el apoyo de nuestros expertos</p>
        <div align="center"><a href="asesoria-tecnica.php" class="btn bg-azul" target="_blank">LEER MÁS</a></div>
      </div>
    </div></div>

    <div class="col3 xs-mb20"><div class="prefoot" data-href="https://blogquimobasicos.com/category/noticias-de-la-industria/">
      <div class="img-bg" data-bg="assets/img/bg/noticias.jpg"><div class="va-p"><div class="va p10" align="center"><p class="h2 bco"><strong>NOTICIAS</strong></p></div></div></div>
      <div class="p20">
        <p class="mb20">Consulta nuestros comunicados oficiales</p>
        <div align="center"><a href="https://blogquimobasicos.com/category/noticias-de-la-industria/" class="btn bg-azul" target="_blank">LEER MÁS</a></div>
      </div>
    </div></div>

    <div class="col3"><div class="prefoot" data-href="unete.php">
      <div class="img-bg" data-bg="assets/img/bg/empleos.jpg"><div class="va-p"><div class="va p10" align="center"><p class="h2 bco"><strong>EMPLEOS</strong></p></div></div></div>
      <div class="p20">
        <p class="mb20">Oportunidades laborales en Quimobásicos</p>
        <div align="center"><a href="unete.php" class="btn bg-azul" target="_blank">LEER MÁS</a></div>
      </div>
    </div></div>

  </div></article></section>

	<?php include('footer.php'); ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="assets/js/site.js"></script>
	<script type="text/javascript" src="assets/js/home.js"></script>

</body>
</html>
