<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>Trabaja con nosotros | Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63501207-1', 'auto');
    ga('send', 'pageview');
  </script>

</head>
<body>

  <?php include('header.php'); ?>

  <section class="grad pt140" data-bg="assets/img/stages/jobz2.png">

    <article>

      <div class="row mb50"><div class="col10 off1 bco">

        <h1>TRABAJA CON NOSOTROS</h1>
        <a href="javascript:history.back();"><span class="icon icon-atras h1"></span> <span class="h2 ml10">Volver</span></a>

      </div></div>

    </article>

  </section>

  <section>

    <article class="p60">

      <p class="h3"><strong>Intégrate a una empresa de clase mundial</strong></p>
      <p class="h3">En Quimobásicos nos interesa escuchar acerca de las habilidades, talentos, perspectivas y calidad humana que puedes aportar a nuestra organización.</p>
      <p class="h3"></p>
      <p class="h3">Te invitamos a explorar nuestras oportunidades laborales y a enviar tu información curricular si coinciden con tus habilidades, experiencias, metas personales y objetivos profesionales.</p>

      <table class="careers mt50">
        <thead>
          <tr>
            <th>PUESTO</th>
            <th>ZONA GEOGRÁFICA</th>
            <th>DETALLE</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td data-th="Puesto" colspan="3" align="center">DE MOMENTO NO HAY VACANTES DISPONIBLES</td>
          </tr>
        <!-- EGC  <tr>
            <td data-th="Puesto">EJECUTIVO COMERCIAL</td>
            <td data-th="Zona GeogrÃ¡fica">Monterrey, Nuevo LeÃ³n</td>
            <td data-th="Detalle"><a href="vacante.php" class="btn lg bg-azul">VER MS</a></td>
          </tr> -->
        </tbody>      
        </table>
        <!--
        <p class="h3" >En este momento en Quimobásicos <strong>no contamos con vacantes disponibles</strong>.</p>
        <p class="h3" >No obstante, debido a que la lista de oportunidades laborales var&iacutea frecuentemente, te animamos a que env&iacutees tu información curricular o solicitud de empleo elaborada al área de relaciones industriales para ser añadido a nuestra base de datos y facilitar el poder considerarte en oportunidades futuras.</p>
        -->
    </article>

  </section>

  <section class="bg-azulosc"><article class="p40"><div align="center">
    <span class="bco h2 ls40">Adicionalmente te recordamos que puedes enviar tu CV o aplicación de empleo al correo electrónico: <a href="mailto:raul.acosta@cydsa.com" class="bco italic">raul.acosta@cydsa.com</a></span>
  </div></article></section>

  <?php include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>

</body>
</html>
