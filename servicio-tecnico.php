<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>Servicio Técnico | Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />
  <link type="text/css" rel="stylesheet" href="assets/css/dropkick.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>

</head>
<body>

  <?php include('header.php'); ?>

  <section class="stage grad" data-bg="assets/img/stages/unidades.jpg">
    <article><div class="tbl"><div class="vab">
      <h1 class="bb-verde">SERVICIO TÉCNICO</h1>
    </div></div></article>
  </section>

  <div class="clearfix hide-xs tabs t-mercados">
    <a href="#destruccion" class="col2">DESTRUCCIÓN <br>DE GASES</a>
    <a href="#oems" class="col2 lh100">FABRICANTES DE EQUIPO ORIGINAL (OEMs)</a>
    <a href="#automotrices" class="col2">FABRICANTES <br>AUTOMOTRICES</a>
    <a href="#distribuidores" class="col2 lh40">DISTRIBUIDORES</a>
    <a href="#propelentes" class="col2 lh40">PROPELENTES</a>
    <a href="#espumantes" class="col2 lh40">ESPUMANTES</a>
  </div>
  <div class="show-xs xs-menu"><select class="drop">
    <option value="destruccion">DESTRUCCIÓN DE GASES</option>
    <option value="oems">FABRICANTES DE EQUIPO ORIGINAL (OEMs)</option>
    <option value="automotrices">FABRICANTES AUTOMOTRICES</option>
    <option value="distribuidores">DISTRIBUIDORES</option>
    <option value="propelentes">PROPELENTES</option>
    <option value="espumantes">ESPUMANTES</option>
  </select></div>

  <section id="destruccion" class="bgf1">
    <article class="bb-lima p60">

      <div class="mb30">
        <h2 class="verdeosc">DESTRUCCIÓN DE GASES</h2>
      </div>

      <div class="clearfix tabs t-verde">
        <a href="#por-que" class="col3 act">¿POR QUÉ DESTRUIRLOS?</a>
        <a href="#gases" class="col3">¿QUÉ DESTRUIMOS?</a>
        <a href="#tecnologia" class="col3">LA TECNOLOGÍA</a>
        <a href="#beneficios" class="col3">BENEFICIOS</a>
      </div>

      <div id="por-que" class="tab-cont">
        <div class="row mt10">
          <div class="col6">
            <p class="h4 mb20">Desde que en 1987 México y otros países comprometidos con la protección al ozono firmaron el Tratado de Montreal, surgió la necesidad de métodos para eliminar los gases que dañan el medio ambiente y el compromiso en generar gases de nueva generación que cumplan las mismas funciones sin efectos negativos.</p>

            <p class="h4">Quimobásicos, en línea con los principios de innovación y cuidado al medio ambiente de Grupo Cydsa, además de ofrecer dentro de su amplio portafolio las mejores alternativas en gases con bajo Potencial de Calentamiento Global (GWP), ha puesto en funcionamiento la PRIMERA y ÚNICA planta de destrucción de Sustancias Agotadoras del Ozono (SAOs) de México y América Latina.</p>
          </div>
          <div class="col5 off1 mt20">
            <div class="mb40" align="center"><img src="assets/img/photos/industrias.png" alt=""></div>
          </div>
        </div>
      </div>

      <div id="gases" class="tab-cont">
        <div class="row mt10">
          <div class="col6 xs-mb20">
            <p class="h4 mb20">En la planta de destrucción de Quimobásicos es la primera y única en su tipo en América Latina.</p>
            <p class="h4">En ella damos el servicio de destrucción de las llamadas Sustancias Agotadoras del Ozono, consistentes comúnmente de residuos de CFCs, HCFCs y HFCs como son gases refrigerantes fluorocarbonados recuperados, espumantes,  y propelentes varios.</p>
          </div>
          <div class="col5 sm-col6">
            <div class="quote"><div class="cont" align="center">
              <blockquote class="mb20">En Quimobásicos construimos la primera planta destructora de gases CFCs, HCFCs y HFCs de México.</blockquote>
            </div></div>
          </div>
        </div>
      </div>

      <div id="tecnologia" class="tab-cont">
        <p class="h4 mb20">La tecnología de la planta destructora de Quimobásicos se denomina ‘Destrucción de Plasma’, pues incluye la utilización de tecnología de arco de plasma argón; esta tecnología ha sido aprobada por el Grupo de Evaluación Tecnológica y Económica del Protocolo de Montreal (TEAP, por sus siglas en inglés) y cumple con parámetros de rendimiento así como con requisitos de emisiones al ambiente establecidos en el Protocolo de Montreal y con las regulaciones nacionales aplicables.</p>
        <p class="h4">Las instalaciones donde se lleva a cabo la destrucción están ubicadas en la planta de Quimobásicos en la ciudad de Monterrey, México.</p>
      </div>

      <div id="beneficios" class="tab-cont">
        <p class="h4 mb20">En el equipo de destrucción por plasma los gases se transforman en conductores eléctricos cuando sobrepasan los 4000 grados centígrados, con esta altísima tempratura añadida a la alta densidad de energía y cuidando un control adecuado del proceso nos permite poder descomponer cualquier molécula orgánica inyectada en el plasma a sus iones y átomos elementales debido a las altas temperaturas.</p>
        <p class="h4">La eficiencia de destrucción de la tecnología de arco de plasma es muy alta, alcanzando normalmente 99.995% de eficiencia.</p>
      </div>

    </article>
  </section>

  <section id="distribuidores">
    <article class="bb-verde p60">

      <div class="mb30">
        <h2 class="verdemed">DISTRIBUIDORES</h2>
      </div>

      <div class="mb30">
        <p class="h4 mb20">En Quimobásicos contamos con la mejor y más extensa red de distribuidores de México con más de 500 puntos de venta a lo largo de toda la República así como en Centro, Sudamérica y en el Caribe. Es gracias a esta vasta red de distribuidores que nuestros productos gozan del liderazgo, posicionamiento y presencia predominantes en el mercado de reposición nacional mexicano y de más allá de nuestras fronteras. </p>
        <p class="h4 mb20">Somos el proveedor de la línea más completa de productos fluorocarbonados envasados y a granel; incluyendo a los gases refrigerantes Hidroclorofluorocarbonos (HCFCs), Hidrofluorocarbonos (HFCs) así como las Hidrofluoroolefinas (HFOs) de última generación y con nula afectación al ozono.</p>
        <p class="h4 mb20">Nuestro compromiso de servicio al cliente gira en torno a la satisfacción de las necesidades presentes y futuras, asegurando la transición cómoda y sin inconvenientes hacia los sustitutos de más reciente generación. </p>
        <p class="h4">Además, a través de este amplio portafolio de productos es somos capaces de ofrecer las alternativas ecológicamente viables para la enorme variedad de aplicaciones de aire acondicionado, refrigeración, agentes espumantes, limpiadores de precisión, y aerosoles. </p>
      </div>

    </article>
  </section>

  <section id="oems" class="bgf1">
    <article class="bb-verde p60">

      <div class="mb30">
        <h2 class="verdemed">OEMS</h2>
      </div>

      <div class="mb30">
        <p class="h4 mb20">En el ramo de fabricantes de equipo original contamos con el suministro de gas refrigerante R-134a y R-22 a un segmento importante de compañías dedicadas a la manufactura de aires acondicionados, mini-splits, refrigeradores, calentadores y demás equipo doméstico para su comercialización a nivel nacional e internacional.</p>
        <p class="h4">Dentro de los diferenciadores que ofrecemos a nuestros clientes fabricantes de equipo original podemos mencionar: Seguridad en la cadena de suministros, apoyo con inventarios de Seguridad, servicio 24 horas así como el know-how y amplia experiencia de nuestro equipo técnico.</p>
      </div>

      <div class="row mb30"><div class="col6"><div class="bbverde"><h3><b>ALGUNOS DE NUESTROS CLIENTES:</b></h3></div></div></div>
      <div class="row">
        <div class="col3 xs-col6 xs-center xs-mb20"><img src="assets/img/icons/gm.png" alt=""></div>
        <div class="col3 xs-col6 xs-center xs-mb20"><img src="assets/img/icons/audi.png" alt=""></div>
        <div class="col3 xs-col6 xs-center"><img src="assets/img/icons/vw.png" alt=""></div>
        <div class="col3 xs-col6 xs-center"><img src="assets/img/icons/kia.png" alt=""></div>
      </div>


    </article>
  </section>

  <section id="automotrices">
    <article id="distribuidores" class="bb-lima p60">

      <div class="mb30">
        <h2 class="verdeosc">FABRICANTES AUTOMOTRICES</h2>
      </div>

      <p class="h4 mb20">En Quimobásicos® damos suministro a la Industria Automotriz en México con el refrigerante Genetron® 134a así como con el gas refrigerante de última generación HFO-1234yf el cual no afecta a la capa de ozono. </p>
      <p class="h4 mb20">Los refrigerantes de este mercado son para uso en automóviles, transporte de carga y autobuses, principalmente.</p>
      <p class="h4 mb20">Nuestros clientes en este segmento son fabricantes automotrices los cuales nos prefieren debido a que nuestros estándares garantizan:</p>
      <ul class="bull ml30">
        <li><strong>Seguridad en la cadena de suministros</strong> mediante la operación de estaciones de descarga en carros de ferrocarril, isotanques e inventarios.</li>
        <li><strong>Inventarios de seguridad</strong> para cada uno de nuestros valiosos clientes fabricantes.</li>
        <li>Capacidad de <strong>servicio 24/7</strong> en procedimientos de seguridad, manejo de refrigerante, procesamiento de equipo y labores relacionadas.</li>
        <li><strong>Know-how y expertise</strong>, los cuales garantizamos se aplicarán al proyecto de todos y cada uno de nuestros clientes fabricantes automotrices.</li>
      </ul>

    </article>
  </section>

  <section id="propelentes" class="bgf1">
    <article id="espumantes" class="p60">

      <div class="mb30">
        <h2 class="verdemed">PROPELENTES Y ESPUMANTES</h2>
      </div>

      <p class="h4">La industria del aerosol ha experimentado cambios en la formulación de sus productos, que hasta hace algunos años llevaban como gas propelente algún CFC o mezcla de CFCs. </p>
      <p class="h4">Hoy en día el más utilizado es HCFC 22, esto para aplicaciones en la fabricación de aromatizantes, cosméticos, insecticidas y pinturas en aerosol principalmente.</p>
      <p class="h4">Quimobásicos tiene un importante papel en la transición de la industria de agentes espumantes. Los productos alternativos disponibles actualmente para los mercados de la industria de la construcción, transportación y aparatos electrodomésticos incluyen los siguientes productos: 141b, 142b, 22, 134a y mezclas de HCFC-142b/CHFC-22.</p>
      <p class="h4">Sus propiedades superiores de aislamiento térmico y su compatibilidad con otros materiales, hacen de ellos agentes espumantes ideales para producir gran variedad de espumas de poliuretano y estireno de alto valor.</p>

    </article>
  </section>

  <?php include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>

  <script type="text/javascript" src="assets/js/dropkick.js"></script>
  <script>
    $(document).ready(function(){
      $('.tabs.t-verde a').click(function(){ $('.tabs a').removeClass('act'); $(this).addClass('act'); $('.tab-cont').hide(); $($(this).attr('href')).show(); return false; });
      $('.t-mercados a').click(function(){ $('html, body').stop().animate({ scrollTop: $($(this).attr('href')).offset().top }, 1000); return false; });
      $('.tab-cont:eq(0)').show();

      $('.drop').dropkick({ mobile: true, change: function(){
        $('html, body').stop().animate({ scrollTop: $('#' + this.value).offset().top }, 1000); 
      }});
    });
  </script>

</body>
</html>
