<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>Asesoría Técnica | Quimobásicos</title>
  
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63501207-1', 'auto');
    ga('send', 'pageview');
  </script>

</head>
<body>

  <?php include('header.php'); ?>

  <section class="stage grad" data-bg="assets/img/stages/asesoria.jpg">
    <article><div class="tbl"><div class="vab">
      <h1 class="bb-verde">ASESORÍA TÉCNICA</h1>
    </div></div></article>
  </section>

  <section>
    <article class="p60">

      <p class="h4 mb20">Para asegurar el Soporte Técnico adecuado requerido por el mercado de la refrigeración y el aire acondicionado, en Quimobásicos contamos con un Departamento de Servicio Técnico integrado por profesionales en el medio con entrenamiento a nivel nacional e internacional y con experiencia de varias décadas aplicadas a la industria del frío, el confort y la conservación. </p>
      <p class="h4 mb20">Para solicitar información o asistencia técnica puedes comunicarte con nuestros especialistas dentro de horarios de oficina a los números telefónicos siguientes:</p>

      <div class="row"><div class="col8 off2 sm-col12"><ul class="list">
        <li>
          <div class="row">
            <div class="col6 sm-col6">
              <p class="h2 azulmed ls40"><b>SIN COSTO NACIONAL</b></p>
            </div>
            <div class="col6 sm-col6">
              <p class="h3 azulosc">01 800 830-3300</p>
            </div>
          </div>
        </li>
        <li>
          <div class="row">
            <div class="col6 sm-col6">
              <p class="h2 azulmed ls40"><b>DESDE MONTERREY</b></p>
            </div>
            <div class="col6 sm-col6">
              <p class="h3 azulosc">(81) 8305-4622</p>
            </div>
          </div>
        </li>
      </ul></div></div>

    </article>
  </section>

  <section class="bgf1"><article class="p40"><div align="center">
    
    <div class="row mb30"><div class="col8 off2 sm-col12">
      <p class="h4">Adicionalmente te recordamos que estamos disponibles vía correo electrónico con un Asesor Técnico a tu servicio en el siguiente correo electrónico:</p>
      <a href="mailto:Asesor.quimobasicos@cydsa.com" class="h2 azul"><strong>Asesor.quimobasicos@cydsa.com</strong></a>
    </div></div>
    <p class="f30"><b>¡Esperamos con gusto poder ayudarte!</b></p>

  </div></article></section>

  <?php include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>

</body>
</html>
