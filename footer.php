<footer>

    <div class="bg-azulmed"><div class="container"><div class="tbl">

      <div class="col">
        <h4>NOSOTROS</h4>
        <ul>
          <li><a href="nosotros.php">La Empresa</a></li>
          <li><a href="nosotros.php">Historia</a></li>
          <li><a href="nosotros.php?s=mision">Misión, Visión y Valores</a></li>
          <!--<li><a href="responsabilidad-social.php">Responsabilidad Social</a></li>-->
          <li><a href="aviso.php">Aviso de Privacidad</a></li>
          <li><a href="unete.php">Trabaja con Nosotros</a></li>
        </ul>
      </div>

      <div class="col">
        <h4>SOLUCIONES</h4>
        <ul>
          <li><a href="mercados.php">Mercados</a></li>
          <li><a href="soluciones.php">Soluciones</a></li>
          <li><a href="productos.php">Productos</a></li>
        </ul>
      </div>

      <div class="col">
        <h4>DESCARGABLES </h4>
        <ul>
          <!-- <li><a href="#">Catálogo</a></li> -->
          <li><a href="descargables.php">Manual de Identidad</a></li>
          <li><a href="descargables.php?s=SW">Software Genetron®</a></li>
          <li><a href="descargables.php?s=TP">Tabla Presión vs. Temperatura</a></li>
          <li><a href="descargables.php?s=HS">Hojas de Seguridad</a></li>
          <li><a href="descargables.php?s=FT">Fichas Técnicas</a></li>
          <li><a href="descargables.php?s=PB">Publicidad</a></li>
        </ul>
      </div>

      <div class="col">
        <h4>NOTICIAS</h4>
        <ul>
          <li><a href="https://blogquimobasicos.com/category/eventos-quimobasicos/" target="_blank">Eventos</a></li>
          <li><a href="https://blogquimobasicos.com/" target="_blank">Notas de prensa</a></li>
        </ul>
      </div>

      <div class="col">
        <h4>MI CUENTA</h4>
        <ul>
          <li><a href="sppi.php">Clientes</a></li>
          <li><a href="facturacion.php">Proveedores</a></li>
        </ul>
      </div>

    </div></div></div>

    <div class="bg-azul"><div class="container">

      <h5 class="bco mb40"><b>UBICACIONES</b></h5>

      <div class="row">

        <div class="col6 sm-col12 xs-col6 sm-mb20"><div class="row">
          <div class="col5">
            <h4>MONTERREY, N.L.</h4>
            <p class="bco">
              Av. Adolfo Ruiz Cortines <br>
              No. 2333 Poniente <br>
              Col. Pedro Lozano <br>
              Monterrey, N.L. <br>
              México. CP 64420
            </p>
          </div>
          <div class="col7"><div class="mapa">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1797.4898434631775!2d-100.33590231865206!3d25.70509657242168!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xf3efadbc7f04f3f7!2sQuimob%C3%A1sicos%2C+S.A.+De+C.V.!5e0!3m2!1ses-419!2smx!4v1446836530371" width="100%" height="145" frameborder="0" style="border:0" allowfullscreen></iframe>
            <a href="https://maps.google.com/maps?ll=25.705498,-100.334838&z=16&t=m&hl=en-US&gl=MX&mapclient=embed&cid=17577458895746823159" target="_blank"><b>Ver en Google Maps</b></a>
          </div></div>
        </div></div>

        <div class="col6 sm-col12 xs-col6"><div class="row">
          <div class="col5">
            <h4>CIUDAD DE MÉXICO</h4>
            <p class="bco divider">
              Av. Insurgentes Sur <br>
              No. 800 Piso 20 <br>
              Col. Del Valle <br>
              Delegación Benito Juárez. <br>
              Ciudad de México. CP 03100
            </p>
          </div>
          <div class="col7"><div class="mapa">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d470.4390888038691!2d-99.17349390287684!3d19.39024114694272!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x0c0f4041a1297683!2sTORRE+LOGAR!5e0!3m2!1ses-419!2smx!4v1446837245470" width="100%" height="145" frameborder="0" style="border:0" allowfullscreen></iframe>
            <a href="https://maps.google.com/maps?ll=19.390297,-99.173138&z=18&t=m&hl=en-US&gl=MX&mapclient=embed&cid=868983903726696067" target="_blank"><b>Ver en Google Maps</b></a>
          </div></div>
        </div></div>

      </div>

    </div></div>

    <div class="container xs-center">
      <div class="row mb20">

        <div class="col8 md-col7 sm-col6 xs-mb20">
          <h4 class="mb10">NUESTRAS REDES SOCIALES</h4>
          <div class="social">
            <a href="https://www.facebook.com/Quimobasicos" target="_blank" class="icon icon-fb"></a>
            <a href="https://twitter.com/Quimobasicos" target="_blank" class="icon icon-tw"></a>
            <a href="https://www.linkedin.com/company/quimob%C3%A1sicos" target="_blank" class="icon icon-in"></a>
            <a href="https://www.youtube.com/user/tvquimobasicos" target="_blank" class="icon icon-yt"></a>
            <a href="https://blogquimobasicos.com/" target="_blank" class="blog"><img src="assets/img/icons/blog.png" alt="BLOG"></a>
          </div>
        </div>

        <div class="col4 md-col5 sm-col6 xs-mb20">
          <h4 class="mb10">CERTIFICACIONES</h4>
          <a href="http://cydsa.com.mx/" target="_blank"><img class="inline mr10" src="assets/img/icons/logo-cydsa.png" alt="Cydsa"></a>
          <a href="premios.php"><img class="inline mr10" src="assets/img/icons/logo-excelencia-ambiental.jpg" alt="Excelencia Ambiental"></a>
          <a href="premios.php"><img class="inline mr10" src="assets/img/icons/logo-bureau-veritas.jpg" alt="Bureau Veritas"></a>
          <a href="premios.php"><img class="inline mr10" src="assets/img/icons/logo-industria-limpia.png"  alt="Industria Limpia"></a>
        </div>

      </div>
    </div>
    
    <div class="container xs-center">
         <div class="col8 md-col7 sm-col6 xs-mb20">
            <div ><h6 class="azulosc"><em>Copyright © 2018 Quimobasicos S.A. de C.V.</em></h6>
            </div>
         </div>
         <div class="col4 md-col5 sm-col6 xs-mb20">
            <div align="right" ><h4 class="azul"><em><li><a href="http://www.quimobasicosdelnoreste.com.mx/" target="_blank">Información del régimen opcional para grupos de sociedades del impuesto sobre la renta</a></li></em></h4>
            </div>
         </div>
    </div>


  </footer>

