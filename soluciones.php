<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>Soluciones | Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />
  <link type="text/css" rel="stylesheet" href="assets/css/dropkick.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63501207-1', 'auto');
    ga('send', 'pageview');
  </script>

</head>
<body>

  <?php include('header.php'); ?>

  <section class="stage grad" data-bg="assets/img/stages/soluciones.jpg">
    <article><div class="tbl"><div class="vab">
      <h1 class="bb-azul">SOLUCIONES</h1>
    </div></div></article>
  </section>

  <div class="clearfix hide-xs tabs t-mercados">
    <a href="#industrial" class="col2">REFRIGERACIÓN <br>INDUSTRIAL</a>
    <a href="#comercial" class="col2">REFRIGERACIÓN <br>COMERCIAL</a>
    <a href="#aca" class="col2 lh100">AIRE ACONDICIONADO <br> AUTOMOTRIZ</a>
    <a href="#acr" class="col2 lh100">AIRE ACONDICIONADO <br> RESIDENCIAL</a>
    <a href="#acc" class="col2 lh100">AIRE ACONDICIONADO <br> COMERCIAL</a>
    <a href="#propelentes" class="col2">PROPELENTES <br> Y ESPUMANTES</a>
  </div>
  <div class="show-xs xs-menu"><select class="drop">
    <option value="industrial">REFRIGERACIÓN INDUSTRIAL</option>
    <option value="comercial">REFRIGERACIÓN COMERCIAL</option>
    <option value="aca">AIRE ACONDICIONADO  AUTOMOTRIZ</option>
    <option value="acr">AIRE ACONDICIONADO  RESIDENCIAL</option>
    <option value="acc">AIRE ACONDICIONADO  COMERCIAL</option>
    <option value="propelentes">PROPELENTES  Y ESPUMANTES</option>
  </select></div>


    <section id="aca">
    <article class="bb-azul p60">

      <div class="row">
        <div class="col7 sm-mb20">
          <div class="mb30">
            <h2 class="azul">AIRE ACONDICIONADO AUTOMOTRIZ</h2>
          </div>
          <p class="h4 mb20">En Quimobásicos contamos con suministro a la industria automotriz mexicana, mediante productos como el Genetron® 134a (R-134a) y el nuevo gas refrigerante HFO Solstice® 1234yf, el cual no afecta a la capa de ozono y cuenta con una nula afectación al calentamiento global y la capa superior de ozono. </p>
          <p class="h4 mb20">Los refrigerantes en este mercado son para uso en automóviles, transporte de carga y autobuses, entre otros.</p>
          <a href="productos.php?cat=ACA" target="_blank" class="btn bg-azulmed">VER PRODUCTOS</a>
        </div>
        <div class="col5" align="center"><img src="assets/img/photos/camioneta6.png" alt=""></div>
      </div>


    </article>
  </section>


    <section id="comercial" class="bgf1">
    <article class="bb-azul p60">

      <div class="row">
        <div class="col7 sm-mb20">
          <div class="mb30">
            <h2 class="azulmed">REFRIGERACIÓN COMERCIAL</h2>
          </div>
          <p class="h4 mb20">En Quimobásicos contamos también con liderazgo en el mercado de la refrigeración comercial a través de los gases refrigerantes Genetron® 22, Genetron® Performax LT (R-407F), Genetron® AZ 50 (R-507), Genetron® 404A y la más reciente adición a nuestro vasto portafolio, el HFO Solstice® N40. </p>
          <p class="h4 mb20">En este segmento, los principales clientes usuarios de nuestros productos son cadenas de supermercados y tiendas de conveniencia.</p>
          <a href="productos.php?cat=RC" target="_blank" class="btn bg-azulmed">VER PRODUCTOS</a>
        </div>
        <div class="col5" align="center"><img src="assets/img/photos/ref3.jpg" alt=""></div>
      </div>


    </article>
  </section>


    <section id="acr" class="bgf1">
    <article class="bb-azul p60">

      <div class="row">
        <div class="col7 sm-mb20">
          <div class="mb30">
            <h2 class="azul">AIRE ACONDICIONADO RESIDENCIAL</h2>
          </div>
          <p class="h4 mb20">Dentro del segmento de aire acondicionados domésticos ofrecemos una amplia gama de productos con aplicaciones en gran variedad de equipos que van desde los clásicos de ventana, los populares mini-splits ya sean de pared o de techo, los equipos portátiles e incluso climas centrales.</p>
          <p class="h4 mb20">En aire acondicionado residencial el tema principal a últimas fechas es acerca de la sustitución programada del refrigerante R-22 en el año 2030, en este sentido Quimobásicos cuenta con una amplia gama de refrigerantes para su reemplazo, ya sea para equipos existentes que contienen este producto o refrigerantes para equipos nuevos con alta eficiencia. Los productos Genetron® 422D y Genetron® 407C se recomiendan para reemplazo del R-22 en equipos existentes mientras que el Genetron® AZ20® (R-410A) es uno de los sustitutos más populares para el R-22 en equipos nuevos.</p>
          <a href="productos.php?cat=ACR" target="_blank" class="btn bg-azulmed">VER PRODUCTOS</a>
        </div>
        <div class="col5"><img src="assets/img/photos/residenss.png" alt=""></div>
      </div>


    </article>
  </section>



  <section id="acc">
    <article class="bb-azul p60">

      <div class="row">
        <div class="col7 sm-mb20">
          <div class="mb30">
            <h2 class="azul">AIRE ACONDICIONADO COMERCIAL</h2>
          </div>
          <p class="h4 mb20">En Quimobasicos contamos con la solución para los equipos de A/C en comercios, locales comerciales, grandes establecimientos y tiendas departamentales donde se requiere un confort extra para los clientes, desde el R-22 hasta la última generación de refrigerantes con bajo impacto ambiental.”</p>
          <a href="productos.php?cat=ACC" target="_blank" class="btn bg-azulmed">VER PRODUCTOS</a>
        </div>
        <div class="col5"><img src="assets/img/photos/ofis.jpg" alt=""></div>
      </div>

    </article>
  </section>



  <section id="industrial">
    <article class="bb-azul p60">

      <div class="mb30">
        <h2 class="azulmed">REFRIGERACIÓN INDUSTRIAL</h2>
      </div>

      <div class="row mb30">
        <div class="col7 xs-mb20">
          <p class="h4 mb20">Los productos de Quimobásicos son esenciales en diferentes procesos industriales tales como: refrigeración, aire acondicionado, aislamiento térmico y la fabricación de plásticos fluorados, la industria de los aerosoles, en variedad de usos medicinales y también como agentes de limpieza de componentes electrónicos.</p>
          <p class="h4 mb20">Nuestro enfoque en la calidad y la excelencia nos permite garantizar niveles superiores de calidad y prestaciones a las de cualquier marca competidora tanto nacional como extranjera.</p>
          <a href="productos.php?cat=RI" target="_blank" class="btn bg-azulmed">VER PRODUCTOS</a>
        </div>
        <div class="col5" align="center"><img src="assets/img/photos/colin.jpg" alt=""></div>
      </div>

      <!-- <div class="row"><div class="col6 off3 sm-col12" align="center"><blockquote class="azulmed">- “Desde su instalación iPrism nos ha ofrecido la capacidad para ir afinando el sistema para los requerimientos de recursos de los empleados” .-</blockquote></div></div> -->

    </article>
  </section>







  

  <section id="propelentes" class="bgf1">
    <article class="bb-azul p60">

      <div class="mb30">
        <h2 class="azul">PROPELENTES Y ESPUMANTES</h2>
      </div>

      <div class="row mb30">
        <div class="col7 xs-mb20">
          <p class="h4 mb20">La industria del aerosol ha experimentado cambios en la formulación de sus productos, que hasta hace algunos años llevaban como gas propelente algún CFC o mezcla de CFCs. </p>
          <p class="h4 mb20">Actualmente solo la industria farmacéutica puede seguir utilizando propelentes basados en CFCs en Grado Farmacéutico, para la elaboración de inhaladores de dosis medidas, en el tratamiento de enfermedades respiratorias y pulmonares, lo cual se considera como un uso esencial en donde el empleo de hidrocarburos no es factible por su toxicidad y flamabilidad.</p>
          <p class="h4 mb20">Hoy en día el más utilizado es HCFC 22, esto para aplicaciones en la fabricación de aromatizantes, cosméticos, insecticidas y pinturas en aerosol principalmente.</p>
          <p class="h4 mb20">Quimobásicos tiene un importante papel en la transición de la industria de agentes espumantes. Los productos alternativos disponibles actualmente para los mercados de la industria de la construcción, transportación y aparatos electrodomésticos incluyen los siguientes productos: 141b, 142b, 22, 134a y mezclas de HCFC-142b/CHFC-22.</p>
          <p class="h4 mb20">Sus propiedades superiores de aislamiento térmico y su compatibilidad con otros materiales, hacen de ellos agentes espumantes ideales para producir gran variedad de espumas de poliuretano y estireno de alto valor.</p>
          <a href="productos.php?cat=RD" target="_blank" class="btn bg-azulmed">VER PRODUCTOS</a>
        </div>
        <div class="col5" align="center"><img src="assets/img/photos/propes.jpg" alt=""></div>
      </div>

    </article>
  </section>

  <?php include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>

  <script type="text/javascript" src="assets/js/dropkick.js"></script>
  <script>
    $(document).ready(function(){
      $('.t-mercados a').click(function(){ $('html, body').stop().animate({ scrollTop: $($(this).attr('href')).offset().top -100 }, 1000); return false; });

      $('.drop').dropkick({ mobile: true, change: function(){
        $('html, body').stop().animate({ scrollTop: $('#' + this.value).offset().top -100 }, 1000); 
      }});
    });
  </script>

</body>
</html>
