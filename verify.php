<?php
if(isset($_POST['g-recaptcha-response'])){
  echo verify($_POST['g-recaptcha-response']);
}

function verify($response) {
  $ip = $_SERVER['REMOTE_ADDR'];
  $key="6LcrHhwTAAAAAFhwdaklKrnYagOQ3DnY5vL9e8Zh"; 

  $url = 'https://www.google.com/recaptcha/api/siteverify';
  $full_url = $url.'?secret='.$key.'&response='.$response.'&remoteip='.$ip;

  $data = json_decode(file_get_contents($full_url));
  if(isset($data->success) && $data->success == true) {
    return true;
  }
  return false;
}
?>
