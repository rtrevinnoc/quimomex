<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>Responsabilidad Social | Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63501207-1', 'auto');
    ga('send', 'pageview');
  </script>

</head>
<body>

  <?php include('header.php'); ?>

  <section class="stage grad" data-bg="assets/img/stages/responsabilidad.jpg">
    <article><div class="tbl"><div class="vab">
      <h1 class="bb-verde">POLÍTICA AMBIENTAL DE QUIMOBÁSICOS Y GRUPO CYDSA</h1>
    </div></div></article>
  </section>

  <section class="bgf1">
    <article class="bb-lima p60">

      <div class="mb30">
        <h2 class="verdeosc">UNA POLÍTICA SUSTENTABLE</h2>
      </div>

      <p class="h4">Es política de Quimobásicos, como parte de Grupo Cydsa, conducir sus operaciones de manera sustentable. Esto significa que las Unidades Operativas de Cydsa deberán acatar todas las leyes, tratados internacionales y regulaciones ambientales aplicables, asegurando además que sus operaciones cumplan o excedan las expectativas de sus Grupos de Interés (Clientes, Distribuidores, Personal, Comunidades, Proveedores, Instituciones Financieras, Accionistas y Sociedad Civil).</p>

    </article>
  </section>

  <section>
    <article class="bb-verde p60">

      <div class="mb30">
        <h2 class="verdemed">COMPROMISOS EN SUSTENTABILIDAD</h2>
      </div>

      <div class="mb30">
        <p class="h4 mb20">En sus operaciones, tanto Quimobásicos como las Unidades Operativas de Cydsa deberán establecer el compromiso de operar de manera sustentable, cumpliendo, y excediendo donde sea posible, los estándares y regulaciones en materia de protección ecológica establecidos en México. </p>
        <p class="h4 mb20">Además de ello, cada Unidad Operativa tendrá la responsabilidad de apegarse a los lineamientos de sustentabilidad generales y particulares definidos por la Corporación.</p>
        <p class="h4">Nos comprometemos también a continuar realizando, aún y cuando las necesidades del mercado no lo requieran, tanto los análisis y mediciones necesarios para dar seguimiento y auditar el cumplimiento de los estándares y lineamientos de sustentabilidad ambiental, mediante la certificación internacional ISO 14001 realizada por evaluadores externos reconocidos internacionalmente, como la certificación de Industria Limpia efectuada por evaluadores nacionales certificados por la Procuraduría Federal de Protección al Ambiente (PROFEPA), acorde con los lineamientos del Global Reporting Initative (GRI).</p>
      </div>

    </article>
  </section>

  <section class="bgf1">
    <article class="bb-lima p60">

      <div class="mb30">
        <h2 class="verdeosc">RESPONSABILIDADES</h2>
      </div>

      <p class="h3 mb40">El Área Corporativa responsable de la Sustentabilidad Ambiental, tiene la obligación de establecer los indicadores globales y particulares de sustentabilidad aplicables a las diversas operaciones de Cydsa. La Gerencia Corporativa de Seguridad y Mejoramiento del Ambiente es responsable de supervisar la evaluación y el cumplimiento de los indicadores ambientales de sustentabilidad así como de verificar que se audite su cumplimiento.</p>
      
      <p class="h2 verdemed"><strong>Límites Contaminantes.</strong></p>
      <p class="h4 mb30">La Gerencia Corporativa de Seguridad y Mejoramiento del Ambiente, es responsable de vigilar que se cumplan las leyes y los reglamentos vigentes. Además, dicha Gerencia asesorará a los Negocios en el diseño y la instalación de los equipos y/o procesos necesarios tanto para mantener los contaminantes dentro de los límites autorizados, como para lograr las metas de reducción y control de emisiones.</p>
      
      <p class="h2 verdemed"><strong>Auditorías de Operación.</strong></p>
      <p class="h4 mb30">Cydsa, a través de La Gerencia Corporativa de Seguridad y Mejoramiento del Ambiente, asume el compromiso y asegura una supervisión funcional a través de auditorías periódicas externas, mediante la certificación internacional ISO 14001 realizada por evaluadores externos reconocidos internacionalmente, y la certificación de Industria Limpia efectuada por evaluadores nacionales certificados por la Procuraduría Federal de Protección al Ambiente (PROFEPA), acorde con los lineamientos del Global Reporting Initiative (GRI).</p>
      
      <p class="h2 verdemed"><strong>Actualización de Ley y Reglamentos.</strong></p>
      <p class="h4 mb30">La Gerencia Corporativa de Seguridad y Mejoramiento del Ambiente es responsable de mantenerse actualizada respecto a las modificaciones que puedan tener la legislación, los tratados internacionales o los reglamentos vigentes relacionados con el control ambiental, con el propósito de comunicar y asesorar oportunamente a Quimobásicos y a todas las Unidades Operativas de Cydsa.</p>
      
      <p class="h2 verdemed"><strong>Notificaciones.</strong></p>
      <p class="h4 mb30">Quimobásicos, al igual que todas la Unidades Operativas tienen la obligación de informar oportunamente a la Gerencia Corporativa de Seguridad y Mejoramiento del Ambiente, respecto a cualquier oficio, cuestionario, notificación o aviso, sobre cualquier asunto relacionado con organismos oficiales o particulares en materia ambiental. Asimismo, se deberá involucrar a la Gerencia Corporativa de Seguridad y Mejoramiento del Ambiente, en las negociaciones o gestiones que resulten como consecuencia de dichos oficios, cuestionarios, notificaciones o avisos.</p>
      
      <p class="h2 verdemed"><strong>Ampliaciones y Nuevos Proyectos.</strong></p>
      <p class="h4 mb30">Todas la Unidades Operativas tienen la obligación de informar oportunamente a la Gerencia Corporativa de Seguridad y Mejoramiento del Ambiente, sobre cualquier construcción, aumento de capacidad y nuevos proyectos que vayan a ser realizados en sus instalaciones. Es responsabilidad de dicha Gerencia, evaluar el impacto ambiental de estas obras o actividades y asegurar que se incorporen las acciones necesarias para el cumplimiento de estándares internos y legislaciones vigentes.</p>
      
      <p class="h2 verdemed"><strong>Compromiso a Metas y Objetivos.</strong></p>
      <p class="h4 mb30">Todas las Unidades Operativas tienen el compromiso y la responsabilidad de definir metas y objetivos orientados a: Minimizar residuos; reutilizar materiales; disminuir descargas de contaminantes al ambiente; reducir consumos de energía y de aguas; reutilizar aguas residuales; incorporar el uso de energías renovables donde sea aplicable; y reducir las emisiones al ambiente. La Gerencia Corporativa de Seguridad y Mejoramiento del Ambiente es responsable de asesorar en esta materia y consolidar los resultados de estas acciones, para dar seguimiento al desempeño de Cydsa en el área de generación de residuos.</p>

    </article>
  </section>

  <?php include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>

</body>
</html>
