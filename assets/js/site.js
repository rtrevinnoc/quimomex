$(document).ready(function(){

  // Placeholders
  $.getScript("assets/js/placeholders.js", function(){ });

  // Menú Aside
  $('.btn-menu').click(function(){
    $('body').addClass('menu-open').append('<div class="mask"></div>');
    $('.mask, aside .icon-adelante').click( function(){
      $('body').removeClass('menu-open');
      setTimeout(function(){ $('header').removeClass('fixed'); },200);
      $('.mask').remove();
    });
  });

  // Backgrounds
  $('section, .img-bg').each(function(){ 
    if( $(this).attr('data-bg') ) $(this).css('background-image', 'url(' + $(this).attr('data-bg') + ')'); 
    if( $(this).attr('data-bgc') ) $(this).css('background-color', '#' + $(this).attr('data-bgc')); 
  });

  // Scroll buttons
  $('.scroll').click(function(){
    var $anchor = $(this);
    $('html, body').stop().animate({ scrollTop: $($anchor.attr('href')).offset().top }, 1000);
    return false;
  }); 

});  