$(document).ready(function(){

  // Slider
  $.getScript("assets/js/slick.min.js", function(){ 

    $('#slider').slick({
      dots: true,
      arrows: false,
      infinite: true,
      draggable: false,
      autoplay: true,
      pauseOnHover: false,
      autoplaySpeed: 9000
    });

  });

  
  $('.prefoot').each(function() {
  var $th = $(this);
  $th.on('click', function() {
    window.open($th.attr('data-href'), $th.attr('data-target'));
  });
});

  // Slider
  $.getScript("assets/js/dropkick.js", function(){ 

    $('.drop').dropkick({ mobile: true });

  });

  // Video
  $.getScript("assets/js/jquery.fancybox.js", function(){ 

    $(".fancybox").fancybox({
      maxWidth  : 800,
      maxHeight : 600,
      width   : '80%',
      height    : '80%',
      autoSize  : false,
      closeClick  : false,
      padding : 0,
      margin: 0
    });

  });

  // Buscador
  // $.getScript("assets/js/magicsuggest-min.js", function(){ 

  //   $('#edo').magicSuggest({ placeholder: 'Buscar por Estado', highlight: false, maxDropHeight: 120, maxSelection: 1, data: [{'id':'AGS', 'name':'Aguascalientes'},{'id':'BC', 'name':'Baja California'},{'id':'BCS', 'name':'Baja California Sur'},{'id':'CAMP', 'name':'Campeche'},{'id':'CHIS', 'name':'Chiapas'},{'id':'CHIH', 'name':'Chihuahua'},{'id':'COAH', 'name':'Coahuila'},{'id':'COL', 'name':'Colima'},{'id':'DF', 'name':'Distrito Federal'},{'id':'DGO', 'name':'Durango'},{'id':'GTO', 'name':'Guanajuato'},{'id':'GRO', 'name':'Guerrero'},{'id':'HGO', 'name':'Hidalgo'},{'id':'JAL', 'name':'Jalisco'},{'id':'MEX', 'name':'Edo. de México'},{'id':'MICH', 'name':'Michoacán'},{'id':'MOR', 'name':'Morelos'},{'id':'NAY', 'name':'Nayarit'},{'id':'NL', 'name':'Nuevo León'},{'id':'OAX', 'name':'Oaxaca'},{'id':'PUE', 'name':'Puebla'},{'id':'QRO', 'name':'Querétaro'},{'id':'QR', 'name':'Quintana Roo'},{'id':'SLP', 'name':'San Luis Potosí'},{'id':'SIN', 'name':'Sinaloa'},{'id':'SON', 'name':'Sonora'},{'id':'TAB', 'name':'Tabasco'},{'id':'TAMPS', 'name':'Tamaulipas'},{'id':'TLAX', 'name':'Tlaxcala'},{'id':'VER', 'name':'Veracruz'},{'id':'YUC', 'name':'Yucatán'},{'id':'ZAC', 'name':'Zacatecas'}], selectionRenderer: function(data){ return data.name + '<input type="hidden" name="edo" value="' + data.id + '" />'; } });

  // });

});  