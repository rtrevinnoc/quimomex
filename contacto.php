<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>Contacto | Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63501207-1', 'auto');
    ga('send', 'pageview');
  </script>

</head>
<body>

  <?php include('header.php'); ?>

  <section class="grad pt140" data-bg="assets/img/stages/contacto.jpg">

    <article>

      <div class="row mb50"><div class="col10 off1 bco">

        <h1>CONTÁCTANOS</h1>
        <a href="javascript:history.back();"><span class="icon icon-atras h1"></span> <span class="h2 ml10">Volver</span></a>

      </div></div>

    </article>

  </section>

  <section>

    <article class="p60">

      <div class="mb40">

        <ul class="list">
          <li>
            <div class="row">
              <div class="col6 sm-col5">
                <p class="h2 azulmed ls40"><b>MONTERREY</b></p>
              </div>
              <div class="col6 sm-col7">
                <p class="h4 azulosc">01 800 830-3300</p>
              </div>
            </div>
          </li>
          <li>
            <div class="row">
              <div class="col6 sm-col5">
                <p class="h2 azulmed ls40"><b>CD. DE MÉXICO</b></p>
              </div>
              <div class="col6 sm-col7">
                <p class="h4 azulosc">01 800 800-4940</p>
              </div>
            </div>
          </li>
          <li>
            <div class="row">
              <div class="col6 sm-col5">
                <p class="h2 azulmed ls40"><b>TOLL-FREE FROM USA</b></p>
              </div>
              <div class="col6 sm-col7">
                <p class="h4 azulosc">1 800 962-3771</p>
              </div>
            </div>
          </li>
        </ul>

      </div>

      <div class="row">

        <div class="col6 xs-mb20">
          <p class="h2 azulmed ls40"><b>NUEVO LEÓN</b></p>
          <ul class="list">
            <li>
              <div class="row">
                <div class="col6 sm-col5">
                  <p class="h4 azulmed">Conmutador</p>
                </div>
                <div class="col6 sm-col7">
                  <p class="h4 azulosc">+52 (81) 8305-4600</p>
                </div>
              </div>
            </li>
            <li>
              <div class="row">
                <div class="col6 sm-col5">
                  <p class="h4 azulmed">Área Comercial 1</p>
                </div>
                <div class="col6 sm-col7">
                  <p class="h4 azulosc">+52 (81) 8305-4610</p>
                </div>
              </div>
            </li>
            <li>
              <div class="row">
                <div class="col6 sm-col5">
                  <p class="h4 azulmed">Área Comercial 2</p>
                </div>
                <div class="col6 sm-col7">
                  <p class="h4 azulosc">+52 (81) 8305-4611</p>
                </div>
              </div>
            </li>
            <li>
              <div class="row">
                <div class="col6 sm-col5">
                  <p class="h4 azulmed">Fax 1</p>
                </div>
                <div class="col6 sm-col7">
                  <p class="h4 azulosc">+52 (81) 8305-4614</p>
                </div>
              </div>
            </li>
            <li>
              <div class="row">
                <div class="col6 sm-col5">
                  <p class="h4 azulmed">Fax 2</p>
                </div>
                <div class="col6 sm-col7">
                  <p class="h4 azulosc">+52 (81) 8305-4615</p>
                </div>
              </div>
            </li>
          </ul>
        </div>

        <div class="col6">
          <p class="h2 azulmed ls40"><b>CIUDAD DE MÉXICO</b></p>
          <ul class="list">
            <li>
              <div class="row">
                <div class="col6 sm-col5">
                  <p class="h4 azulmed">Conmutador</p>
                </div>
                <div class="col6 sm-col7">
                  <p class="h4 azulosc">+52 (55) 9000-4940</p>
                </div>
              </div>
            </li>
            <li>
              <div class="row">
                <div class="col6 sm-col5">
                  <p class="h4 azulmed">Conmutador 2</p>
                </div>
                <div class="col6 sm-col7">
                  <p class="h4 azulosc">+52 (55) 9000-4941</p>
                </div>
              </div>
            </li>
            <li>
              <div class="row">
                <div class="col6 sm-col5">
                  <p class="h4 azulmed">Conmutador 3</p>
                </div>
                <div class="col6 sm-col7">
                  <p class="h4 azulosc">+52 (55) 9000-4942</p>
                </div>
              </div>
            </li>
            <li>
              <div class="row">
                <div class="col6 sm-col5">
                  <p class="h4 azulmed">Fax</p>
                </div>
                <div class="col6 sm-col7">
                  <p class="h4 azulosc">+52 (55) 9000-4943</p>
                </div>
              </div>
            </li>
          </ul>
        </div>

      </div>

    </article>

  </section>

  <section class="bg-azulosc"><article class="p40"><div align="center">
    <span class="bco h2 ls40">O escríbenos al correo: <a href="mailto:quimobasicos@cydsa.com" class="bco italic">quimobasicos@cydsa.com</a></span>
  </div></article></section>

  <!-- <section class="bgf1"><article class="p40"><div class="tbl">
    <div class="col2 va xs-center"><h3 class="azulmed ls40">Línea sin costo</h3></div>
    <div class="col7 va" align="center"><h1 class="azulosc ls40"><b class="italic">01 800 830-3300</b></h1></div>
    <div class="col3" align="center"><img src="assets/img/icons/contacto.png" alt=""></div>
  </div></article></section> -->

  <?php include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>

</body>
</html>
