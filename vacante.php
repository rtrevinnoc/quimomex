<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>CUENTAS POR PAGAR                       (Monterrey, Nuevo León) | Trabaja con nosotros | Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63501207-1', 'auto');
    ga('send', 'pageview');
  </script>
  <script src='https://www.google.com/recaptcha/api.js?hl=es'></script>

</head>
<body>

  <?php include('header.php'); ?>

  <section class="grad pt140" data-bg="assets/img/stages/unete02a.jpg">

    <article>

      <div class="row mb50"><div class="col10 off1 bco">

        <h1>TRABAJA CON NOSOTROS</h1>
        <a href="javascript:history.back();"><span class="icon icon-atras h1"></span> <span class="h2 ml10">Volver</span></a>

      </div></div>

    </article>

  </section>

  <section>

    <article class="p60">

      <div class="row">

        <!-- <div class="col6"> -->
          <h1>EJECUTIVO COMERCIAL</h1>
          <p class="h4 mb30"><strong>Lugar:</strong> Monterrey, Nuevo León</p>

          <div class="mb30">
            <h3><strong>Descripción de la Vacante</strong></h3>
            <p>El departamento comercial de Quimobásicos requiere de una persona para desempeñar las funciones  de Ejecutivo Comercial de Zona Norte.</p>
	    <p>La formación académica del postulante deberá ser de Licenciatura o Ingenieria, con un minimo de 3 años de experiencia en actividades de venta. Dentro de los objetivos del prospecto a elegir se encuentran el incrementar la rentabilidad de la empresa, desarrollo y atención de una cartera de clientes asegurando que los procesos se cumplan en los tiempos establecidos.</p>
            <p>El ejecutivo de la zona norte deberá ser capaz de ejecutar estrategias comerciales a fin de optimizar incrementos en utilidad en las diversas líneas de productos, así como de asegurar que los procesos administrativos y de cobranza se lleven a cabo sin contratiempos.</p>
            <h3><strong>¿Cómo Aplicar?</strong></h3>
            <p>Si cumples con el perfil y deseas pertenecer a una organización comprometida con el logro de resultados así como con la superación de expectativas deberás enviar tu información curricular directamente al área de Relaciones Institucionales de Quimobásicos al correo Electrónico siguiente:</p>
            <p><div align="center"><a href="mailto:raul.acosta@cydsa.com" class="h2 azul"><strong>raul.acosta@cydsa.com</strong></a></div></p>
          </div>
          <p><strong>Compartir:</strong></p>
          <div class="social">
            <a href="javascript:window.open('http://www.facebook.com/sharer.php?u=http://www.quimobasicos.com/vacante.php', '', 'scrollbars=yes,width=800,height=400');" target="_blank" class="icon icon-fb"></a>
            <a href="javascript:window.open('http://twitter.com/home?status=\'Trabaja con Nosotros: CUENTAS POR PAGAR\' via @Quimobasicos - http://www.quimobasicos.com/vacante.php', '', 'scrollbars=yes,width=800,height=400');" target="_blank" class="icon icon-tw"></a>
            <a href="javascript:window.open('http://www.linkedin.com/shareArticle?mini=true&url=http://www.quimobasicos.com/vacante.php&title=Trabaja con Nosotros: CUENTAS POR PAGAR', '', 'scrollbars=yes,width=800,height=400');" target="_blank" class="icon icon-in"></a>
          </div>

        <!-- </div>    -->

        <!-- <div class="col5 off1 mt20"><form action="verify.php" method="post">
          <div class="mb10"><input name="nombre" type="text" class="bd-azul required" placeholder="Nombre(s)*"></div>
          <div class="mb10"><input name="apellidos" type="text" class="bd-azul required" placeholder="Apellidos*"></div>
          <div class="mb10"><input name="email" type="email" class="bd-azul required" placeholder="Correo electrónico*"></div>
          <div class="mb10"><input name="cel" type="text" class="bd-azul" placeholder="Teléfono móvil"></div>
          <div class="mb10"><input name="tel" type="text" class="bd-azul" placeholder="Teléfono fijo"></div>
          <div class="mb10"><input name="puesto" type="text" class="bd-azul required" placeholder="Puesto de Interés" value="CUENTAS POR PAGAR"></div>
          <div class="mb10"><div class="g-recaptcha" data-type="image" data-sitekey="6LcrHhwTAAAAAJB8Vab7WcKFlMxuJ3VoEvRRtyMS"></div></div> -->
          <!-- <div class="mb10 bd-azul"><div class="row">
            <div class="col6 xs-mb20 mt10"><strong>Solicitud de empleo o CV:</strong></div>
            <div class="col6"><input type="file"></div>
          </div></div> -->
        <!--  <div align="right"><button class="btn lg bg-azulmed">Enviar</button></div>
        </form></div>    -->

      </div>

    </article>

  </section>

  <?php include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>
  <script>
    $(document).ready(function(){
      $('.required').keyup(function(){ $(this).parent().removeClass('error'); });
      $('form').submit(function(e){
        e.preventDefault();
        var empty = false;
        $('.error').removeClass('error');
        $(".required").each(function() { if(!$.trim($(this).val())){ empty = true; $(this).parent().addClass('error'); } });
        if( !empty ){
          var serializedValues = $("form").serialize();
          $.ajax({ type: 'POST', url:"verify.php", data: serializedValues, success:function(result){
            if(result){
              $.ajax({ type: 'POST', url:"send.php", data: serializedValues, success:function(result){
                $('form').html('<h2>Tu información ha sido enviada</h2>');
              }});
            } else {
              alert('Favor de verificar el Captcha');
            }
          }});      
        }
      }); 
    }); 
  </script>

</body>
</html>
