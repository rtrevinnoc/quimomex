<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>Descargables | Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63501207-1', 'auto');
    ga('send', 'pageview');
  </script>

</head>
<body>

  <?php include('header.php'); ?>

  <section class="grad pt140" data-bg="assets/img/stages/descargas.jpg">

    <article>

      <div class="row mb50"><div class="col10 off1 bco">

        <h1>DESCARGABLES</h1>
        <a href="javascript:history.back();"><span class="icon icon-atras h1"></span> <span class="h2 ml10">Volver</span></a>

      </div></div>

    </article>

  </section>

  <section class="bgf1">

    <article class="p60">

      <div class="mb30">
        <h2 class="azulosc mb30">ARCHIVOS DESCARGABLES</h2>
        <p class="h3">Aquí puedes encontrar una extensa variedad de archivos descargables, así como una amplia gama de información complementaria que abarca boletines técnicos, folletos de productos, actualización de las regulaciones gubernamentales y material audiovisual de capacitación.</p>
      </div>

      <ul class="descargas">

        <li><a href="assets/files/ManualdeusodeMarcaQuimobasicos-IMMXVI.pdf" target="_blank">Manual de Identidad <span class="icon icon-descarga"></span></a></li>
        <li><a id="SW" href="assets/files/GenetronPropertiesv1.2.zip" target="_blank">Software Genetron® <span class="icon icon-descarga"></span></a></li>
        <li><a id="TP" href="assets/files/TPT_QUIMOBASICOS_2016_27052016.pdf" target="_blank">Tabla de Presiones <span class="icon icon-descarga"></span></a></li>
        <li>
          <a class="acc" id="HS">Hojas de Seguridad <span class="icon icon-mas"></span></a>
          <div><ul>
            <li><a target="_blank" href="assets/files/21HDS.pdf">Enovate® 245fa<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/02HDS.pdf">Genetron® 22<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/03HDS.pdf">Genetron® AZ20® (R-410A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/04HDS.pdf">Genetron® 404A<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/22HDS.pdf">Genetron® 134a<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/23AHDS.pdf">Genetron® AZ 50®  (R-507)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/23BHDS.pdf">Genetron® AZ 50®  (R-507A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/24HDS.pdf">Genetron® HP 80 (R-402A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/25HDS.pdf">Genetron® HP 81 (R-402B)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/26HDS.pdf">Genetron® MP 66 (R-401B)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/27HDS.pdf">Genetron® MP 39 (R-401A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/28HDS.pdf">Genetron® PERFORMAX LT® (R-407F)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/29HDS.pdf">Genetron® 23<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/30HDS.pdf">Genetron® 407C<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/31HDS.pdf">Genetron® 408A<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/32HDS.pdf">Genetron® 409A<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/33HDS.pdf">Genetron® 422D<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/34HDS.pdf">Genetron® 508B<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/39HDS.pdf">Genetron® 141b<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/40HDS.pdf">Genetron® 123<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/41HDS.pdf">Genetron® 124<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/52HDS.pdf">Genetron® 502<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/53HDS.pdf">Genetron® 152a<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/55HDS.pdf">Genetron® 32<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/20HDS.pdf">Quimobásicos Eco® Flush HFO-1233zd<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/01AHDS.pdf">Solstice® 1234yf (CNRSYMAS) (U.N. 3161)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/01BHDS.pdf">Solstice® 1234yf (LATA) (U.N. 2037)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/35HDS.pdf">Solstice® 1234ze<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/36HDS.pdf">Solstice® N40 (R-448A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/37HDS.pdf">Solstice® 1233zd<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/43HDS.pdf">Solstice® N41 (R-466A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/44HDS.pdf">Solstice® N15 (R-515B)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/45HDS.pdf">Solstice® L40X (R455A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/46HDS.pdf">Solstice® N13 (R450A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/47HDS.pdf">Solstice® L41Y (R452B)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/48HDS.pdf">Solstice® N71 (R-471A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/49HDS.pdf">Solstice® 452A (R-452A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/50HDS.pdf">Solstice® 513A (R-513A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/54HDS.pdf">Solstice® 454B (R-454B)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/56HDS.pdf">Solstice® 449A (R-449A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/15HDS.pdf">R-600a (Isobutano)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/16HDS.pdf">R-32 (Difluorometano)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/17HDS.pdf">R-410A (Granel)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/18HDS.pdf">R-125 (Granel)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/19HDS.pdf">R-404A (Granel)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/38HDS.pdf">HFC-245fa<span class="icon icon-descarga"></span></a></li>
          </ul>
        </div>
        </li>
        <li>
          <a class="acc" id="HS">Hojas de Seguridad Aquion <span class="icon icon-mas"></span></a>
          <div><ul>
            <!--<li><a target="_blank" href="assets/files/05HDS.pdf">Aquion® 22</a></li>!-->
            <li><a target="_blank" href="assets/files/06HDS.pdf">Aquion® 32</a></li>
            <li><a target="_blank" href="assets/files/07HDS.pdf">Aquion® 134a</a></li>
            <li><a target="_blank" href="assets/files/08HDS.pdf">Aquion® 290</a></li>
            <li><a target="_blank" href="assets/files/09HDS.pdf">Aquion® 404A</a></li>
            <li><a target="_blank" href="assets/files/10HDS.pdf">Aquion® 407C</a></li>
            <li><a target="_blank" href="assets/files/11HDS.pdf">Aquion® 410A</a></li>
            <li><a target="_blank" href="assets/files/12HDS.pdf">Aquion® 507</a></li>
            <li><a target="_blank" href="assets/files/13HDS.pdf">Aquion® 600a</a></li>
            <li><a target="_blank" href="assets/files/14HDS.pdf">Aquion® Limpieza</a></li>
          </ul>
        </div>
        </li>
        <li>
          <a class="acc" id="FT">Fichas Técnicas <span class="icon icon-mas"></span></a>
          <div><ul>

            <li><a target="_blank" href="assets/files/06FT.pdf">Aquion® 32</a></li>
            <li><a target="_blank" href="assets/files/07FT.pdf">Aquion® 134a</a></li>
            <li><a target="_blank" href="assets/files/08FT.pdf">Aquion® 290</a></li>
            <li><a target="_blank" href="assets/files/13FT.pdf">Aquion® 600a</a></li>
            <li><a target="_blank" href="assets/files/14FT.pdf">Aquion® Limpieza</a></li>
            <li><a target="_blank" href="assets/files/21FT.pdf">Enovate® 245fa<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/02FT.pdf">Genetron® 22<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/03FT.pdf">Genetron® AZ20® (R-410A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/04FT.pdf">Genetron® 404A<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/22FT.pdf">Genetron® 134a<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/23AFT.pdf">Genetron® AZ 50®  (R-507)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/23BFT.pdf">Genetron® AZ 50®  (R-507A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/24FT.pdf">Genetron® HP 80 (R-402A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/25FT.pdf">Genetron® HP 81 (R-402B)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/26FT.pdf">Genetron® MP 66 (R-401B)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/27FT.pdf">Genetron® MP 39 (R-401A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/28FT.pdf">Genetron® PERFORMAX LT® (R-407F)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/29FT.pdf">Genetron® 23<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/30FT.pdf">Genetron® 407C<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/31FT.pdf">Genetron® 408A<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/32FT.pdf">Genetron® 409A<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/33FT.pdf">Genetron® 422D<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/34FT.pdf">Genetron® 508B<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/39FT.pdf">Genetron® 141b<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/40FT.pdf">Genetron® 123<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/41FT.pdf">Genetron® 124<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/55FT.pdf">Genetron® 32<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/01FT.pdf">Solstice® 1234yf (CNRSYMAS) (U.N. 3161)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/35FT.pdf">Solstice® 1234ze<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/36FT.pdf">Solstice® N40 (R-448A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/37FT.pdf">Solstice® 1233zd<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/43FT.pdf">SOLSTICE® N41 (R-466A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/44FT.pdf">SOLSTICE® N15 (R-515B)</a></li>
            <li><a target="_blank" href="assets/files/45FT.pdf">Solstice® L40X (R455A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/46FT.pdf">Solstice® N13 (R450A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/47FT.pdf">Solstice® L41Y (R452B)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/48FT.pdf">Solstice® N71 (R-471A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/49FT.pdf">Solstice® 452A (R-452A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/50FT.pdf">Solstice® 513A (R-513A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/54FT.pdf">Solstice® 454B (R-454B)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/56FT.pdf">Solstice® 449A (R-449A)<span class="icon icon-descarga"></span></a></li>
            <li><a target="_blank" href="assets/files/20FT.pdf">Quimobásicos Eco® Flush HFO-1233zd<span class="icon icon-descarga"></span></a></li>
          </ul></div>
        </li>
      </ul>

    </article>

  </section>

  <?php include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>

  <script>
    $(document).ready(function(){
      $('.acc').click(function(){ $(this).next().slideToggle(); $(this).toggleClass('act'); });
      <?php if( $_GET['s'] != '' ) : ?>
      $('html, body').stop().animate({ scrollTop: $('#<?php echo $_GET['s']; ?>').offset().top - 130 }, 1000);
      $('#<?php echo $_GET['s']; ?>').next().slideToggle(); $('#<?php echo $_GET['s']; ?>').toggleClass('act');
      <?php endif; ?>
    });
  </script>

</body>
</html>
