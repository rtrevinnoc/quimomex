<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>AQUION® 290 | Gases Refrigerantes | Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>

</head>
<body>

  <? include('header.php'); ?>

  <section class="grad pt140"><div class="img-bg" data-bg="assets/img/stages/detalle.jpg"></div>

    <article>

      <div class="row mb50"><div class="col10 off1 bco">

        <h1>GASES REFRIGERANTES</h1>
        <h3>Creamos productos hechos a la medida de tus necesidades. </h3>
        <a href="javascript:history.back();"><span class="icon icon-atras h1"></span> <span class="h2 ml10">Volver</span></a>

      </div></div>

      <div class="content"><div class="row">

        <div class="col3 sm-col4">
          <div class="mb20"><img src="assets/img/renders/08b.png" alt="AQUION® 290"></div>
          <a href="assets/files/08HDS.pdf" target="_blank" class="btn bg-dorado full mb10"><span class="icon icon-seguridad"></span> HOJA DE SEGURIDAD</a>
          <a href="assets/files/08FT.pdf" target="_blank" class="btn bg-gris full mb10"><span class="icon icon-nube"></span> FICHA TÉCNICA</a>
          <!-- <a href="ubica.php" class="btn bg-vaqua full mb10"><span class="icon icon-mapa"></span> DÓNDE COMPRAR</a> --!>
        </div>

        <div class="col8 off1">

          <h1><b style="    font-size: .9em !important;">AQUION® 290</b></h1>
          <h4 class="mb40">

            Refrigerante hidrocarburo que no daña la capa de ozono y con muy bajo potencial de efecto invernadero, presenta un gran desempeño en refrigeración comercial.

          </h4>

          <table class="mb40">
            <thead>
              <tr>
               <th>Sección</th>
                <th>Especificación</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td data-th="Item">ASHRAE #</td>
                <td data-th="Specification">R-290</td>
              </tr>
              <tr>
                <td data-th="Item">Clase</td>
                <td data-th="Specification">HC</td>
              </tr>
              <tr>
                <td data-th="Item">Tipo de Refrigerante</td>
                <td data-th="Specification">Gas licuado</td>
              </tr>
              <tr>
                <td data-th="Item">Notas</td>
                <td data-th="Specification">Mineral</td>
              </tr>
              <tr>
                 <td data-th="Item">Contenido o Fórmula</td>
                <td data-th="Specification">C3H8</td>                
              </tr>
              <!--<tr>                <td data-th="Item">Substitutes</td>
                <td data-th="Specification">HCFC-22</td>               
              </tr>
              <tr>
                <td data-th="Item">Glide</td>
                <td data-th="Specification">Near zero glide</td>
              </tr>-->
              <tr>
                <td data-th="Item">Nombre</td>
                <td data-th="Specification">Propano</td>
              </tr>
              <tr>
                <td data-th="Item">Notas</td>
                <td data-th="Specification">Compuesto puro que facilita su manejo y utilización</td>
              </tr>

            <tr>
                <td data-th="Item">Presentaciones disponibles</td>
                <td data-th="Specification">Lata 150 g, Cilindro 5 kg</td>
              </tr>
            </tbody>
          </table>

          <p class="h4">

            Por ser altamente inflamable su uso debe ser en lugares ventilados y lejos de fuentes de ignición y chispa.
          
          </p>

        </div>

      </div></div>

      <div class="content bge3"><div class="row">

        <div class="col3 h85 bbot xs-ha xs-left xs-mb20" align="right"><h3 class="gris6">PRODUCTOS RELACIONADOS</h3></div>

        <div class="col3 xs-mb10 rel-prod" data-href="aquion32.php">
          <div class="h85 bbot row" align="center"><div class="col8 off2 sm-col12"><img src="assets/img/renders/06a.png"class="sm-col6 sm-off3" alt=""></div></div>
          <p align="center"><b>Aquion® 32</b></p>
        </div>

        <div class="col3 sm-mb10 rel-prod" data-href="aquion600a.php">
          <div class="h85 bbot row" align="center"><div class="col8 off2 sm-col12"><img src="assets/img/renders/13a.png" class="sm-col6 sm-off3" alt=""></div></div>
          <p align="center"><b>Aquion® 600a</b></p>
        </div>

        <div class="col3 rel-prod" data-href="aquion_limpieza.php">
          <div class="h85 bbot row" align="center"><div class="col8 off2 sm-col12"><img src="assets/img/renders/14a.png" class="sm-col6 sm-off3" alt=""></div></div>
          <p align="center"><b>Aquion® Limpieza</b></p>
        </div>

      </div></div>

    </article>

  </section>

  <? include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>

  <script>
    $(document).ready(function(){
      
      $('.rel-prod').click(function(){ window.location.href = $(this).data('href'); }); 
      $('.rel-prod').hover(
        function(){ $(this).find('img').css({ 'width': '135%', 'margin': '-17% 0 0 -17%' }); },
        function(){ $(this).find('img').removeAttr('style'); }
      ); 

    });
  </script>

</body>
</html>
