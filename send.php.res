<?php

$message = '<html><body>';
$message .= '<table width="100%" cellpadding="10">';
$message .= "<tr style='background: #2a70ac; border: 1px solid #069;'><td colspan='2' align='center'><strong style='color:#FFFFFF;'>Información de contacto</strong> </td></tr>";

// Contact Information
if(isset($_POST['nombre'])){
  $message .= "<tr><td width='30%'><strong>Nombre:</strong> </td><td>" . strip_tags($_POST['nombre']) . " " . strip_tags($_POST['apellidos']) . "</td></tr>";
}
if(isset($_POST['email'])){
  $message .= "<tr><td><strong>E-mail:</strong> </td><td>" . strip_tags($_POST['email']) . "</td></tr>";
}
if(isset($_POST['tel'])){
  $message .= "<tr><td><strong>Tel.:</strong> </td><td>" . strip_tags($_POST['tel']) . "</td></tr>";
}
if(isset($_POST['cel'])){
  $message .= "<tr><td><strong>Cel.:</strong> </td><td>" . strip_tags($_POST['cel']) . "</td></tr>";
}
if(isset($_POST['puesto'])){
  $message .= "<tr><td><strong>Puesto de Interés:</strong> </td><td>" . strip_tags($_POST['puesto']) . "</td></tr>";
}

$message .= "</table>";
$message .= "</body></html>";
$header = "From:no-reply@quimobasicos.com \r\n";
$header .= "MIME-Version: 1.0\r\n";
$header .= "Content-Type: text/html; charset=UTF-8\r\n";

$to = 'adsguz@gmail.com';
$subject = "Formulario vacante: " . strip_tags($_POST['puesto']);

$retval = mail($to,$subject,$message,$header);

?>