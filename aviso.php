<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>Aviso de Privacidad | Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63501207-1', 'auto');
    ga('send', 'pageview');
  </script>

</head>
<body>

  <?php include('header.php'); ?>

  <section class="grad pt140 img-bg" data-bg="assets/img/stages/aviso.jpg">

    <article>

      <div class="row mb50"><div class="col10 off1 bco">

        <h1>AVISO DE PRIVACIDAD</h1>
        <a href="javascript:history.back();"><span class="icon icon-atras h1"></span> <span class="h2 ml10">Volver</span></a>

      </div></div>

    </article>

  </section>

  <section>

    <article class="p60">

      <p>Quimobásicos, S.A. de C.V., con domicilio en la avenida Ruiz Cortines 2333 Pte., de la Colonia Pedro Lozano, en Monterrey, Nuevo León, México, en el caso de que usted proporcione datos personales, es responsable de recabarlos, del uso que se le dé a los mismos y de su protección.</p>
      <p>Su información personal será utilizada para análisis, para atender su solicitud, decidir sobre los servicios y/o productos que ha solicitado, informarle sobre cambios en los mismos y evaluar la calidad de nuestra actividad. Para las finalidades antes mencionadas, usualmente obtenemos algunos o todos los datos personales siguientes: nombre, denominación o razón social, nacionalidad, domicilio, teléfono, cuenta de correo electrónico, ocupación, datos económicos y fiscales. También para las finalidades antes mencionadas, es posible que obtengamos datos personales como el estado de salud, entre otros, que la Ley Federal de Protección de Datos Personales en Posesión de los Particulares determina como sensibles.</p>
      <p>Usted tiene derecho de acceder, rectificar y cancelar sus datos personales, así como de oponerse al tratamiento de los mismos o revocar el consentimiento que para tal fin nos haya otorgado, a través de los procedimientos que hemos implementado.</p>
      <p>Los procedimientos que se han implementado para el ejercicio de dichos derechos son a través de la presentación de la solicitud por escrito respectiva en el domicilio ubicado en la avenida Ruiz Cortines 2333 Pte., Colonia Pedro Lozano, en Monterrey, Nuevo León, México, en donde su solicitud será recibida por la unidad receptora designada, misma que lo turnara al responsable de la protección de los datos personales para su análisis y resolución. Su solicitud deberá contener la siguiente información: determinar con precisión cual o cuales derechos son los que desea ejercer, de entre acceder, rectificar y cancelar sus datos personales, oponerse a su tratamiento y revocar el consentimiento otorgado para su tratamiento; también la solicitud deberá contener los datos personales específicos objeto de su solicitud, la fecha y motivo por el cual nos proporcionó sus datos personales, así como su nombre completo, su domicilio, teléfono, correo electrónico, y deberá acompañar copia certificada del poder en caso de que actúe representando de alguien. El plazo para atender su solicitud es de 45 días hábiles, en el caso de que la solicitud sea presentada en forma clara y completa, de lo contrario, se le requerirá dentro de los 20 días hábiles siguientes a la presentación de la solicitud la aclare o complete para lo cual tendrá un término de 20 días hábiles; los 45 días hábiles para contestar a su solicitud comenzaran a correr desde la fecha en que contemos con la solicitud completa y clara. En el caso de que no recibamos la información complementaria y las aclaraciones requeridas dentro del término indicado, su solicitud se dará por desistida y concluida para todos los efectos legales. Tanto el requerimiento de información aclaratoria o complementaria, como la respuesta a su solicitud se le notificaran en el domicilio ubicado en la avenida Ruiz Cortines 2333 Pte., Colonia Pedro Lozano, en Monterrey, Nuevo León, México, o por correo electrónico a la cuenta de correo que usted haya proporcionado a elección nuestra.</p>
      <p>En caso de duda sobre los procedimientos, los requisitos y plazos, se puede poner en contacto con nuestro departamento de datos personales en el domicilio ubicado en avenida Ruiz Cortines 2333 Pte., Colonia Pedro Lozano, en Monterrey, Nuevo León, México, o enviando su preguntas o dudas a la siguiente dirección electrónica: proteccion.datospersonales@cydsa.com.</p>
      <p>Asimismo, le informamos que sus datos personales pueden ser transferidos y tratados dentro y fuera del país, por personas distintas a esta empresa. En ese sentido, su información puede ser compartida con personas relacionadas con esta empresa, para análisis, para responder a su solicitud y en su caso para decidir sobre los servicios y/o productos que ha solicitado, informarle sobre cambios en los mismos y para evaluación. Si usted no manifiesta su oposición para que sus datos personales sean transferidos, se entenderá que ha otorgado su consentimiento para ello.</p>
      <p>Si usted desea dejar de recibir mensajes promocionales de nuestra parte puede solicitarlo en nuestras Oficinas Generales ubicadas en la Avenida Ruíz Cortines 2333 Poniente, en la Colonia Pedro Lozano, en Monterrey, Nuevo León, México.  o en la siguiente dirección electrónica: <a class="azul under" href="mailto:proteccion.datospersonales@cydsa.com">proteccion.datospersonales@cydsa.com</a>.</p>
      <p>Las leyes y regulaciones de diferentes países pueden imponer diferentes requerimientos (e inclusive conflictivos) en el Internet y la protección de la información. Nosotros estamos ubicados en México y todos los asuntos en relación al Aviso de Privacidad son regidos por las leyes de México. Si usted está ubicado en algún otro país fuera de México y nos contacta, por favor tome en cuenta que cualquier información que usted nos provea será transferida a México, y al momento de ingresar su información usted autoriza esta transferencia y la aceptación del presente Aviso de Privacidad.</p>
      <p>Cualquier modificación a este aviso de privacidad podrá consultarla en el portal de internet: <a href="http://www.quimobasicos.com.mx" class="azul under">www.quimobasicos.com.mx</a></p>
      <p>Fecha última actualización: <strong>15 de Febrero del 2016</strong>.</p>
      <p>En el caso de que se recaben datos personales sensibles, consiento que los datos personales sensibles sean tratados conforme a los términos y condiciones del presente aviso de privacidad.</p>

    </article>

  </section>

  <?php include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>

</body>
</html>
