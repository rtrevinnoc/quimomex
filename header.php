<header><div class="container">

  <div class="row">

    <div class="col4 sm-col3 xs-col10"><a href="index.php"><img src="assets/img/icons/logo.png" alt="Quimobásicos"></a></div>

    <div class="col8 sm-col9 xs-col2" align="right">
      <a href="https://blogquimobasicos.com/" target="_blank" class="btn bg-azulmed mr20"><span class="icon icon-blog"></span> BLOG</a>
      <a href="sppi.php" class="btn bg-azulmed mr20"><span class="icon icon-pedidos"></span> SISTEMA DE PEDIDOS</a>
      <a class="btn-menu"><span><span></span><span></span><span></span></span> <span class="hide-xs">MENÚ</span></a>
    </div>

  </div>

</div></header>

<aside>

  <div align="right"><a class="icon icon-adelante"></a></div>
  <nav><ul>
    <li><a href="index.php">HOME</a></li>
    <li><a href="nosotros.php">NOSOTROS</a></li>
    <li><a href="mercados.php">MERCADOS</a></li>
    <li><a href="soluciones.php">SOLUCIONES</a></li>
    <li><a href="productos.php?cat=ACA">PRODUCTOS</a></li>
    <li><a href="https://blogquimobasicos.com/" target="_blank">BLOG</a></li>
    <li><a href="contacto.php">CONTÁCTANOS</a></li>
  </ul></nav>
  <div class="social">
    <a href="https://www.facebook.com/Quimobasicos" target="_blank" class="icon icon-fb"></a>
    <a href="https://twitter.com/Quimobasicos" target="_blank" class="icon icon-tw"></a>
    <a href="https://www.linkedin.com/company/quimob%C3%A1sicos" target="_blank" class="icon icon-in"></a>
    <a href="https://www.youtube.com/user/tvquimobasicos" target="_blank" class="icon icon-yt"></a>
    <a href="https://blogquimobasicos.com/" target="_blank" class="blog"><img src="assets/img/icons/blog.png" alt="BLOG"></a>
  </div>
  <p align="center">Copyright © 2018 Quimobásicos S.A. de C.V. </p>

</aside>
