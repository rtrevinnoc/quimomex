<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>Búsqueda | Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />
  <!--<link type="text/css" rel="stylesheet" href="assets/css/dropkick.css" />-->

   <link rel="stylesheet" href="dist/store-locator2.css">

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63501207-1', 'auto');
    ga('send', 'pageview');
  </script>

 <style type="text/css">
   ::placeholder {  /* Chrome, Firefox, Opera, Safari 10.1+ */
    display: none !important; }
 </style>


</head>
<body>

  <?php include('header.php'); ?>

  <section class="grad pt140" data-bg="assets/img/stages/empresa.jpg">

    <article>

      <div class="row mb50"><div class="col10 off1 bco">

        <h1>BÚSQUEDA</h1>
        <a href="javascript:history.back();"><span class="icon icon-atras h1"></span> <span class="h2 ml10">Volver</span></a>

      </div></div>

    </article>

  </section>

  <section class="bgf1" style="text-align: center;">

    <article class="p60">



      <div class="mb30">
        <h2>LA RED MÁS GRANDE DE DISTRIBUIDORES EN MÉXICO</h2>
        <p class="h4 slab"><em>LOCALIZA TU DISTRIBUIDOR MÁS CERCANO EN LA REPÚBLICA.</em></p><br>
          <div id="my-store-locator">
      <!-- map will be rendered here-->
    </div>
      </div>
  
      

    

    </article>

  </section>

  <?php include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>

  <!--<script type="text/javascript" src="assets/js/dropkick.js"></script>-->
  <script type="text/javascript" src="assets/js/jquery.quick.pagination.min.js"></script>



 <script src="dist/store-locator.js"></script>
    <script>
      storeLocator({
        container: 'my-store-locator',
        apiKey: 'AIzaSyAVLLFjWjZkQC170Txc6VEPLT2v1RsmOx0',
        searchHint: "Filtra tu búsqueda tecleando la ciudad o localidad deseada.",
        stores: [
           {
            name: 'OASA - Tijuana',
            address: 'Avenida Malinche 22, Gabilondo, Tijuana, BC, México',
            location: {lat: 32.515795, lng: -117.01536},
            website: 'http://www.oasatijuana.com/'
          },

          {
            name: 'OASA - Mexicali',
            address: 'Blvd. López Mateos 825, Bellavista, Mexicali, BC, México',
            location: {lat: 32.656344, lng: -115.47827},
            website: 'http://www.oasatijuana.com/'
          },

            {
            name: 'Cuevas Corporación - Matriz',
            address: 'Bravo 273, Centro, Mexicali, BC, México',
            location: {lat: 32.66137, lng: -115.484407},
            website: 'http://www.cuevasco.com/'
          },
             {
            name: 'Cuevas Corporación - Jardines del Lago',
            address: 'Lazaro Cardenas 446, Jardines del Lago, Mexicali, BC, México',
            location: {lat: 32.623942, lng: -115.485976},
            website: 'http://www.cuevasco.com/'
          },
          {
            name: 'Cuevas Corporación - Aviación',
            address: 'Aviación 1002, Prohogar, Mexicali, BC, México',
            location: {lat: 32.658389, lng: -115.430826},
            website: 'http://www.cuevasco.com/'
          },
          {
            name: 'Cuevas Corporación - Lazaro Cardenas',
            address: 'Lázaro Cardenas 819, Independencia, Mexicali, BC, México',
            location: {lat: 32.624934, lng: -115.433553},
            website: 'http://www.cuevasco.com/'
          },
          {
            name: 'Cuevas Corporación - Magna',
            address: 'Calzada Anahuac 2/1/1398, Casa Magna, Mexicali, BC, México',
            location: {lat: 32.591407, lng: -115.476078},
            website: 'http://www.cuevasco.com/'
          },
          {
            name: 'Cuevas Corporación - Nuevo Mexicali',
            address: 'Novena 950, Nuevo Mexicali, Mexicali, BC, México',
            location: {lat: 32.609732, lng: -115.387593},
            website: 'http://www.cuevasco.com/'
          },
          {
            name: 'Refrigeración Ornelas - Matriz',
            address: '1700, Nacozari, Mexicali, BC, México',
            location: {lat: 32.640642, lng: -115.4596},
            
          },
          {
            name: 'DEROSA - Matriz',
            address: 'Lazaro Cardenas 214, Jardines del Lago, Mexicali, BC, México',
            location: {lat: 32.623643, lng: -115.488802},
          
          },
          {
            name: 'PROFEPART - Matriz',
            address: 'Puebla 1099, Pueblo Nuevo, Mexicali, BC, México',
            location: {lat: 32.652645, lng: -115.495261},
            website: 'http://www.profepart.com.mx'
          },
          {
            name: 'PROFEPART - Aviación',
            address: 'Calz. Cuauhtémoc 873, Pro-Hogar, Mexicali, BC, México',
            location: {lat: 32.658417, lng: -115.437738},
            website: 'http://www.profepart.com.mx'
          },
          {
            name: 'PROFEPART - Nuevo Mexicali',
            address: 'San Pedro Mezquital 2996, Polaco, Mexicali, BC, México',
            location: {lat: 32.607894, lng: -115.389786},
            website: 'http://www.profepart.com.mx'
          },
          {
            name: 'PROFEPART - Villas del Rey',
            address: 'Blvd. Anáhuac 288-5, Villas del Rey, Mexicali, BC, México',
            location: {lat: 32.608219, lng: -115.479239},
            website: 'http://www.profepart.com.mx'
          },
          {
            name: 'PROFEPART - López Mateos',
            address: 'Blvd. López Mateos 2290, Plaza Catellón, Mexicali, BC, México',
            location: {lat: 32.615723, lng: -115.442169},
            website: 'http://www.profepart.com.mx'
          },
          {
            name: 'PROFEPART - Tijuana',
            address: 'Av. Ocampo 2051-B, Centro, Tijuana, BC, México',
            location: {lat: 32.531816, lng: -117.030709},
            website: 'http://www.profepart.com.mx'
          },
          {
            name: 'PROFEPART - Ensenada',
            address: 'Iturbide 896, Plaza Santa Lucia, Ensenada, BC, México',
            location: {lat: 31.866341, lng: -116.609162},
            website: 'http://www.profepart.com.mx'
          },
          {
            name: 'PROFEPART - San Luis Rio Colorado',
            address: 'Av. Libertad L10, Plaza Bonita, San Luis Rio Colorado, SON, México',
            location: {lat: 32.452069, lng: -114.771662},
            website: 'http://www.profepart.com.mx'
          },
          {
            name: 'PROFEPART - Hermosillo',
            address: 'Veracruz 38, San Benito, Hermosillo, SON, México',
            location: {lat: 29.092261, lng: -110.9706},
            website: 'http://www.profepart.com.mx'
          },
          {
            name: 'PROFEPART - Puerto Peñasco',
            address: 'Constitución L19, Benito Juárez, Puerto Peñasco, SON, México',
            location: {lat: 31.31242, lng: -113.535589},
            website: 'http://www.cuevasco.com/'
          },
          {
            name: 'Radiadores Frontera - Matriz',
            address: 'Av. Valentín Fuentes 1643, Villahermosa, Ciudad Juárez,CHIH, México',
            location: {lat: 31.718176, lng: -106.432293},
            website: 'http://www.radiadoresfrontera.com/'
          },
          {
            name: 'Radiadores Frontera',
            address: 'Paseo de la Victoria 2707, Paseo de la Victoria, Ciudad Juárez, CHIH, México',
            location: {lat: 31.699591, lng: -106.403827},
            website: 'http://www.radiadoresfrontera.com/'
          },
          {
            name: 'Radiadores Frontera',
            address: 'Av. López Mateos 1624, Paseo del Norte, Ciudad Juárez, CHIH, México',
            location: {lat: 31.719215, lng: -106.4513},
            website: 'http://www.radiadoresfrontera.com/'
          },
          {
            name: 'Radiadores Frontera',
            address: 'Blvd. Independencia 157, Zaragoza, Ciudad Juárez, CHIH, México',
            location: {lat: 31.621839, lng: -106.357294},
            website: 'http://www.radiadoresfrontera.com/'
          },
          {
            name: 'Radiadores Frontera',
            address: 'Av. 20 de Noviembre 1309, Obrera, Chihuahua, CHIH, México',
            location: {lat: 28.628258, lng: -106.069974},
            website: 'http://www.radiadoresfrontera.com/'
          },
          {
            name: 'Radiadores Frontera',
            address: 'Av. Tenologico 9909, Revolucion, Chihuahua, CHIH, México',
            location: {lat: 28.693373, lng: -106.115322},
            website: 'http://www.radiadoresfrontera.com/'
          },
          {
            name: 'Radio Refrigeración de Juárez',
            address: 'Torres Félix Lugo 5502, Los Alcaldes, Ciudad Juárez, CHIH, México',
            location: {lat: 28.643548, lng: -106.070848},
            website: 'http://www.radiorefrigeraciondejuarez.com/'
          },
          {
            name: 'Radio Refrigeración de Juárez',
            address: 'Av. Jilotepec 70358, Del Real, Ciudad Juárez, CHIH, México',
            location: {lat: 31.660817, lng: -106.361945},
            website: 'http://www.radiorefrigeraciondejuarez.com/'
          },
          {
            name: 'Radio Refrigeración de Juárez',
            address: 'Gómez Morin 9850, Partido Senecú, Ciudad Juárez, CHIH, México',
            location: {lat: 31.694932, lng: -106.388065},
            website: 'http://www.radiorefrigeraciondejuarez.com/'
          },
          {
            name: 'Radio Refrigeración de Juárez',
            address: 'Av. Vicente Guerrero 6096, Av. Vicente Guerrero, Ciudad Juárez, CHIH, México',
            location: {lat: 31.732908, lng: -106.430677},
            website: 'http://www.radiorefrigeraciondejuarez.com'
          },
          {
            name: 'Radio Refrigeración de Juárez',
            address: 'Av. Tecnológico 6305, Las Granjas, Chihuahua, CHIH, México',
            location: {lat: 28.67596, lng: -106.102429},
            website: 'http://www.radiorefrigeraciondejuarez.com'
          },
          {
            name: 'Radio Refrigeración de Juárez',
            address: 'Av. De la raza 5988, Misiones San Miguel, Ciudad Juárez, CHIH, México',
            location: {lat: 31.726445, lng: -106.43252},
            website: 'http://www.radiorefrigeraciondejuarez.com'
          },
          {
            name: 'Lemarsa - Matriz',
            address: 'Corregidora 2700, Santo Niño, Chihuahua, CHIH, México',
            location: {lat: 28.650457, lng: -106.078266},
           
          },
          {
            name: 'Sistemas de Conservación Inteligente - Matriz',
            address: 'Belisario Chavez 4885, Quintas Lupita, Cuauhtemoc, CHIH, México',
            location: {lat: 28.427469, lng: -106.870925},
            website: 'http://www.srefrigeracioninteligente.com.mx/'
          },
          {
            name: 'Konfort - Matriz',
            address: 'Julian Carrillo 292, Centro, Torreon, COAH, México',
            location: {lat: 25.538798, lng: -103.421447},
            website: 'http://www.konfort.com.mx/'
          },
          {
            name: 'Konfort - Torreon',
            address: 'Arcos de San Sebastian 102, Portales de San Sebastian, León, GTP, México',
            location: {lat: 21.102955, lng: -101.707927},
            website: 'http://www.konfort.com.mx/'
          },
          {
            name: 'MS Mantenimiento - Matriz',
            address: 'Cuitlahuac 3450, Cuitlahuac, San Marcos, Torreon, COAH, México',
            location: {lat: 25.545969, lng: -103.411697},
            website: 'http://www.thermaltek.com.mx/'
          },
          {
            name: 'Climas del Norte',
            address: 'Venustiano Carranza 909, Villa de Fuente, Piedras Negras, COAH, México',
            location: {lat: 28.667329, lng: -100.535137},
            website: 'http://www.climasdelnorte.com/'
          },
          {
            name: 'Climas del Norte',
            address: 'Armando Treviño 704, Guillen, Piedras Negras, COAH, México',
            location: {lat: 28.678375, lng: -100.561546},
            website: 'http://www.climasdelnorte.com/'
          },
          {
            name: 'MESA - Matriz',
            address: 'Aquiles Serdan 486, Villa Milenio, Ciudad Acuña, COAH, México',
            location: {lat: 29.329339, lng: -100.943337},
           
          },
          {
            name: 'Chavitar - Norte',
            address: 'Tecnológico 5708, Francisco I Madero, Chihuahua, CHIH, México',
            location: {lat: 28.673937, lng: -106.097782},
            website: 'http://www.chavitar.com.mx/'
          },
          {
            name: 'Chavitar - Sur',
            address: 'Melchor Ocampo 2604, Pacifico, Chihuahua, CHIH, México',
            location: {lat: 28.626763, lng: -106.071725},
            website: 'http://www.chavitar.com.mx/'
          },
          {
            name: 'Totaline - Tijuana',
            address: 'Via Rapida 14321, Los Santos, Tijuana, BC, México',
            location: {lat: 32.512454, lng: -116.967628},
            website: 'http://www.totaline.com.mx/'
          },
          {
            name: 'Totaline - Hermosillo',
            address: 'Nayarit 139, San Benito, Hermosillo, SON, México',
            location: {lat: 29.09423, lng: -110.961945},
            website: 'http://www.totaline.com.mx/'
          },
          {
            name: 'Totaline - Tampico',
            address: 'Universidad 601, Los Pinos, TAMPS, México',
            location: {lat: 22.259041, lng: -97.863079},
            website: 'http://www.totaline.com.mx/'
          },
          {
            name: 'Totaline - Veracruz',
            address: 'Miguel Aleman, 2549, Adolfo López Mateos, VER, México',
            location: {lat: 19.163704, lng: -96.138064},
            website: 'http://www.totaline.com.mx/'
          },
          {
            name: 'Totaline - Merida',
            address: 'Calle 19 352b, Pedregales de Lindavista, Mérida, YUC, México',
            location: {lat: 21.012412, lng: -89.654214},
            website: 'http://www.totaline.com.mx/'
          },
          {
            name: 'Totaline - Cancún',
            address: 'Andres Quintana Roo 45, Super Manzana 46, Cancún, QR, México',
            location: {lat: 21.149756, lng: -86.850176},
            website: 'http://www.totaline.com.mx/'
          },
          {
            name: 'Totaline - Guadalajara',
            address: 'Vallarta 1125, Americana, Guadalajara, JAL, México',
            location: {lat: 20.684802, lng: -103.361921},
            website: 'http://www.totaline.com.mx/'
          },
          {
            name: 'Totaline - Tlalnepantla',
            address: 'Primero de Mayo 77, Industrial, Tlalnepantla, MEX, México',
            location: {lat: 19.541595, lng: -99.209793},
            website: 'http://www.totaline.com.mx/'
          },
          {
            name: 'Totaline - Monterrey',
            address: 'Gonzalitos 515, Mitras Norte, Monterrey, NL, México',
            location: {lat: 25.712225, lng: -100.349908},
            website: 'http://www.totaline.com.mx/'
          },
          {
            name: 'Cool & Parts - Gonzalitos',
            address: 'Gonzalitos 240, Vista Hermosa, Monterrey, NL, México',
            location: {lat: 25.691274, lng: -100.351796},
            website: 'http://www.coolpartsmexico.com/'
          },
          {
            name: 'Cool & Parts - Morones Prieto',
            address: '5 de mayo 1013, La Huerta, Guadalupe, NL, México',
            location: {lat: 25.686557, lng: -100.247857},
            website: 'http://www.coolpartsmexico.com/'
          },
          {
            name: 'Cool & Parts - Puerto Vallarta',
            address: 'Rio Colorado 269, Agua Azul, Puerto Vallarta, JAL, México',
            location: {lat: 20.621539, lng: -105.22177},
            website: 'http://www.coolpartsmexico.com/'
          },
          {
            name: 'Cool & Parts - Mazatlan',
            address: 'Ejercito Mexicano 2205, Palos Prietos, Mazatlán, SIN, México',
            location: {lat: 23.227342, lng: -106.421668},
            website: 'http://www.coolpartsmexico.com/'
          },
          {
            name: 'Cool & Parts - Veracruz',
            address: 'Nicolas Bravo 403, Centro, Veracruz, VER, México',
            location: {lat: 19.197999, lng: -96.142586},
            website: 'http://www.coolpartsmexico.com/'
          },
          {
            name: 'AC Frio - Matriz',
            address: 'Pino Suarez 223, Centro, Monterrey, NL, México',
            location: {lat: 25.67536, lng: -100.319266},
            website: 'http://www.acfrio.com.mx/'
          },
          {
            name: 'Aislacon - Matriz',
            address: 'Carretera a Agua Fria 1000 B, Agua Fria, Apodaca, NL, México',
            location: {lat: 25.786802, lng: -100.161334},
            website: 'http://www.grupoaislacon.com.mx/'
          },
          {
            name: 'Comercializadora Silbotti - Matriz',
            address: 'Quinta Avenida 107B, Cumbres 1er sector, Monterrey, NL, México',
            location: {lat: 25.696478, lng: -100.359165},
           
          },
          {
            name: 'Daikin - Monterrey',
            address: 'Eugenio Garza Sada 1205, Country, Monterrey, NL, México',
            location: {lat: 25.633675, lng: -100.280534},
            website: 'http://www.daikin.com.mx/'
          },
          {
            name: 'Gama - Matriz',
            address: 'Chapultepec 615, Caracol, Monterrey, NL, México',
            location: {lat: 25.667228, lng: -100.293187},
            website: 'http://gamarefacciones.com/'
          },
          {
            name: 'Lessa - Matriz',
            address: 'Carretera Nacional S/N, Centro, Allende, NL, México',
            location: {lat: 25.270544, lng: -100.011995},
            website: 'http://lesadistribuidora.com/'
          },
          {
            name: 'Lessa - Monterrey',
            address: 'Los Angeles 730-5, Del Norte, Monterrey, NL, México',
            location: {lat: 25.714053, lng: -100.299002},
            website: 'http://lesadistribuidora.com/'
          },
          {
            name: 'Multiequipos - Matriz',
            address: 'Actopan 2856, Mitras Norte, Monterrey, NL, México',
            location: {lat: 25.691625, lng: -100.341973},
            website: 'http://multiequipos.com.mx/'
          },
          {
            name: 'Multiequipos - Colón',
            address: 'Cristobal Colón 2013, Centro, Monterrey, NL, México',
            location: {lat: 25.681788, lng: -100.275213},
            website: 'http://multiequipos.com.mx/'
          },
          {
            name: 'Multiequipos - Sta. Catarina',
            address: 'Gustavo Diaz Ordaz 110-6, Protexa, Santa Catarina, NL, México',
            location: {lat: 25.674661, lng: -100.428523},
            website: 'http://multiequipos.com.mx/'
          },
          {
            name: 'Proensa - Matriz',
            address: 'Washington 1234, Centro, Monterrey, NL, México',
            location: {lat: 25.673733, lng: -100.301494},
            website: 'http://www.proyectosyequipos.com.mx/'
          },
          {
            name: 'Refrigeración y Repuestos - Matriz',
            address: 'Quinta Avenida 1306, Zimix, Santa Catarina, NL, México',
            location: {lat: 25.685591, lng: -100.493041},
           
          },
          {
            name: 'Refrigeración y Repuestos - Centro',
            address: 'Isaac Garza 1027, Centro, Monterrey, NL, México',
            location: {lat: 25.679751, lng: -100.304565},
           
          },
          {
            name: 'Refrigeracion NR - Matriz',
            address: 'Rafael Platon Sanchez 1314, Terminal, Monterrey, NL, México',
            location: {lat: 25.685892, lng: -100.301424},
            website: 'http://gruponr.com/'
          },
          {
            name: 'Solquem - Matriz',
            address: 'Rigo Tovar 266, Control 3, Matamoros, TAMPS, México',
            location: {lat: 25.875373, lng: -97.528069},
            
          },
          {
            name: 'Tecsir - Matriz',
            address: 'Topo Chico 500, Parque Industrial Kronos, Apodaca, NL, México',
            location: {lat: 25.772639, lng: -100.162997},
            website: 'http://tecsir.com/'
          },
          {
            name: 'Thermopartes - Matriz',
            address: 'Lazaro Cardenas 2380, Del Fresno, Guadalajara, JAL, México',
            location: {lat: 20.658851, lng: -103.379306},
            website: 'http://www.thermopartes.com.mx/'
          },
          {
            name: 'Clirsa - Matriz',
            address: 'Felipe Berriozabal 538, Pedro J Mendez, Ciudad Victoria, TAMPS, México',
            location: {lat: 23.742319, lng: -99.155551},
            website: 'http://www.clirsa.com/'
          },
          {
            name: 'Clirsa - Gaviotas',
            address: 'Felipe Berriozabal 1443, Del Norte, Ciudad Victoria, TAMPS, México',
            location: {lat: 23.740434, lng: -99.138975},
            website: 'http://www.clirsa.com/'
          },
          {
            name: 'Clirsa - Tampico',
            address: 'Hidalgo 3602, Flores, Tampico, TAMPS, México',
            location: {lat: 22.247447, lng: -97.873926},
            website: 'http://www.clirsa.com/'
          },
          {
            name: 'Totaline - Reynosa',
            address: 'Occidental 1240, Longoria, Reynosa, TAMPS, México',
            location: {lat: 26.076351, lng: -98.2908187},
            website: 'http://www.totaline.com.mx/'
          },
          {
            name: 'Totaline - Matamoros',
            address: 'Lauro Villar 186, Modelo, Matamoros, TAMPS, México',
            location: {lat: 25.872124, lng: -97.491007},
            website: 'http://www.totaline.com.mx/'
          },
          {
            name: 'Totaline - Puebla',
            address: 'Bvd. Hermanos Serdán 752, San Rafael Oriente, Puebla, PUE, México',
            location: {lat: 19.087333, lng: -98.228928},
            website: 'http://totalinepuebla.com/'
          },
          {
            name: 'Valtierra - Matriz',
            address: 'Ramon Treviño 1639, Terminal, Monterrey, NL, México',
            location: {lat: 25.685018, lng: -100.298119},
            
          },
          {
            name: 'Jomar - Moderna',
            address: 'Antonio I Villareal 64530, Moderna, Monterrey, NL, México',
            location: {lat: 25.696437, lng: -100.277705},
            website: 'http://grupojomar.com/'
          },
          {
            name: 'Jomar - Penitencieria',
            address: 'Penitencieria 2210, Simon Bolivar, Monterrey, NL, México',
            location: {lat: 25.72366, lng: -100.338328},
            website: 'http://grupojomar.com/'
          },
          {
            name: 'Jomar - Colibri',
            address: 'Benito Juárez 4003, Benito Juárez, Guadalupe, NL, México',
            location: {lat: 25.675039, lng: -100.21622},
            website: 'http://grupojomar.com/'
          },
          {
            name: 'Jomar - HSM',
            address: 'Acapulco 1500, Zozaya, Guadalupe, NL, México',
            location: {lat: 25.722071, lng: -100.202012},
            website: 'http://grupojomar.com/'
          },
          {
            name: 'Jomar - Sta. Catarina',
            address: 'Cuauhtemoc 1001, Unidad Nacional, Santa Catarina, NL, México',
            location: {lat: 25.679267, lng: -100.439645},
            website: 'http://grupojomar.com/'
          },
          {
            name: 'Jomar - Escobedo',
            address: 'Raul Salinas L302, Hacienda del Topo, General Escobedo, NL, México',
            location: {lat: 25.792313, lng: -100.329645},
            website: 'http://grupojomar.com/'
          },
          {
            name: 'Jomar - Allende',
            address: 'Carretera Nacional 226, Seccion Juárez, Allende, NL, México',
            location: {lat: 25.285502, lng: -100.030303},
            website: 'http://grupojomar.com/'
          },
          {
            name: 'Jomar - Mayoreo',
            address: '20 de octubre 2380, Jardines de la Moderna, Monterrey, NL, México',
            location: {lat: 25.694306, lng: -100.280291},
            website: 'http://grupojomar.com/'
          },
          {
            name: 'Jomar - Venustiano',
            address: 'Venustiano Carranza 1101, Centro, Monterrey, NL, México',
            location: {lat: 25.687044, lng: -100.330033},
            website: 'http://grupojomar.com/'
          },
          {
            name: 'Jomar - Solidaridad',
            address: 'Cabezada 10301, Barrio Aztlan, Monterrey, NL, México',
            location: {lat: 25.77642, lng: -100.383023},
            website: 'http://grupojomar.com/'
          },
          {
            name: 'Jomar - Concordia',
            address: 'Concordia 532A, Bosque Real 3er Sector, Apodaca, NL, México',
            location: {lat: 25.784677, lng: -100.241626},
            website: 'http://grupojomar.com/'
          },
          {
            name: 'Jomar - Reynosa Longoria',
            address: 'Rio Mante 1901, Prolongacion Longoria, Reynosa, TAMPS, México',
            location: {lat: 26.070622, lng: -98.294656},
            website: 'http://grupojomar.com/'
          },
          {
            name: 'Jomar - Reynosa Juárez',
            address: 'Carretera a San Fernando 1715, Carretera a San Fernando, Plaza Juárez, Reynosa, TAMPS, México',
            location: {lat: 26.026846, lng: -98.278751},
            website: 'http://grupojomar.com/'
          },
          {
            name: 'Jomar - Reynosa Libramiento',
            address: 'Libramiento Mty-Matamoros KM 6, Renacimiento, Reynosa, TAMPS, México',
            location: {lat: 26.044915, lng: -98.314769},
            website: 'http://grupojomar.com/'
          },
          {
            name: 'Jomar - Rio Bravo Madero',
            address: 'Francisco I Madero 315, Rio Bravo, Rio Bravo, TAMPS, México',
            location: {lat: 25.987417, lng: -98.099946},
            website: 'http://grupojomar.com/'
          },
           {
            name: 'Jomar - Miguel Aleman',
            address: 'Boulevard Miguel Aleman KM 1, Hersilia, Miguel Aleman, TAMPS, México',
            location: {lat: 26.396662, lng: -99.041572},
            
          },
          {
            name: 'Jomar - Morelos',
            address: 'Boulevard Morelos 100, Los Rodriguez, Reynosa, TAMPS, México',
            location: {lat: 26.068739, lng: -98.288913},
            
          },
          {
            name: 'Jomar - La Joya',
            address: 'La Joya S/N, La Joya, Reynosa, TAMPS, México',
            location: {lat: 26.016876, lng: -98.24343},
            
          },
          {
            name: 'Jomar - Rio Bravo 2',
            address: 'Brecha 209 417, 1 de Mayo, Rio Bravo, TAMPS, México',
            location: {lat: 25.979632, lng: -98.080943},
            
          },
          {
            name: 'Jomar - Laredo San Rafael',
            address: 'Cesar López de Lara 2460, San Rafael, Nuevo Laredo, TAMPS, México',
            location: {lat: 27.47699, lng: -99.517637},
           
          },
          {
            name: 'Jomar - Laredo Hipodromo',
            address: 'Iturbide 6516, Hipodromo, Nuevo Laredo, TAMPS, México',
            location: {lat: 27.479186, lng: -99.540638},
            
          },
          {
            name: 'Jomar - Guadalajara',
            address: 'Calzada del ejercito 1475, Quinta Velarde, Guadalajara, JAL, México',
            location: {lat: 20.652774, lng: -103.336611},
            
          },
          {
            name: 'Jomar - La Cima',
            address: 'Juan Gil Preciado 1600, Arcos de Zapopan, Zpopan, JAL, México',
            location: {lat: 20.720368, lng: -103.38789},
            
          },
          {
            name: 'Jomar - Matamoros',
            address: 'Pedro Cardenas 8, Buena Vista, Matamoros, TAMPS, México',
            location: {lat: 25.811012, lng: -97.519973},
            
          },
          {
            name: 'Muro - Matriz',
            address: 'Canales 16, Norberto Treviño, Matamoros, TAMPS, México',
            location: {lat: 25.863073, lng: -97.482744},
            
          },
          {
            name: 'Muro - Diagonal',
            address: 'Diagonal Cuauhtemoc 807, Centro, Matamoros, TAMPS, México',
            location: {lat: 25.875672, lng: -97.51317},
            
          },
          {
            name: 'Muro - Sendero',
            address: 'Sendero Nacional 28, Las Mitras, Matamoros, TAMPS, México',
            location: {lat: 25.870328, lng: -97.550109},
            
          },{
            name: 'Muro - Lauro Villar',
            address: 'Lauro Villar 23A, San Juan, Matamoros, TAMPS, México',
            location: {lat: 25.858542, lng: -97.47177},
            
          },{
            name: 'Muro - Acuario',
            address: 'Leyes de Reforma 41, Santa Elena, Matamoros, TAMPS, México',
            location: {lat: 25.862187, lng: -97.522658},
            
          },{
            name: 'Muro - La Luz',
            address: 'Carretera a Victoria KM 7, Ejido La Luz, Matamoros, TAMPS, México',
            location: {lat: 25.790946, lng: -97.52858},
            
          },{
            name: 'Muro - Loma Linda',
            address: 'Boulevard el maestro 130D, Modulo, Reynosa, TAMPS, México',
            location: {lat: 26.069979, lng: -98.305509},
            
          },{
            name: 'Muro - Reynosa Juárez',
            address: 'Carretera San DFO 25, Pedro J Mendez, Reynosa, TAMPS, México',
            location: {lat: 26.018103, lng: -98.277442},
            
          },{
             name: 'Muro -  Pedro Cardenas',
            address: 'Pedro Cardenas 1115, Amado Nervo, Matamoros, TAMPS, México',
            location: {lat: 25.833488, lng: -97.510482},
            
          },{
            name: 'Muro -  Valle Hermoso',
            address: 'Lazaro Cardenas S/N, Villa Satelite, Valle Hermoso, TAMPS, México',
            location: {lat: 25.679163, lng: -97.81602},
            
          },{
            name: 'Muro - Periféfico',
            address: 'Manuel Cavazos Lerma 14, Moderno, Matamoros, TAMPS, México',
            location: {lat: 25.852348, lng: -97.502939},
            
          },{
            name: 'Muro - Doctores',
            address: 'Deandar Amador 619, Doctores, Reynosa, TAMPS, México',
            location: {lat: 26.059114, lng: -98.304125},
            
          },{
            name: 'Muro - Tecnológico',
            address: 'Santo Domingo 354, Lomas del Real, Reynosa, TAMPS, México',
            location: {lat: 26.040572, lng: -98.354129},
            
          },{
            name: 'Muro - San Fernando',
            address: 'Ruiz Cortines 412, Centro, San Fernando, TAMPS, México',
            location: {lat: 24.849992, lng: -98.153941},
            
          },{
            name: 'Muro - Nuevo Progreso',
            address: 'Benito Juarez S/N, Centro, Nuevo Progreso, TAMPS, México',
            location: {lat: 26.045011, lng: -97.952931},
            
          },{
            name: 'Muro - Cumbres',
            address: 'Carretera Monterrey 124, San Antonio, Reynosa, TAMPS, México',
            location: {lat: 26.072326, lng: -98.325499},
            
          },{
            name: 'Proesa Tecnogas - Abastos',
            address: 'Calle Nance 1560, Col. La Aurora, Guadalajara, JAL, México',
            location: {lat: 20.649585, lng: -103.380852},
            
          },{
            name: 'Proesa Tecnogas - Camionera',
            address: 'Calle Dr. R. Michel 200, Col. Ferrocarril, Guadalajara, JAL, México',
            location: {lat: 20.663477, lng: -103.345852},
            
          },{
            name: 'Proesa Tecnogas - Independencia',
            address: 'Calle Independencia 11, Col. Centro, Guadalajara, JAL, México',
            location: {lat: 20.67828, lng: -103.340786},
            
          },{
            name: 'Proesa Tecnogas - San Juan Bosco',
            address: 'Calle Industria 1778, Col. Las Huertas, Guadalajara, JAL, México',
            location: {lat: 20.674809, lng: -103.311321},
            
          },{
            name: 'Proesa Tecnogas - Expo',
            address: 'Calle Paz 76, Col. Mexicaltzingo, Guadalajara, JAL, México',
            location: {lat: 20.668194, lng: -103.347175},
            
          },{
            name: 'Proesa Tecnogas - Santa Tere',
            address: 'Calle Juan Alvarez 1585, Col. Villaseñor, Guadalajara, JAL, México',
            location: {lat: 20.684893, lng: -103.369001},
            
          },{
            name: 'Proesa Tecnogas - Cancún',
            address: 'Av. Yaxchilan 92 Mza.13 S.M. 22 Lote 41 y 43, Centro, Cancún, QR, México',
            location: {lat: 21.158902, lng: -86.828774},
            
          },{
            name: 'Proesa Tecnogas - La Paz 921',
            address: 'Av. La Paz 921, Col. Mexicaltzingo, Guadalajara, JAL, México',
            location: {lat: 20.668536, lng: -103.349074},
            
          },{
            name: 'Proesa Tecnogas - Aguascalientes',
            address: 'Av. De la Convencion Poniente 1707, Col. Miravalle, Aguascalientes, AGS, México',
            location: {lat: 21.89077, lng: -102.312337},
            
          },{
            name: 'Proesa Tecnogas - Acapulco',
            address: 'Av. Cuauhtemoc 134 Local 6 y 7, Centro, Acapulco, GRO, México',
            location: {lat: 16.858774, lng: -99.895998},
            
          },{
            name: 'Proesa Tecnogas - Celaya',
            address: 'Calle Antonio Plaza 238 A-B, Centro, Celaya, GTO, México',
            location: {lat: 20.516737, lng: -100.807784},
            
          },{
            name: 'Proesa Tecnogas - Suc. Guasave',
            address: 'Calle Emiliano Zapata 52, Centro, Guasave, SIN, México',
            location: {lat: 25.564249, lng: -108.462603},
            
          },{
            name: 'Proesa Tecnogas - Suc. P. Vallarta',
            address: 'Calle Peru 1279, Col. 5 de Diciembre, Puerto Vallarta, JAL, México',
            location: {lat: 20.619598, lng: -105.230539},
            
          },{
            name: 'Proesa Tecnogas - Suc. Hermosillo',
            address: 'Calle Veracruz 45, Col. San Benito, Hermosillo, SON, México',
            location: {lat: 29.093776, lng: -110.956495},
            
          },{
            name: 'Proesa Tecnogas - Suc. San Luis Potosí',
            address: 'Calle 16 de Septiembre 133, Centro, San Luis Potosi, SLP, México',
            location: {lat: 23.130826, lng: -101.115183},
            
          },{
            name: 'Proesa Tecnogas - Suc. Obregón',
            address: 'Calle No Reeleción Oriente 322, Centro, Ciudad Obregón, SON, México',
            location: {lat: 27.491646, lng: -109.936978},
            
          },  {
            name: 'Proesa Tecnogas - Suc. Morelia',
            address: 'Calle Virrey de Mendoza 918, Col. Ventura Puente, Morelia, MICH, México',
            location: {lat: 19.694031, lng: -101.190594},
            
          },
          {
            name: 'Proesa Tecnogas - Suc. Zamora',
            address: 'Av. 5 de Mayo 186, Centro, Zamora, MICH, México',
            location: {lat: 19.988817, lng: -102.279985},
            
          },{
            name: 'Proesa Tecnogas - Suc. Culiacán',
            address: 'Blvd. Emiliano Zapata 37, Almada, Culiacán, SIN, México',
            location: {lat: 24.797486, lng: -107.39446},
            
          },{
            name: 'Proesa Tecnogas - Suc. Guaymas',
            address: 'Calzada Garcia Lopez 102 loc-17 Ed. El Corsario, Centro, Guaymas, SON, México',
            location: {lat: 27.925339, lng: -110.904923},
            
          },{
            name: 'Proesa tecnogas - Suc. Mochis',
            address: 'Av. Hidalgo 545, Centro, Los Mochis, SIN, México',
            location: {lat: 25.790188, lng: -108.996685},
            
          },{
            name: 'Proesa Tecnogas - Suc. Navojoa',
            address: 'Calle Talamante, 704, Col. Constitucion, Navojoa, SON, México',
            location: {lat: 27.073819, lng: -109.451092},
            
          },{
            name: 'Proesa Tecnogas - Zihuatanejo',
            address: 'Av. Marina Nacional 71, Centro, Zihuatanejo, GRO, México',
            location: {lat: 17.644079, lng: -101.552977},
            
          },{
            name: 'Proesa Tecnogas - León',
            address: 'Calle 5 de Febrero 792, Centro, León, GTO, México',
            location: {lat: 21.119078, lng: -101.672644},
            
          },{
            name: 'Proesa Tecnogas - Queretaro',
            address: 'Guerrero Sur 89, Centro, Queretaro, QRO, México',
            location: {lat: 20.596572, lng: -100.396626},
            
          },{
            name: 'Proesa Tecnogas - Suc. Alm. General',
            address: 'Periferico Sur 555, Co. Cd. Granja, Zapopan, JAL, México',
            location: {lat: 20.683858, lng: -103.455085},
            
          },{
            name: 'Tecno World',
            address: 'Periferico Sur 555, Co. Cd. Granja, Zapopan, JAL, México',
            location: {lat: 20.683858, lng: -103.455085},
            
          },{
            name: 'Magnetos y Refacciones',
            address: 'Av. La Paz 72, Col. Mexicaltzingo, Guadalajara, JAL, México',
            location: {lat: 20.668167, lng: -103.347302},
            
          },{
            name: 'RefriGas',
            address: 'Calle Veracruz 48, Col. San Benito, Hermosillo, SON, México',
            location: {lat: 29.092745, lng: -110.964383},
            
          },{
            name: 'Refacciones y Equipos California - Suc. La Paz',
            address: 'Calle Morelos 1245-A, Centro, LA Paz, BCS, México',
            location: {lat: 24.157917, lng: -110.301266},
            
          },{
            name: 'Refacciones y Equipos California - Suc. Los Cabos',
            address: 'Calle Morelos 59, Col. Matamoros, Cabo San Lucas, BCS, México',
            location: {lat: 22.891912, lng: -109.914581},
            
          },{
            name: 'Refripartes y Aires S.A. de C.V.',
            address: 'Calle California 1224, Col. Sochiloa, Ciudad Obregón, SON, México',
            location: {lat: 27.478767, lng: -109.945252},
            
          },{
            name: 'FreeO',
            address: 'Periferico Sur 555-B, Col. Ciudad Granja, Zapopan, JAL, México',
            location: {lat: 20.683858, lng: -103.455085},
            
          },{
            name: 'R&R - Tampico Suc. Matriz',
            address: 'Calle Dr. Antonio Matienzo Norte 215, Centro, Tampico, TAMPS, México',
            location: {lat: 22.218902, lng: -97.859417},
            
          },{
            name: 'R&R - Tampico Suc. Norte',
            address: 'Carretera Tampico-Mante 810, Col. Las Americas, Tampico, TAMPS, México',
            location: {lat: 22.306302, lng: -97.881937},
            
          },{
            name: 'R&R - Suc. Veracruz',
            address: 'Azueta 1290, Centro, Veracruz, VER, México',
            location: {lat: 19.187467, lng: -96.134921},
            
          },{
            name: 'R&R - Suc. Cd. Valles',
            address: 'Boulevard Mexico-Laredo Sur 42, Col. Obrera, Ciudad Valles, SLP, México',
            location: {lat: 21.984639, lng: -99.011441},
            
          },{
            name: 'EQUIPSA - La Paz',
            address: 'Melchor Ocampo 907, Col. Centro, La Paz, BCS, México',
            location: {lat: 24.1539, lng: -110.3102},
            
          },{
            name: 'EQUIPSA - Los Cabos',
            address: 'Adolfo López Mateos Local 4, Col. Ejidal, Cabo San Lucas, BCS, México',
            location: {lat: 22.8973, lng: -109.9197},
            
          },{
            name: 'Refrigeracion Robles',
            address: 'Calle Independencia 1408, Col. Independencia, Guadalajara, JAL, México',
            location: {lat: 20.695569, lng: -103.330551},
            
          },{
            name: 'Equipos de Refrigeración RG',
            address: 'Calle Morelos Poniente 462, Col. Jorge Almada, Culiacán, SIN, México',
            location: {lat: 24.800419, lng: -107.397196},
            
          },{
            name: 'Perylsa - Matriz',
            address: 'Calle Revillagigedo 74, Col. Centro, México, CDMX',
            location: {lat: 19.429508, lng: -99.146876},
            
          },{
            name: 'Perylsa - Norte',
            address: 'Calle Norte 78-A, 5127-A, Col. Gertudris Sanchez, México, CDMX',
            location: {lat: 19.461111, lng: -99.104804},
            
          },{
            name: 'Perylsa - Tlalnepantla',
            address: 'Av. Gustavo Baz, 247 Local C, La Loma, Tlalnepantla, MEX, México',
            location: {lat: 19.529824, lng: -99.2079},
            
          },{
            name: 'Perylsa - Coatzacoalcos',
            address: 'Av. Rodriguez Malpica 319, Centro, Coatzacoalcos, VER, México',
            location: {lat: 18.14458, lng: -94.41471},
            
          },{
            name: 'Perylsa - Acapulco',
            address: 'Av. Cuauhtemoc 120 Local A y B, Col. Centro, Acapulco, GRO, México',
            location: {lat: 16.858221, lng: -99.89787},
            
          },{
            name: 'Perylsa - Cancún',
            address: 'Av. Lopez Portillo Mz. 2 Lt. 50 Sm.62, Col. Benito Juarez, Cancún, QR, México',
            location: {lat: 21.168509, lng: -86.837748},
            
          },  {
            name: 'Perylsa - Playa del Carmen',
            address: 'Av. 40 Mz. 117 Lt. 12, Col. Gonzalo Guerrero, Playa del Carmen, QR, México',
            location: {lat: 20.633685, lng: -87.076317},
            
          },
          {
            name: 'Carvi S.A. de C.V. - México',
            address: 'Calle Revillagigedo, 47 Local 6, Centro, México, CDMX',
            location: {lat: 19.431609, lng: -99.146539},
            
          },{
            name: 'Carvi S.A. de C.V. - Cd. Del Carmen',
            address: 'Av. Aviacion 173 Local 1 y 2, Col. Petrolera, Ciudad del Carmen, CAMP, México',
            location: {lat: 18.643561, lng: -91.822347},
            
          },{
            name: 'Relyem - Suc. Santa Cruz Meyehualco',
            address: 'Calle 45 1, Col. Santa Cruz Meyehualco, México, CDMX, México',
            location: {lat: 19.343358, lng: -99.041914},
            
          },{
            name: 'Relyem - Suc. Eje 5',
            address: 'Calle 16 de Marzo de 1861 Mz. 133 Lt. 1410 Local A Eje 5, Col. Leyes de Reforma 3A, México, CDMX, México',
            location: {lat: 19.37693, lng: -99.065439},
            
          },{
            name: 'Relyem - Suc. Xochimilco',
            address: 'Prol. Division del Norte 243 Local A, Col. San Lorenzo La Cebada, México, CDMX, México',
            location: {lat: 19.280214, lng: -99.123533},
            
          },{
            name: 'Ingeniería y Mantenimiento del Soconusco, S.A. de C.V. - Suc. Tapachula',
            address: 'Av. 4ta Sur, Esq. Calle 2da Poniente, Centro, Tapachula, CHIS, México',
            location: {lat: 14.906498, lng: -92.265887},
            
          },{
            name: 'Comercializadora en Sistemas Ambientales, S.A. de C.V.',
            address: 'Calle 15a. Poniente Esq. Calle 5a Norte 575, Centro, Tuxtla Gutiérrez, CHIS, México',
            location: {lat: 16.760702, lng: -93.130035},
            
          },{
            name: 'Ingenieria y Mantenimiento del Soconusco, S.A. de C.V. - Suc. Tuxtla Gutierrez',
            address: 'Calle Oaxaca 7, Col. Santa Maria La Rivera, Tuxtla Gutiérrez, CHIS, México',
            location: {lat: 16.740486, lng: -93.094307},
            
          },{
            name: 'Central de Refacciones 1 - Suc. Distrito Federal',
            address: 'Calle Articulo 123 74-F, Centro, México, CDMX, México',
            location: {lat: 19.432648, lng: -99.146042},
            
          },{
            name: 'Central de Refacciones 2 - Suc. Distrito Federal',
            address: 'Calle Articulo 123 81-E, Centro, México, CDMX, México',
            location: {lat: 19.432789, lng: -99.146619},
            
          },{
            name: 'Comercial Gaby - Suc. Distrito Federal',
            address: 'Calle Articulo 123 65-B, Centro, México, CDMX, México',
            location: {lat: 19.432617, lng: -99.145383},
            
          },{
            name: 'Refacciones Lupita - Suc. Edo. de Mexico',
            address: 'Av. Hank Gonzalez Mz. 44 100-5, Col. Fuentes de Aragon, Ecatepec, MEX, México',
            location: {lat: 19.517696, lng: -99.0344},
            
          },{
            name: 'La Esquina - Suc. Puebla',
            address: 'Calle 2 Norte 1009-E, Centro, Puebla, PUE, México',
            location: {lat: 19.048204, lng: -98.194525},
            
          },{
            name: 'Comercial Gaby - Suc. Puebla',
            address: 'Calle Orientes 217, Centro, Puebla, PUE, México',
            location: {lat: 19.046963, lng: -98.193987},
            
          },{
            name: 'Distribuciones Frilav',
            address: 'Calle Articulo 123 81-A, Centro, México, CDMX, México',
            location: {lat: 19.432808, lng: -99.146334},
            
          },{
            name: 'Refrigerantes Cold',
            address: 'Calle Articulo 123 41-A, Centro, México, CDMX, México',
            location: {lat: 19.432473, lng: -99.144057},
            
          },{
            name: 'Distribuciones Frilav - Suc. 41',
            address: 'Calle Articulo 123 41-B, Centro, México, CDMX, México',
            location: {lat: 19.432472, lng: -99.144042},
            
          },{
            name: 'Genuine - Suc.22',
            address: 'Calle Articulo 123 22-A, Centro, México, CDMX, México',
            location: {lat: 19.432176, lng: -99.142615},
            
          },{
            name: 'Genuine - Suc.20',
            address: 'Calle Articulo 123 20 Local 2, Centro, México, CDMX, México',
            location: {lat: 19.432162, lng: -99.14263},
            
          },{
            name: 'Frilav - Suc. Puebla',
            address: 'Calle 4 Poniente, 908 Local A y B, Centro, Puebla, PUE, México',
            location: {lat: 19.049392, lng: -98.204238},
            
          },{
            name: 'Distribuidora en Partes de Refrigeración y Lavadoras Navarro, S.A. de C.V.',
            address: 'Calle Lagos de Moreno, Mz.66 Lt. 857, Col. San Felipe de Jesus, México, CDMX, México',
            location: {lat: 19.495571, lng: -99.068253},
            
          },{
            name: 'Productos Navarro, S.A de C.V.',
            address: 'Calle Lagos de Moreno, Mz.66 Lt. 861, Col. San Felipe de Jesus, México, CDMX, México',
            location: {lat: 19.495721, lng: -99.068158},
            
          },{
            name: 'Lavadoras y Refrigeración de Neza',
            address: 'Av. Pantitlan 76-A, Col. Palmas, Nezahualcoyotl, MEX, México',
            location: {lat: 19.409221, lng: -99.049297},
            
          },{
            name: 'Distribuidora de Refrigeracion Aire Acondicionado y Lavadoras, S.A. de C.V.',
            address: 'Calle Prados de Framboyan, Mz.18 Lt.12, Col. Prados de Aragon, Nezahualcoyotl, MEX, México',
            location: {lat: 19.468433, lng: -99.043866},
            
          },{
            name: 'Distribuidora de Refrigeracion Aire Acondicionado y Lavadoras, S.A. de C.V. - Suc. Campestre Guadalupana',
            address: 'Calle Campestre Guadalupana 262, Col. Campestre Guadalupna, Nezahualcoyotl, MEX, México',
            location: {lat: 19.484302, lng: -99.062797},
            
          },{
            name: 'Distribuidora de Refrigeracion Aire Acondicionado y Lavadoras, S.A. de C.V. - Suc. San Felipe',
            address: 'Calle Parral, Mz.187 lt.3, Col. San Felipe de Jesus, Gustavo A. Madero, CDMX, México',
            location: {lat: 19.490414, lng: -99.07304},
            
          },{
            name: 'Distribuidora de Refrigeracion Aire Acondicionado y Lavadoras, S.A. de C.V. - Suc. Texcoco',
            address: 'Calle Miguel Negrete, 324-C, Col. San Mateo, Texcoco, MEX, México',
            location: {lat: 19.518699, lng: -98.887075},
            
          },{
            name: 'Refrigeracion, Aire Acondicionado y Lavadoras del Bajio, S.A. de C.V.',
            address: 'Salvatierra Celaya, 957-B, Centro, Salvatierra, GTO, México',
            location: {lat: 20.215674, lng: -100.872864},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. Matriz Aguilar Refacciones',
            address: 'Calle 27, 125 x 24 y 26, Col. Chichen Itza, Mérida, YUC, México',
            location: {lat: 20.960129, lng: -89.58056},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. 62 Aguilar Refacciones',
            address: 'Calle 62, 525 x 65 y 67, Centro, Mérida, YUC, México',
            location: {lat: 20.964726, lng: -89.62479},
            
          },
          {
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. 60 Aguilar Refacciones',
            address: 'Calle 60, 256 Int. 1 x 19 y 21-A, Col. Campestre, Mérida, YUC, México',
            location: {lat: 21.004895, lng: -89.62247},
            
          },
          {
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. 54 Aguilar Refacciones',
            address: 'Calle 54, 522-C Depto 2 x 65 y 67, Col. Centro, Col. Centro, YUC, México',
            location: {lat: 20.963824, lng: -89.619594},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. Campeche Aguilar Refacciones',
            address: 'Calle 53, 54-A x 16, Col. Centro, Campeche, CAMP, México',
            location: {lat: 19.843308, lng: -90.534068},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. Itzaes Aguilar Refacciones',
            address: 'Calle 86 Av. Itzaes, 499 x 59-A y 49, Centro, Mérida, YUC, México',
            location: {lat: 20.97582, lng: -89.640386},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. Garcia Aguilar Refacciones',
            address: 'Calle 7, 180-C Int. 1 x 30 y Circuito Colonias, Col. Garcia Gineres, Mérida, YUC, México',
            location: {lat: 20.994642, lng: -89.636204},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. 67 Aguilar Refacciones',
            address: 'Calle 67, 125 x 24 y 26, Col. Garcia Gineres, Mérida, YUC, México',
            location: {lat: 20.963419, lng: -89.622502},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. Fco. De Montejo Aguilar Refacciones',
            address: 'Calle 48, 303 Int 4 x 13, Fracc. Terranova, Mérida, YUC, México',
            location: {lat: 21.020568, lng: -89.650824},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. Cancun Aguilar Refacciones',
            address: 'Av. Sunyaxchen, Sm. 24 Mz. 26 Lote 47, Col. Benito Juarez, Cancún, QR, México',
            location: {lat: 21.16238, lng: -86.831287},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. Playa Aguilar Refacciones',
            address: 'Av. Constituyentes x Av. 30 y 35, Lote 5 Local 4, Col. Gonzalo Guerrero, Playa del Carmen, QR, México',
            location: {lat: 20.633057, lng: -87.074419},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. Merida',
            address: 'Calle 43, 458 x 52 x 54, Centro, Mérida, YUC, México',
            location: {lat: 20.976954, lng: -89.616145},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. Cancún',
            address: 'Av. Xcaret x Av. Yaxchilan, Super Mz. 20, Mz 17 Lote 106, Centro, QR, México',
            location: {lat: 21.15599, lng: -86.828521},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. Cd. Del Carmen',
            address: 'Calle 47, 129 x 48 y 38-A, Col. Santa Margarita, Ciudad del Carmen, CAMP, México',
            location: {lat: 18.648426, lng: -91.832235},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. Villahermosa',
            address: 'Av. Paseo Usimacinta esq. Agapito Dominguez Canabal, Guayabal, Villahermosa, TAB, México',
            location: {lat: 17.974125, lng: -92.92684},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. Campeche',
            address: 'Av. Gobernadores x 43 112, Santa Ana, Campeche, CAMP, México',
            location: {lat: 19.843248, lng: -90.525578},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. Playa del Carmen',
            address: 'Av. 115 Norte, Mz. 008 Lote 001, Col. Ejido, Playa del Carmen, QR, México',
            location: {lat: 20.639159, lng: -87.093082},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. Cozumel',
            address: 'Av. Pedro Joaquin Coldwell 2, Centro, Conzumel, QR,México',
            location: {lat: 20.506712, lng: -86.945319},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. Coatzacoalcos',
            address: 'Av. Revolucion 1014-B, Centro, Coatzacoalcos, VER, México',
            location: {lat: 18.148281, lng: -94.428669},
            
          },{
            name: 'Refrimart de Mexico, S. A. de C.V. - Suc. Altabrisa',
            address: 'Calle 18, 219 x 13 y 11, Fracc. Residencial Camara de Comercio Plaza Hacienda, Mérida, YUC, México',
            location: {lat: 21.015306, lng: -89.587565},
            
          },{
            name: 'Climas de Sinaloa, S.A. de C.V:',
            address: 'Eje Central Lazaro Cardenas, 155-C, Guerrero, Cuauhtemoc, CDMX, México',
            location: {lat: 19.445085, lng: -99.138636},
            
          },{
            name: 'Servicio y Refacciones para Aire Acondicionado, S.A. de C.V.',
            address: 'Calzada Tlalpan 2410, Avante, Coyoacan, CDMX, México',
            location: {lat: 19.331985, lng: -99.14046},
            
          },{
            name: 'Suministros para Refrigeración, S.A. de C.V.',
            address: 'Emiliano Zapata 309, Tajín, Poza Rica, VER, México',
            location: {lat: 20.531653, lng: -97.463567},
            
          },{
            name: 'Refacciones Alex S.A. de C.V.',
            address: 'Av. Morelos 48, San Cristobal, Ecatepec, MEX, México',
            location: {lat: 19.602651, lng: -99.05082},
            
          },{
            name: 'Reapel Refrigeracion y Equipos, S.A. de C.V.',
            address: 'Calle Tlaloc 2, Tlaxpana, México, CDMX, México',
            location: {lat: 19.443939, lng: -99.167369},
            
          },{
            name: 'NAPA Autoparts Villahermosa',
            address: 'Periferico Carlos Pellicer 669, Miguel Hidalgo, Villahermosa, TAB, México',
            location: {lat: 17.984129, lng: -92.970144},
            
          },{
            name: 'NAPA Autoparts Playa',
            address: 'Carretera Federal Cancun - Tulum, S/N Mza. 73, Playa del Carmen, QR, México',
            location: {lat: 20.637085, lng: -87.076744},
            
          },{
            name: 'NAPA Autoparts Cancun',
            address: 'Av. Jose Lopez Portillo, Smza. 101 Mza 60 Lotes 16, 17 y 18, Cancún, QR, México',
            location: {lat: 21.150382, lng: -86.875235},
            
          },{
            name: 'NAPA Autoparts Periferico',
            address: 'Periferico, Km. 16 Tablaje Catastral 13622, Mérida, YUC, México',
            location: {lat: 20.947913, lng: -89.572048},
            
          },{
            name: 'NAPA Autoparts Dorada',
            address: 'Calle 50, 134 x 21, Col. Miguel Hidalgo, Mérida, YUC, México',
            location: {lat: 20.99164, lng: -89.65186},
            
          },{
            name: 'NAPA Autoparts Fiesta',
            address: 'Calle 17, 194 x 18 Local 6, Col. Mexico Oriente, Mérida, YUC, México',
            location: {lat: 21.002782, lng: -89.603242},
            
          },{
            name: 'NAPA Autoparts Matriz',
            address: 'Calle 81-A, 645 x calle 100, Col. Sambula, Mérida, YUC, México',
            location: {lat: 20.949721, lng: -89.649751},
            
          },{
            name: 'NAPA Autoparts Chiburna',
            address: 'Calle 20, 111-H, Col. Chuburna de Hidalgo, Mérida, YUC, México',
            location: {lat: 21.00772, lng: -89.631132},
            
          },{
            name: 'AirPlus Matriz',
            address: 'Av. Francisco I. Madero 700-A, Centro, Villahermosa, TAB, México',
            location: {lat: 17.992209, lng: -92.916712},
            
          },{
            name: 'AirPlus Juan Escutia',
            address: 'Calle Juan Escutia 429, Local 3, Col. Lindavista, Villahermosa, TAB, México',
            location: {lat: 17.992245, lng: -92.944756},
            
          },{
            name: 'FriHogar',
            address: 'Calle Vicente Guerrero Sur 51, Rosales, Culiacán, SIN, México',
            location: {lat: 24.805751, lng: -107.385286},
            
          },{
            name: 'Refrigerantes Tlalnepantla S.A. de C.V. - Matriz',
            address: 'Avenida Gustavo Baz 114, Bellavista, Tlalnepantla, MEX, México',
            location: {lat: 19.515664, lng: -99.215077},
            
          },{
            name: 'Refrigerantes Tlalnepantla S.A. de C.V. - Galeana',
            address: 'Hermenegildo Galeana 14, Tlalnemex, MEX, México',
            location: {lat: 19.530637, lng: -99.206975},
            
          },{
            name: 'Refrigerantes Tlalnepantla S.A. de C.V. - Zaragoza',
            address: 'Calzada Ignacio Zaragoza 415, Valentín Gómez Farias, Cd. de México, CDMX, México',
            location: {lat: 25.615558, lng: -100.337894},
            
          },{
            name: 'Refrigerantes Cuautitlán - Centro Urbano',
            address: 'Londres 3, Centro Urbano, Cuautitlán Izcalli, MEX, México',
            location: {lat: 19.652953, lng: -99.208644},
            
          },{
            name: 'Refrigerantes Cuautitlán - Cerrito',
            address: 'Calzada Guadalupe, El Cerrito, Cuautitlán, MEX, México',
            location: {lat: 19.66802, lng: -99.193188},
            
          },{
            name: 'Refrigerantes Cuautitlán - Morales',
            address: '20 de Noviembre 310, Los Morales, Cuautitlán, MEX, México',
            location: {lat: 19.675368, lng: -99.182541},
            
          },{
            name: 'Refrigerantes Naucalpan S.A. de C.V.',
            address: 'Av. Gustavo Baz Norte 113, Colonia Centro Naucalpan, Naucalpan de Juárez, MEX, México',
            location: {lat: 19.475534, lng: -99.233254},
            
          },{
            name: 'Refrigerantes Guadalajara S.A. de C.V.',
            address: 'Calle Mexicaltzingo 1540, Americana, Guadalajara, JAL, México',
            location: {lat: 20.668611, lng: -103.359944},
            
          },{
            name: 'Resurtidora de Aire y Calefacción - Matriz',
            address: 'Doctor Andrade 63, Doctores, México, CDMX, México',
            location: {lat: 19.421374, lng: -99.145512},
            
          },{
            name: 'Resurtidora de Aire y Calefacción -Tlalpan',
            address: 'Calzada de Tlalpan 2356, Avante, Coyoacán, CDMX, México',
            location: {lat: 19.333746, lng: -99.140939},
            
          },{
            name: 'Resurtidora de Aire y Calefacción - Cuernavaca',
            address: 'José María Morelos 126 Las Palmas, Cuernavaca, MOR, México',
            location: {lat: 18.909119, lng: -99.233175},
            
          },{
            name: 'Resurtidora de Aire y Calefacción - Monterrey',
            address: 'Avenida de las Torres 333, Miguel Alemán, San Nicolas de los Garza, NL, México',
            location: {lat: 25.715595, lng: -100.219846},
            
          },{
            name: 'Resurtidora de Aire y Calefacción - Puerto Vallarta',
            address: 'Océano Atlántico 102, Palmar de Aramara, Puerto Vallarta, JAL, México',
            location: {lat: 20.654811, lng: -105.233379},
            
          },{
            name: 'Resurtidora de Aire y Calefacción - Cancún',
            address: 'Avenida Chichen Itzá, Lote 104, Local 3, Manzana 9, Cancún, QR, México',
            location: {lat: 21.167283, lng: -86.83372},
            
          },{
            name: 'Resurtidora de Aire y Calefacción -Acapulco',
            address: 'Avenida Farallón 194, Fraccionamiento Farallón, Acapulco, GRO, México',
            location: {lat: 16.868088, lng: -99.869387},
            
          },{
            name: 'Totaline - Querétaro',
            address: '5 de Febrero 205, Querétaro, QRO, México',
            location: {lat: 20.589437, lng: -100.413594},
            
          },{
            name: 'Totaline - León',
            address: 'Boulevard Maríano Escobedo 4307, Jardines de Jerez, León, GTO, México',
            location: {lat: 21.10404, lng: -101.639666},
            
          },{
            name: 'Totaline - Acapulco',
            address: 'Juan Rodríguez Cabrillo 1, Hornos, Acapulco, GRO, México',
            location: {lat: 16.858733, lng: 99.892595},
            
          },{
            name: 'Totaline - Ixtapa',
            address: 'Av. Marina Nacional 59, Centro, Zihuatanejo, GRO, México',
            location: {lat: 17.642487, lng: -101.552282},
            
          },{
            name: 'Totaline - Vallejo',
            address: 'Av. Río Consulado 1460, Vallejo, Gustavo A. Madero, CDMX, México',
            location: {lat: 19.466513, lng: -99.135047},
            
          },{
            name: 'Climas Coatzacoalcos',
            address: 'Av. H. Rodriguez Malpica, 501, María de la Piedad, Coatzacoalcos, VER, México',
            location: {lat: 18.144098, lng: -94.417455},
            
          },{
            name: 'Tiendas Ryse - Torres Landa 1',
            address: 'Blvd. Torres Landa 2307, Santa María del Granero, León, GTO, México',
            location: {lat: 21.100552, lng: -101.672922},
            
          },{
            name: 'Tiendas Ryse - Morelos',
            address: 'Blvd. Morelos 1204, El condado plus, León, GTO, México',
            location: {lat: 21.143421, lng: -101.678541},
            
          },{
            name: 'Tiendas Ryse - Mega Refacciones',
            address: 'Blvd. Torres Landa 901, Prados Verdes, León, GTO, México',
            location: {lat: 21.105118, lng: -101.70199},
            
          },{
            name: 'Tiendas Ryse - Hidalgo',
            address: 'Blvd. Hidalgo 1039, La Florida, León, GTO, México',
            location: {lat: 21.145636, lng: -101.672976},
            
          },{
            name: 'Tiendas Ryse - Centro',
            address: 'Pino Suárez 301, Centro, León, GTO, México',
            location: {lat: 21.119433, lng: -101.682994},
            
          },{
            name: 'Tiendas Ryse - Aguascalientes Centro',
            address: 'General Guadalupe Victoria 118, Centro, Aguascalientes, AGS, México',
            location: {lat: 21.881892, lng: -102.297549},
            
          },{
            name: 'Tiendas Ryse - Guanajuato',
            address: 'Plaza Pozuelos, Loma Pozuelos, Guanajuato, GTO, México',
            location: {lat: 21.013295, lng: -101.261343},
            
          },{
            name: 'Tiendas Ryse - Lagos',
            address: 'Blvd. Orozco y Jiménez 264, Colinas de San Javier, Lagos de Moreno, JAL, México',
            location: {lat: 21.358913, lng: -101.940911},
            
          },{
            name: 'Tiendas Ryse - San Francisco del Rincón',
            address: 'Josefa Ortíz de Dominguez Sur 208, Centro, San Francisco del Rincón, GTO, México',
            location: {lat: 21.01973, lng: -101.861971},
            
          },{
            name: 'Tiendas Ryse - Torres Landa 2',
            address: 'Blvd. Torres Landa 1321, San Sebastián, León, GTO, México',
            location: {lat: 21.097311, lng: -101.653858},
            
          },{
            name: 'Tiendas Ryse Aguascalientes Independencia',
            address: 'Avenida Independencia 1853, Trojes de Alonso, Aguascalientes, AGS, México',
            location: {lat: 21.922188, lng: -102.297668},
            
          },{
            name: 'Tiendas Ryse - Silao',
            address: 'Avenida la Joya 3, La Joyita, Silao, GTO, México',
            location: {lat: 20.946469, lng: -101.418609},
            
          },{
            name: 'Tiendas Ryse - Tec',
            address: 'Avenida Tecnológico 627, Fovissste, Celaya, GTO, México',
            location: {lat: 20.5417, lng: -100.820392},
            
          },{
            name: 'Ryse - Celaya Manuel Doblado',
            address: 'Manuel Doblado 102, Centro, Celaya, GTO, México',
            location: {lat: 20.52348, lng: -100.815735},
            
          },{
            name: 'Ryse - Celaya Avenida Plaza 1',
            address: 'Antonio Plaza 200, El Vergel, Celaya, GTO, México',
            location: {lat: 20.516301, lng: -100.807689},
            
          },{
            name: 'Ryse - Celaya Avenida Plaza 2',
            address: 'Antonio Plaza 221, Resurrección, Celaya, GTO, México',
            location: {lat: 20.51776, lng: -100.807796},
            
          },{
            name: 'Ryse Cortazar',
            address: 'Ignacio Aldama 206, Centro, Cortazar, GTO, México',
            location: {lat: 20.484595, lng: -100.965123},
            
          },{
            name: 'Ryse Ramón Corona',
            address: 'Ramón Corona 137, Centro, Irapuato, GTO, México',
            location: {lat: 20.672139, lng: -101.348021},
            
          },{
            name: 'Ryse Irapuato Revolución',
            address: 'Avenida Revolución y Manuel Doblado, Centro, Irapuato, GTO, México',
            location: {lat: 20.676495, lng: -101.345259},
            
          },{
            name: 'Ryse Mega Refacciones',
            address: 'Prolongación Guerrero 2371, Prolongación Guerrero, Irapuato, GTO, México',
            location: {lat: 20.692092, lng: -101.359091},
            
          },{
            name: 'Ryse Irapuato Central de Abastos',
            address: 'Av Mariano J. García 1315, El Refugio, Irapuato, GTO, México',
            location: {lat: 20.652763, lng: -101.354258},
            
          },{
            name: 'Ryse Espigas',
            address: 'Blvd. Díaz Ordaz, 3184, Las Reynas, Irapuato, GTO, México',
            location: {lat: 20.689914, lng: -101.355512},
            
          },{
            name: 'Ryse Guerrero',
            address: 'Guerrero esquina Morelos, Centro, Irapuato, GTO, México',
            location: {lat: 20.677895, lng: -101.349392},
            
          },{
            name: 'Ryse Zamora',
            address: 'Avenida 5 de Mayo 195, Centro, Zamora, MICH, México',
            location: {lat: 19.988935, lng: -102.279916},
            
          },{
            name: 'Ryse La Piedad',
            address: 'Calle Blvd. Lázaro Cárdenas 36, Centro, La Piedad de Cavadas, MICH, México',
            location: {lat: 20.342274, lng: -102.021066},
            
          },{
            name: 'Ryse Querétaro',
            address: 'Manuel Gamez 2, Centro, Querétaro, QRO, México',
            location: {lat: 20.586726, lng: -100.386039},
            
          },{
            name: 'CIA Universal de Refrigeración',
            address: 'Alameda Juan Pablo II 134, San Salvador, El Salvador',
            location: {lat: 13.702385, lng: -89.190144},
            
          },{
            name: 'Distribuidora Granada',
            address: '25 AVE SUR 765, Cucucmacayan, San Salvador, El Salvador',
            location: {lat: 13.694220, lng: -89.205325},
            
          },{
            name: 'Pochy Ieromazzo',
            address: 'AV. SAN MARTIN 196, En La Fé, Santo Domingo, Rep. Dominicana',
            location: {lat: 18.484666, lng: -69.915823},
            
          },{
            name: 'Almen Internacional - Bodega',
            address: 'Vía tocumen aeropuerto comercial park bodega # 6, 6, Panamá',
            location: {lat: 9.064271, lng: -79.419153},
            
          },{
            name: 'Cabarria IQA SAS - Bodega',
            address: 'KM 6 Vía Cajicá - Zipaquira, centro empresarial el cortijo, Cajicá, Colombia',
            location: {lat: 4.972254, lng: -74.003354},
            
          },{
            name: 'INCOPAR SAS',
            address: 'CRA 43 # 43 56, Barranquilla, Colombia',
            location: {lat: 10.982723, lng: -74.784643},
            
          },{
            name: 'CIA Universal de Refrigeración ',
            address: '5a avenida 7-01 zona 9, Guatemala',
            location: {lat: 14.612983, lng: -90.52082},
            
          },{
            name: 'CIA de Importaciones y Representaciones',
            address: 'Calle 14 de Septiembre Edificio P del H 1-1/2 cuadra al este, Nicaragua',
            location: {lat: 12.141921, lng: -86.247971},
            
          },{
            name: 'RCR Refrigeración - Suc. Matriz',
            address: 'Calle de la Plata 75, Hermosillo, SON, México',
            location: {lat: 29.025642, lng: -110.911281},
            
          },{
            name: 'MIDWAY LTD. - Matriz',
            address: 'Center Road 580, Spanish Lockout, Cayo District',
            location: {lat: 17.260418, lng: -89.004733},
            
          },{
            name: 'RCR - Tijuana',
            address: 'Juventino Rosas 1195, Los Alamos, Tijuana, BC, México',
            location: {lat: 32.54657, lng: -116.935517},
            
          },{
            name: 'RCR - Hermosillo',
            address: 'De La Plata 75, Parque Industrial, Hermosillo, SON, México',
            location: {lat: 29.025337, lng: -110.911109},
            
          },{
            name: 'RCR - Culiacán',
            address: 'Blvd. Diamante 1833, Stase, Culiacán, SIN, México',
            location: {lat: 24.828234, lng: -107.412202},
            
          },{
            name: 'RCR - Monterrey',
            address: 'Altamisa 140-19, Barrio Estrella, Monterrey, NL, México',
            location: {lat: 25.766598, lng: -100.405970},
            
          },{
            name: 'RCR - Bajío',
            address: 'San Antonio 216, Col. Brisas del Campo, León, GTO, México',
            location: {lat: 21.111114, lng: -101.621005},
            
          },{
            name: 'IMARE - Guadalajara',
            address: 'Calzada Juan Pablo II, Santa María 1452, Santa Maria, Guadalajara, JAL, México',
            location: {lat: 20.689814, lng: -103.315188},
            
          },{
            name: 'EQUIPSA - Cancún',
            address: 'Av. José López Portillo, Super Manzana 063, Zona Urbana, Cancún, QR, México',
            location: {lat: 21.16936, lng: -86.836164},
            
          },{
            name: 'EQUIPSA - Monterrey',
            address: 'Av. Bernardo Reyes 3742, Popular, Monterrey, NL, México',
            location: {lat: 25.710293, lng: -100.328345},
            
          },{
            name: 'EQUIPSA - Constitución',
            address: 'Blvd. Gral. Olachea, Col. Centro, Cd. Constitución, BC, México',
            location: {lat: 25.0299, lng: -111.6718},
            
          },{
            name: 'EQUIPSA - Mérida',
            address: 'Diagonal 15, 98, Yucatán, Mérida, YUC, México',
            location: {lat: 20.9971, lng: -89.62756},
            
          },{
            name: 'CONSEJO TECNOGAS - Barragan',
            address: 'Av.Manuel Barragán 520, Res. Anahuac, San Nicolás de los Garza, NL, México',
            location: {lat: 25.739731, lng: -100.313751},
            
          },{
            name: 'CONSEJO TECNOGAS - Monclova',
            address: 'Av. De La Fuente 149, Centro, Monclova, COAH, México',
            location: {lat: 26.903363, lng: -101.425464},
            
          },{
            name: 'CONSEJO TECNOGAS - Madero',
            address: 'Av. Francisco I. Madero 776, Centro, Monterrey, NL, México',
            location: {lat: 25.684457, lng: -100.319850},
            
          },{
            name: 'CONSEJO TECNOGAS - Reynosa',
            address: 'Heron Ramirez 1122, Centro, Reynosa, TAMPS, México',
            location: {lat: 26.076469, lng: -98.288311},
            
          },{
            name: 'CONSEJO TECNOGAS - Saltillo',
            address: 'Fragua 1154, Centro, Saltillo, COAH, México',
            location: {lat: 25.429964, lng: -100.994595},
            
          },{
            name: 'CONSEJO TECNOGAS - Cuernavaca',
            address: 'Av. Plan de Ayala 511, Lomas del Mirador, Cuernavaca, MOR, México',
            location: {lat: 18.924195, lng: -99.216571},
            
          },{
            name: 'MAGNETOS Y REFACCIONES - Matriz',
            address: 'Av. La Paz 80, Mexicaltzingo, Guadalajara, JAL, México',
            location: {lat: 20.668175, lng: -103.347395},
            
          },{
            name: 'MAGNETOS Y REFACCIONES -Tlaquepaque',
            address: 'Av. Revolución 80, Meseros, Tlaquepaque, JAL, México',
            location: {lat: 20.640344, lng: -103.301425},
            
          },{
            name: 'MAGNETOS Y REFACCIONES - FRI',
            address: 'Héroe de Nacozari 214, Ferrocarril, Guadalajara, JAL, México',
            location: {lat: 20.647572, lng: -103.352206},
            
          },{
            name: 'MAGNETOS Y REFACCIONES - Puebla',
            address: '2 Poniente, 2909-1, Amor, Puebla, PUE, México',
            location: {lat: 19.056731, lng: -98.219565},
            
          },{
            name: 'MAGNETOS Y REFACCIONES - Querétaro',
            address: 'Prolongacion Corregidora 176, Fracc. la Estrella, Querétaro, QRO, México',
            location: {lat: 20.589963, lng: -100.390474},
            
          },{
            name: 'MAGNETOS Y REFACCIONES - Mazatlán',
            address: 'Juan Carrasco 312, Montuosa, Mazatlán, SIN, México',
            location: {lat: 23.212456, lng: -106.416161},
            
          },{
            name: 'MAGNETOS Y REFACCIONES - Monterrey',
            address: 'Albino Espinosa 955, Centro, Monterrey, NL, México',
            location: {lat: 25.652572, lng: -100.277929},
            
          },{
            name: 'MAGNETOS Y REFACCIONES - León',
            address: 'Calzada Tepeyac, 228-A, León Moderno, León, GTO, México',
            location: {lat: 21.107369, lng: -101.671406},
            
          },{
            name: 'MAGNETOS Y REFACCIONES - Culiacán',
            address: 'Gabino Vázquez, 840-1c, Fracc. Los Pinos, Culiacán, SIN, México',
            location: {lat: 24.795392, lng: -107.406844},
            
          },{
            name: 'Refrigeración MATHEOS - Progreso',
            address: 'Progreso, 25, Salina Cruz, OAX, México',
            location: {lat: 16.178616, lng: -95.19472},
            
          },{
            name: 'Refrigeración MATHEOS - Juchitán',
            address: '5 de Septiembre, Local 1, Juchitán OAX, México',
            location: {lat: 16.442924, lng: -95.022050},
            
          },{
            name: 'Refrigeración MATHEOS - Huatulco',
            address: 'Macuil, 210, Bahías de Huatulco, OAX, México',
            location: {lat: 15.766809, lng: -96.138418},
            
          },{
            name: 'EQUIPSA - Hermosillo',
            address: 'Cuernavaca casi esquina con Veracruz, 55, San Benito, Hermosillo, SON, México',
            location: {lat: 29.092774, lng: -110.962287},
            
          },{
            name: 'EQUIPSA - Obregón',
            address: 'Blvd. Rodolfo Elías Calles, 1005-A, Sochiloa, Ciudad Obregón, SON, México',
            location: {lat: 27.075734, lng: -109.444274},
            
          },{
            name: 'EQUIPSA - Navojoa',
            address: 'Pequeira, Local 3, Centro, Navojoa, SON, México',
            location: {lat: 27.075734, lng: -109.444274},
            
          },
        ]
      })

     var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    icon: iconBase + 'icono.png'
  });
    </script>

</body>
</html>
