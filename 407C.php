<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>GENETRON® 407C | Gases Refrigerantes | Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63501207-1', 'auto');
    ga('send', 'pageview');
  </script>

</head>
<body>

  <?php include('header.php'); ?>

  <section class="grad pt140"><div class="img-bg" data-bg="assets/img/stages/detalle.jpg"></div>

    <article>

      <div class="row mb50"><div class="col10 off1 bco">

        <h1>GASES REFRIGERANTES</h1>
        <h3>Creamos productos hechos a la medida de tus necesidades. </h3>
        <a href="javascript:history.back();"><span class="icon icon-atras h1"></span> <span class="h2 ml10">Volver</span></a>

      </div></div>

      <div class="content"><div class="row">

        <div class="col3 sm-col4">
          <div class="mb20"><img src="assets/img/renders/407C.png" alt="GENETRON® 407C"></div>
          <a href="assets/files/30HDS.pdf" class="btn bg-dorado full mb10" target="_blank"><span class="icon icon-seguridad"></span> HOJA DE SEGURIDAD</a>
          <a href="assets/files/30FT.pdf" target="_blank" class="btn bg-gris full mb10"><span class="icon icon-nube"></span> FICHA TÉCNICA</a>
        </div>

        <div class="col8 off1">

          <h1><b>GENETRON® 407C</b></h1>
          <h4 class="mb40">

            El Genetron® 407C es un reemplazo a largo plazo para el R-22 el cual se usa comúnmente en aplicaciones de aire acondicionado.

          </h4>

          <table class="mb40">
            <thead>
              <tr>
               <th>Sección</th>
                <th>Especificación</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td data-th="Sección">ASHRAE #</td>
                <td data-th="Especificación">R-407C</td>
              </tr>
              <tr>
                <td data-th="Sección">Clase</td>
                <td data-th="Especificación">HFC</td>
              </tr>
              <tr>
                <td data-th="Sección">Tipo de Refrigerante</td>
                <td data-th="Especificación">Mezcla Zeotrópica</td>
              </tr>
              <tr>
                <td data-th="Sección">Lubricante típico</td>
                <td data-th="Especificación">Polioléster</td>
              </tr>
              <tr>
                 <td data-th="Sección">Contenido o Fórmula</td>
                <td data-th="Especificación">R-32 (23%), R125 (25%), R134a (52%)</td>                
              </tr>
              <!--<tr>                <td data-th="Sección">Substitutes</td>
                <td data-th="Especificación">HCFC-22</td>               
              </tr>
              <tr>
                <td data-th="Sección">Glide</td>
                <td data-th="Especificación">Near zero glide</td>
              </tr>-->
              <tr>
                <td data-th="Sección">Descripción</td>
                <td data-th="Especificación">Mezcla</td>
              </tr>
              <tr>
                <td data-th="Sección">Notas</td>
                <td data-th="Especificación">Capacidad inferior al R-22, puede requerir rediseño de equipo instalado.</td>
              </tr>
              <tr>
                <td data-th="Sección">Presentaciones Disponibles</td>
                <td data-th="Especificación">Cilindro de 11.3 kg</td>
              </tr>
            </tbody>
          </table>

          <p class="h4">

            Es una excelente opción para reemplazar al R-22, pues funciona de manera similar al 22 y se puede utilizar para reconversión en equipos de aire acondicionado existentes. El R-407C también se puede utilizar para remplazar el R-502 en aplicaciones de temperatura media con temperaturas de evaporación arriba de los 20°F (-7°C).
           <br><br>
           
           Los usos más comunes para el R-407C es en aires acondicionados residenciales o comerciales, bombas de calor y aplicaciones existentes de temperatura media.

           </p>

        </div>

      </div></div>

      <div class="content bge3"><div class="row">

        <div class="col3 h85 bbot xs-ha xs-left xs-mb20" align="right"><h3 class="gris6"><br class="hide-sm"><strong>PRODUCTOS RELACIONADOS</strong></h3></div>

        <div class="col3 xs-mb10 rel-prod" data-href="22.php">
          <div class="h85 bbot row" align="center"><div class="col8 off2 sm-col12"><img src="assets/img/renders/22.png"class="sm-col6 sm-off3" alt=""></div></div>
          <p align="center"><b>Genetron® 22</b></p>
        </div>

        <div class="col3 sm-mb10 rel-prod" data-href="AZ20.php">
          <div class="h85 bbot row" align="center"><div class="col8 off2 sm-col12"><img src="assets/img/renders/AZ20.png" class="sm-col6 sm-off3" alt=""></div></div>
          <p align="center"><b>Genetron® AZ 20® (R-410A)</b></p>
        </div>

        <div class="col3 rel-prod" data-href="407F.php">
          <div class="h85 bbot row" align="center"><div class="col8 off2 sm-col12"><img src="assets/img/renders/407F.png" class="sm-col6 sm-off3" alt=""></div></div>
          <p align="center"><b>Genetron® PERFORMAX LT® (R-407F)</b></p>
        </div>

      </div></div>

    </article>

  </section>

  <?php include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>

  <script>
    $(document).ready(function(){
      
      $('.rel-prod').click(function(){ window.location.href = $(this).data('href'); }); 
      $('.rel-prod').hover(
        function(){ $(this).find('img').css({ 'width': '135%', 'margin': '-17% 0 0 -17%' }); },
        function(){ $(this).find('img').removeAttr('style'); }
      ); 

    });
  </script>

</body>
</html>
