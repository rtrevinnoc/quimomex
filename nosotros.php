<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>Empresa | Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63501207-1', 'auto');
    ga('send', 'pageview');
  </script>

</head>
<body>

  <?php include('header.php'); ?>

  <section class="stage grad" data-bg="assets/img/stages/nosotres.jpg">
    <article><div class="col8 sm-col10 tbl"><div class="vab">
      <h1 class="bb-lima">QUIMOBÁSICOS, UNA EMPRESA DE CLASE MUNDIAL</h1>
    </div></div></article>
  </section>

  <section class="bgf1" style="    background: url(http://incubo.com.mx/quimobasicos/assets/img/nosotros2.jpg);
    background-position: right;
    background-repeat: no-repeat;">
    <article class="bb-lima pb0" >

      <div class="row tbl">
        <div class="col8 p60">

      <h2 class="lima mb30">ACERCA DE NOSOTROS</h2>
      <p class="h4 mb20">Quimobásicos nace en el año de 1961 en la ciudad de Monterrey como resultado de la visión del Grupo Cydsa en alianza estratégica con Honeywell.</p>
      <p class="h4 mb20">Nuestro enfoque principal es el de la fabricación y comercialización de gases refrigerantes fluorocarbonados bajo la marca Genetron® para consumo Nacional y para las necesidades de Centro, Sudamérica y el Caribe.</p>
      <p class="h4 mb20">Somos el único fabricante de gas refrigerante en México y en nuestra planta de la ciudad de Monterrey usamos la tecnología de procesado de fluorocarbonos de Honeywell. </p>
      <p class="h4">La labor de Quimobásicos y de los refrigerantes Genetron® es consolidar el liderazgo en la industria construido durante estos nuestros primeros 57 años de éxitos mientras contribuimos con soluciones ecológicamente viables a la conservación, el bienestar y el confort necesarios en la vida de las personas.</p>

      </div>
            <div align="center" class="col4 vab"></div>
      </div>

    </article>
  </section>

  <section id="mision" style="background: url(http://incubo.com.mx/quimobasicos/assets/img/nosotros1.jpg);
    background-position: right;
    background-repeat: no-repeat;" >
    <article class="bb-lima pb0" >

      <div class="row tbl">
        <div class="col8 p60">
          <h2 class="lima mb30">NUESTRA META, <br>DAR EL MEJOR SERVICIO AL CLIENTE</h2>
          <p class="h4 mb20">En Quimobásicos, nos esforzamos por ser ese proveedor de servicio integral para todos nuestros clientes alrededor del mundo. Nuestra amplia línea de productos brinda alternativas para casi cualquier aplicación de aire acondicionado y refrigeración, agentes espumantes, limpiadores de precisión, gases esterilizantes, aerosoles y otros usos especiales.</p>

          <p class="h4 mb20">Nuestro compromiso de servicio a clientes incluye satisfacer sus necesidades actuales y futuras, asegurando una transición sin inconvenientes hacia los productos sustitutos de los CFC's. Como parte de ello, brindamos a nuestros consumidores soporte de servicio técnico y de aplicaciones, así como una amplia gama de información complementaria que abarca boletines técnicos, folletos de productos, actualización de las regulaciones gubernamentales y material audiovisual de capacitación.</p>

        </div>
        <div align="center" class="col4 vab"></div>
      </div>

    </article>
  </section>

 




  <section class="bgf1">

    <article class="p60 bb-lima">

      <h2 class="lima mb30">NUESTRA FILOSOFÍA DE VALOR</h2> PROPORCIONAR PRODUCTOS Y SERVICIOS QUE EXCEDAN LAS NECESIDADES Y EXPECTATIVAS DE NUESTROS USUARIOS INTERNOS Y EXTERNOS; A TRAVES DE:<br><br><br>

      <h2 class="lima mb30">VISIÓN</h2>
      <p class="h4 mb40">La visión de Quimobásicos es sencilla y a la vez muy ambiciosa: ser una organización de clase mundial.</p>

      <br>
      <h2 class="lima mb30">MISIÓN</h2>
      <p class="h4 mb20">Proporcionar productos y servicios que excedan las necesidades y expectativas de nuestros usuarios internos y externos; a travez de:</p>
      <ul class="bull mb40 ml40"><b>
        <li>La Innovación y la Mejora Continua de los Procesos.</li>
        <li>El Trabajo en Equipo.</li>
        <li>La Cultura de Seguridad y el Cuidado del Medio Ambiente.</li>
        <li>La Creación de Valor para el Cliente, el Personal, los Accionistas y la Comunidad.</li></b>
      </ul><br><br>
      <h2 class="lima mb30">VALORES</h2>
      <p class="h4 mb20">Los valores en los que cimentamos esta visión y misión como organización son:</p>
      <ul class="bull mb40 ml40">
        <li><b>Orientación al Cliente y al Consumidor Final:</b> El actuar de Quimobásicos está enfocado en mejorar continuamente nuestros productos, procesos y servicios para alcanzar y exceder los requerimientos de nuestros clientes de manera rentable, permitiendo maximizar la rentabilidad de la Organización, el desarrollo humano de nuestro personal y el valor para nuestros accionistas.<br><br>
          A través de nuestras operaciones, buscamos ser la mejor opción para nuestros clientes internos y externos; actuales y potenciales, porque ellos son los destinatarios de nuestro trabajo y la razón de nuestra existencia. <br><br>
De igual manera, en Quimobásicos creemos que debemos trabajar anticipándonos a las necesidades de nuestros clientes y consumidores, cuidando cada detalle con una actitud permanente de superación.

</li>
        <li><b>Trabajo en Equipo:</b> En Quimobásicos estamos convencidos de que la cooperación armónica entre nuestros colaboradores y socios externos actúa como un catalizador de nuevas ideas, promueve el desarrollo de soluciones creativas y el intercambio proactivo de las más variadas propuestas y visiones.<br><br>
Por otro lado, esta colaboración fomenta el respeto por la diversidad, desarrolla las habilidades de liderazgo de nuestros colaboradores y refuerza la comunicación efectiva entre los integrantes de la Organización.<br><br>
Asimismo, el trabajo en equipo coadyuva en el fortalecimiento de nuestra identidad colectiva, el sentido de pertenencia del personal y la lealtad a la Organización.

</li>
        <li><b>Actuación Ética, Transparente y Trascendente:</b> Entre los objetivos primordiales de Quimobásicos, destaca el fortalecimiento de una cultura de trabajo honesto, honorable y comprometido con los más altos estándares de desempeño personal y profesional de cada integrante de nuestra Organización. <br><br>
Para ello, estamos comprometidos como Empresa y como individuos con el cabal cumplimiento de las leyes de las jurisdicciones donde operamos.<br><br>
Igualmente, nos distinguimos por nuestro actuar comprometido con el uso eficiente de recursos institucionales, el desempeño con el más alto profesionalismo de nuestros colaboradores, la protección de la confidencialidad y la comunicación transparente con nuestros grupos de interés. <br><br>
De la misma forma, activamente evitamos cualquier situación que represente o pudiere representar un conflicto entre intereses particulares y los intereses de la Organización, así como la participación en cualquier acto de corrupción, soborno, fraude o encubrimiento.

</li>
        <li><b>Innovación y Mejora Continua:</b> Uno de los pilares del crecimiento de Quimobásicos es el desarrollo de productos y servicios innovadores, así como la implementación de procesos de vanguardia, que cumplan y excedan las expectativas de nuestros clientes y que su vez, generen valor para nuestros distintos grupos de interés<br><br> 
Es por ello que Quimobásicos realiza inversiones en tecnología de punta para asegurar y potenciar nuestra competitividad con el propósito de estar a la altura de una empresa de clase mundial. 
<br><br>
No conformándonos con nuestros logros obtenidos, en Quimobásicos nos exigimos, día con día, seguir siendo cada vez mejores, planteándonos y superando nuevos y mayores retos, sentando así las bases para nuestro futuro y para la competitividad que nos hace trascender como Organización. A fin de asegurar la calidad y eficiencia de nuestras operaciones.<br><br>
En Quimobásicos perseveramos constantemente en la implementación de acciones de mejora continua, así como en el análisis de la satisfacción de nuestros clientes internos y externos. <br><br>
Asimismo, buscamos continuamente perfeccionar nuestros sistemas de gestión de calidad y estamos comprometidos con el más estricto apego a normas y certificaciones tanto industriales como medioambientales.</li>
        <li><b>El Cuidado del Medio Ambiente:</b> Quimobásicos con el compromiso del cuidado del medio ambiente, avanza hacia una mayor sustentabilidad de sus operaciones frente a la sociedad y el medioambiente, minimizando su huella ambiental y apoyando el bienestar de las poblaciones vecinas a sus instalaciones productivas, en un marco de diálogo que ayude a mejorar las condiciones de seguridad para nuestra comunidad.
</li>
        <li><b>Respeto a las Personas:</b> Quimobásicos promueve el Respeto a las Personas: Aceptamos las ideas y diferencias entre las personas así como los derechos y la propiedad de otros.
<br><br>
Aunado a lo anterior, Quimobásicos reprueba cualquier acto de discriminación o falta de respeto a la integridad y dignidad de las personas y se implementan políticas, medidas y mecanismos para evitar estas conductas.
</li>
        
      </ul>

    </article>
  </section>


<section id="">
    <article class="pb0">

      <div class="row tbl">
        <div class="col12 p60">
          <h2 class="lima mb30">POLÍTICAS</h2>
            <div id="bloques"><article>

      <div class="row"><div class="tbl"><CENTER>

        <div class="col4"><div class="home-block xs-bgverde"><div class="cont">
          <span class="btn full bgvd">POLÍTICA<br>DE CALIDAD</span>
          <hr>
          <p>Mejorar  Continuamente  nuestros  Procesos, para Exceder las Necesidades de nuestros Usuarios Internos y Externos, a través de Objetivos enfocados a la Creación de Valor, mediante el Trabajo en Equipo, la Innovación y el cumplimiento de los Estándares contenidos en la Norma ISO 9001:2015.</p>
        </div></div></div>

        <div class="col4"><div class="home-block xs-bgnaranja"><div class="cont">
          <span class="btn full bgvd">POLÍTICA<br>AMBIENTAL</span>
          <hr>
          <p>Mejorar Continuamente nuestros Procesos para hacer un Uso Racional y Eficiente de los Recursos a través de Objetivos que aseguren la Competitividad, la Trascendencia y el Cuidado del Medio Ambiente; Cumpliendo con los estándares establecidos en la Norma ISO 14001:2015, la Legislación Vigente y los Compromisos con la Comunidad.</p>
        </div></div></div>

        <div class="col4"><div class="home-block xs-bgazulmed"><div class="cont">
          <span class="btn full bgvd">POLÍTICA DE<br>SEGURIDAD</span>
          <hr>
          <p>Mejorar continuamente nuestros procesos para: Lograr mediante una Cultura de Prevención; la Integridad física del Personal, de las Instalaciones y de la Comunidad; enfocándonos en la Reducción del Error Humano y en el cumplimiento de la Legislación Vigente.</p>
        </div></div></div></CENTER>

      </div></div>

    </article></div>

        </div>
        
      </div>

    </article>
  </section>






  <section class="bg-lima" style="background-color: #2c313e !important;">


    <article class="p60"><div class="tbl">  

      <!--<div class="col3 xs-center xs-mb20" align="right"><img src="assets/img/icons/globo.png" alt=""></div>-->
          
      <div class="col9 va" align="left"><img src="assets/img/icons/logo.png"><blockquote class="bco">Como parte del Grupo Cydsa, lleva a cabo una importante labor de protección ambiental desarrollando y comercializando productos ecológicamente viables.</blockquote></div>

    </div></article>
  </section>

  <section style="background-color: #2c313e !important;">
    <article>
      <div class="p20" align="center">
      <img src="assets/img/photos/ambiental.png" style="max-height: 300px;"  alt="">
      <img src="assets/img/photos/Enmarcado-1.png" style="max-height: 300px;" alt="">
      <img src="assets/img/photos/semarnat.png" style="max-height: 300px;" alt="">
     
      </div>
    </article>
  </section>

  <?php include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>

  <script>
    $(document).ready(function(){
      <?php if( $_GET['s'] != '' ) : ?>
      $('html, body').stop().animate({ scrollTop: $('#<?php echo $_GET['s']; ?>').offset().top - 130 }, 1000);
      <?php endif; ?>
    });
  </script>

</body>
</html>
