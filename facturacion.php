<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>Sistema de Facturación | Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63501207-1', 'auto');
    ga('send', 'pageview');
  </script>

</head>
<body>

  <?php include('header.php'); ?>

  <section class="grad pt140" data-bg="assets/img/stages/facturacion.jpg">

    <article>

      <div class="row mb50"><div class="col10 off1 bco">

        <h1>FACTURACIÓN PROVEEDORES</h1>
        <a href="javascript:history.back();"><span class="icon icon-atras h1"></span> <span class="h2 ml10">Volver</span></a>

      </div></div>

    </article>

  </section>

  <section class="bgf1">

    <article class="p60">

      <div class="mb30">
        <h2>BIENVENIDO AL SISTEMA DE FACTURACIÓN</h2>
        <p class="h4 slab"><em>EN ESTE APARTADO ENCONTRARÁS TODA LA INFORMACIÓN QUE NECESITAS PARA REVISAR TUS PAGOS PENDIENTES O TUS FACTURAS.</em></p>
        <p class="h4 slab"><em>SI AUN NO CUENTAS CON TU USUARIO Y CONTRASEÑA FAVOR DE COMUNICARTE CON TU CONTACTO DEL DEPARTAMENTO DE COMPRAS.</em></p>
      </div>

      <form action="https://www.cydsa-e.com.mx/cgi-bin/brkpagproqb.sh/homfrmdis010.r" method=POST>
      
      <div class="row"><div class="col6 sm-col8">
      <div class="col6"><input type="Hidden" name=idioma id=idioma value=1></div>
        <div class="row"><div class="tbl mb20">
          <div class="col6 va xs-mb10"><label>USUARIO:</label></div>
          <div class="col6"><input type="text" class="bd-azul" name=user id=user placeholder="Usuario"></div>
        </div></div>

        <div class="row"><div class="tbl mb20">
          <div class="col6 va xs-mb10"><label>CONTRASEÑA:</label></div>
          <div class="col6"><input type="password" class="bd-azul" name=pass id=pass placeholder="Contraseña"></div>
        </div></div>

        <div align="right"><div class="col12"><button type="submit" class="btn bg-azul">ENTRAR</button></div></div>

      </div></div>

      </form>

    </article>

  </section>

  <?php include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>

</body>
</html>
