<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />
  <link type="text/css" rel="stylesheet" href="assets/css/jquery.fancybox.css" />
  <link type="text/css" rel="stylesheet" href="assets/css/slick.css" />
  <link type="text/css" rel="stylesheet" href="assets/css/dropkick.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63501207-1', 'auto');
    ga('send', 'pageview');
  </script>


</head>
<body>

  <?php include('header.php'); ?>

  <section id="home-stage">

    <div id="slider">

      <div class="img-bg" data-bg="assets/img/stages/BANNER1/1-PORTADA_HOME-QUIMOBASICOS_1309x227px-BG.jpg"><article style="font-family: 'gothamregular';">

        <h1 class="bco"><img src="assets/img/stages/BANNER1/1-PORTADA_HOME-QUIMOBASICOS_1309x227px-BODEGON-2.png" id="slide1Overlay" ></h1>

        <h1 id="slide1Header1"><span style="color: #33c3e5;">LA LÍNEA<br>MÁS COMPLETA</span><br><span style="color: white;">DE GASES<br><span style="text-decoration: underline;">RE</span>FRIGERANTES</span></h1>

        <h1 id="slide1Header2">CON LA MEJOR Y MÁS EXTENSA RED DE<br>DISTRIBUIDORES, ¡SIEMPRE CERCA!</h1>

      </article></div>

      <div class="img-bg" data-bg="assets/img/stages/BANNER2.1/2.1-PORTADA_HOME-QUIMOBASICOS_1309x227px-BG.jpg"><article style="font-family: 'gothamregular';">

        <h1 class="bco"><img src="assets/img/stages/BANNER2.1/2.1-PORTADA_HOME-QUIMOBASICOS_1309x227px-RENDERS.png" id="slide2Overlay" ></h1>

        <img src="assets/img/icons/logo.png" id="slide2Logo" >

        <h1 id="slide2Text">Ecologico | Eficiente | Seguro | Confiable</h1>

        <h1 id="slide2Header1"><img src="assets/img/icons/eco_flush.png" id="slide2Header2">1233zd Eco<sup>®</sup> Flush</h1>

        <h1 id="slide2Header3">EL AGENTE LIMPIADOR PRESURIZADO,<br>ULTRA-EFICIENTE DE ÚLTIMA GENERACIÓN<br>Y AMIGABLE CON EL PLANETA</h1>

      </article></div>

      <div class="img-bg" data-bg="assets/img/stages/BANNER2.2/2.2-PORTADA_HOME-QUIMOBASICOS_1309x227px-BG.jpg"><article style="font-family: 'gothamregular';">

        <h1 class="bco"><img src="assets/img/stages/BANNER2.2/2.2-PORTADA_HOME-QUIMOBASICOS_1309x227px-RENDERS.png"  id="slide3Overlay" ></h1>

        <img src="assets/img/icons/logo.png" id="slide3Logo" >

        <h1 id="slide3Header" ><img src="assets/img/icons/solstice.png" id="slide3Icon">Solstice<sup>®</sup> 1234yf</h1>

        <hr id="slide3Separator">

        <h1 id="slide3Text">¡LOS REFRIGERANTES DEL FUTURO HOY!<br>LÍNEA AUTOMOTRIZ DE NUEVA<br>GENERACIÓN, CON GWP MENOR A 1.</h1>

      </article></div>


    </div>

    <div id="bloques"><article>

      <div class="row"><div class="tbl" style="padding-bottom: 16px;">

        <div class="col4"><div class="home-block xs-bgverde"><div class="cont">
          <a href="mercados.php" class="btn full bgvd">MERCADOS</a>
          <hr>
          <p>Somos el proveedor de servicio completo para la industria nacional e internacional.</p>
        </div></div></div>

        <div class="col4"><div class="home-block xs-bgnaranja"><div class="cont">
          <a href="soluciones.php" class="btn full bg-naranja">SOLUCIONES</a>
          <hr>
          <p>Nuestra amplia oferta de soluciones cubre tus necesidades actuales y futuras.</p>
        </div></div></div>

        <div class="col4"><div class="home-block xs-bgazulmed"><div class="cont">
          <a href="productos.php?cat=RC" class="btn full bg-azulmed">PRODUCTOS</a>
          <hr>
          <p>Los gases refrigerantes Genetron® brindan alternativas para casi cualquier aplicación.</p>
        </div></div></div>

      </div></div>

    </article></div>

    <div class="trans-bco" style="background-color: #2c313e !important;"><article>

      <div class="row mt10">

        <div class="col4 hide-sm" align="center"><a href="https://www.youtube.com/playlist?list=PLOlHKkJnlcz6e3ZAxbVDxr2uWXdChdI1t" target="_blank"><img class="block" src="assets/img/photos/youtube01.png" alt="" style="height: 78px;"></a></div>
        <div class="col8 sm-col12 p20 clearfix">

         
          <div class="left"><a href="https://www.youtube.com/playlist?list=PLOlHKkJnlcz6e3ZAxbVDxr2uWXdChdI1t" target="_blank"><p class="h2 azul ls40" style="color: #fff !important;">WEBINARS EN YOUTUBE</p></a></div>
          <div class="right busqueda"><form action="ubica.php" method="post">
            <!-- <input id="edo" type="text" placeholder="Buscar por Estado"> -->
            <!-- <select name="edo" class="drop">
              <option value="AGS">Aguascalientes</option>
              <option value="BC">Baja California</option>
              <option value="BCS">Baja California Sur</option>
              <option value="CAMP">Campeche</option>
              <option value="CHIS">Chiapas</option>
              <option value="CHIH">Chihuahua</option>
              <option value="CDMX">Ciudad de México</option>
              <option value="COAH">Coahuila</option>
              <option value="COL">Colima</option>
              <option value="DGO">Durango</option>
              <option value="GTO">Guanajuato</option>
              <option value="GRO">Guerrero</option>
              <option value="HGO">Hidalgo</option>
              <option value="JAL">Jalisco</option>
              <option value="MEX">Estado de México</option>
              <option value="MICH">Michoacán</option>
              <option value="MOR">Morelos</option>
              <option value="NAY">Nayarit</option>
              <option selected value="NL">Nuevo León</option>
              <option value="OAX">Oaxaca</option>
              <option value="PUE">Puebla</option>
              <option value="QRO">Querétaro</option>
              <option value="QR">Quintana Roo</option>
              <option value="SLP">San Luis Potosí</option>
              <option value="SIN">Sinaloa</option>
              <option value="SON">Sonora</option>
              <option value="TAB">Tabasco</option>
              <option value="TAMPS">Tamaulipas</option>
              <option value="TLAX">Tlaxcala</option>
              <option value="VER">Veracruz</option>
              <option value="YUC">Yucatán</option>
              <option value="ZAC">Zacatecas</option>
            </select> -->
          
          <div class="left"><p class="h2 azul ls40" style="color: #fff !important;"><a href="https://www.youtube.com/playlist?list=PLOlHKkJnlcz6e3ZAxbVDxr2uWXdChdI1t" class="btn full bg-naranja" target="_blank"><span style="color:white !important;">VERLOS AQUÍ</span>&nbsp; &nbsp;<span class="icon icon-zoom"></span></a></p></div> 
           
          </form></div>

        </div>

      </div>

    </article></div>

  </section>

  <section class="img-bg circ" data-bg="assets/img/stages/bloque-home.jpg" style="background-position: center;"><article>

  
    <div class="va-p"><div class="va" align="center">
      <h1 class="bco mb30"><b>APRENDE CON NOSOTROS</b></h1>
      <a href="https://www.youtube.com/embed/videoseries?si=jJERcMasxjNRJwm8&amp;list=PLOlHKkJnlcz6e3ZAxbVDxr2uWXdChdI1t" class="fancybox fancybox.iframe btn bg-azulmed">IR A VIDEOS</a>
    </div></div>

  </article></section>

  <section class="bgf1 p60"><article><div class="tbl">

     <div class="col3 xs-mb20"><div class="prefoot" data-href="premios.php" >
      <div class="img-bg" data-bg="assets/img/bg/premios.jpg"><div class="va-p"><div class="va p10" align="center"><p class="h2 bco"><strong>PREMIOS</strong></p></div></div></div>
      <div class="p20">
        <p class="mb20">Algunos reconocimientos a nuestra labor</p>
        <div align="center"><a href="premios.php" class="btn bg-azul" target="_blank">LEER MÁS</a></div>
      </div>
    </div></div>

    <div class="col3 xs-mb20"><div class="prefoot" data-href="asesoria-tecnica.php" >
      <div class="img-bg" data-bg="assets/img/bg/asesoria.jpg"><div class="va-p"><div class="va p10" align="center"><p class="h2 bco"><strong>ASESORÍA</strong></p></div></div></div>
      <div class="p20">
        <p class="mb20">Solicita el apoyo de nuestros expertos</p>
        <div align="center"><a href="asesoria-tecnica.php" class="btn bg-azul" target="_blank">LEER MÁS</a></div>
      </div>
    </div></div>

    <div class="col3 xs-mb20"><div class="prefoot" data-href="https://blogquimobasicos.com/" target="_blank">
      <div class="img-bg" data-bg="assets/img/bg/noticias.jpg"><div class="va-p"><div class="va p10" align="center"><p class="h2 bco"><strong>NOTICIAS</strong></p></div></div></div>
      <div class="p20">
        <p class="mb20">Consulta nuestros comunicados oficiales</p>
        <div align="center"><a href="https://blogquimobasicos.com/" class="btn bg-azul" target="_blank">LEER MÁS</a></div>
      </div>
    </div></div>

    <div class="col3"><div class="prefoot" data-href="unete.php">
      <div class="img-bg" data-bg="assets/img/bg/empleos.jpg"><div class="va-p"><div class="va p10" align="center"><p class="h2 bco"><strong>EMPLEOS</strong></p></div></div></div>
      <div class="p20">
        <p class="mb20">Oportunidades laborales en Quimobásicos</p>
        <div align="center"><a href="unete.php" class="btn bg-azul" target="_blank">LEER MÁS</a></div>
      </div>
    </div></div>

  </div></article></section>

  <?php include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>
  <script type="text/javascript" src="assets/js/home.js"></script>
  <script type="text/javascript">
    
  </script>



</body>
</html>
