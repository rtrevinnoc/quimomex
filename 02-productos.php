<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>Productos | Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />
  <link type="text/css" rel="stylesheet" href="assets/css/dropkick.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63501207-1', 'auto');
    ga('send', 'pageview');
  </script>

</head>
<body>

  <?php include('header.php'); ?>

  <section class="grad pt140"><div class="img-bg" data-bg="assets/img/stages/refrigerantes.jpg"></div>

    <article>

      <div class="row mb50"><div class="col10 off1 bco">

        <h1>PRODUCTOS</h1>
        <h3>La línea más completa de gases refrigerantes. </h3>
        <a href="javascript:history.back();"><span class="icon icon-atras h1"></span> <span class="h2 ml10">Volver</span></a>

      </div></div>

      <div class="clearfix tabs hide-xs">
        <a data-cat="RC" class="col2 act">REFRIGERACIÓN COMERCIAL</a>
        <a data-cat="RI" class="col2">REFRIGERACIÓN INDUSTRIAL</a>
        <a data-cat="RD" class="col2">REFRIGERACIÓN DOMÉSTICA</a>
        <a data-cat="ACA" class="col2">A/C <br> AUTOMOTRIZ</a>
        <a data-cat="ACR" class="col2">A/C <br> RESIDENCIAL</a>
        <a data-cat="ACC" class="col2">A/C <br> COMERCIAL</a>
      </div>
      <div class="show-xs xs-menu"><select class="drop">
        <option value="RC">REFRIGERACIÓN COMERCIAL</option>
        <option value="RI">REFRIGERACIÓN INDUSTRIAL</option>
        <option value="RD">REFRIGERACIÓN DOMÉSTICA</option>
        <option value="ACA">A/C AUTOMOTRIZ</option>
        <option value="ACR">A/C RESIDENCIAL</option>
        <option value="ACC">A/C COMERCIAL</option>
      </select></div>

      <div class="content">

        <h3><b>PRODUCTOS DE <span id="txt-cat">REFRIGERACIÓN COMERCIAL</span>:</b></h3>

        <div class="row">

        <div class="col3 sm-col4 prod ACR ACA ACC RI RC" data-color="679177" data-href="flush.php">
            <div class="img"><img src="assets/img/renders/flush.png" height="320" width="254" alt="GENETRON® Flush"></div>
            <p align="center"><strong>GENETRON® Flush</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod ACR ACC RI" data-color="679177" data-href="22.php">
            <div class="img"><img src="assets/img/renders/22.png" height="320" width="254" alt="GENETRON® 22"></div>
            <p align="center"><strong>GENETRON® 22</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod ACR ACC" data-color="986777" data-href="AZ20.php">
            <div class="img"><img src="assets/img/renders/AZ20.png" height="320" width="254" alt="GENETRON® AZ 20® (R-410A)"></div>
            <p align="center"><strong>GENETRON® AZ 20® (R-410A)</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod RC RI" data-color="a63c1a" data-href="404A.php">
            <div class="img"><img src="assets/img/renders/404A.png" height="320" width="254" alt="GENETRON® 404A"></div>
            <p align="center"><strong>GENETRON® 404A</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod RC RI" data-color="017a75" data-href="AZ50.php">
            <div class="img"><img src="assets/img/renders/AZ50.png" height="320" width="254" alt="GENETRON® AZ 50®  (R-507A)"></div>
            <p align="center"><strong>GENETRON® AZ 50®  (R-507A)</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod ACA RD RC RI" data-color="6c96a3" data-href="134a.php">
            <div class="img"><img src="assets/img/renders/134a.png" height="320" width="254" alt="GENETRON® 134a"></div>
            <p align="center"><strong>GENETRON® 134a</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod ACA RD RC RI" data-color="02689c" data-href="eco.php">
            <div class="img"><img src="assets/img/renders/eco.png" height="320" width="254" alt="GENETRON® 134aeco"></div>
            <p align="center"><strong>GENETRON® 134a ECO</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod ACA RD" data-color="a16b7c" data-href="MP39.php">
            <div class="img"><img src="assets/img/renders/MP39.png" height="320" width="254" alt="GENETRON® MP39 (R-401A)"></div>
            <p align="center"><strong>GENETRON® MP39 (R-401A)</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod ACR ACC" data-color="6a9904" data-href="422D.php">
            <div class="img"><img src="assets/img/renders/422D.png" height="320" width="254" alt="GENETRON® 422D"></div>
            <p align="center"><strong>GENETRON® 422D</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod RC RI" data-color="90a45b" data-href="407F.php">
            <div class="img"><img src="assets/img/renders/407F.png" height="320" width="254" alt="GENETRON® PERFORMAX LT® (R-407F)"></div>
            <p align="center"><strong>GENETRON® PERFORMAX LT® (R-407F)</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod RC" data-color="771364" data-href="408A.php">
            <div class="img"><img src="assets/img/renders/408.png" height="320" width="254" alt="GENETRON® 408A"></div>
            <p align="center"><strong>GENETRON® 408A</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod RC RI" data-color="9e9865" data-href="HP80.php">
            <div class="img"><img src="assets/img/renders/HP80.png" height="320" width="254" alt="GENETRON® HP 80 (R-402A)"></div>
            <p align="center"><strong>GENETRON® HP 80 (R-402A)</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod RC RI" data-color="4d4814" data-href="HP81.php">
            <div class="img"><img src="assets/img/renders/HP81.png" height="320" width="254" alt="GENETRON® HP 81 (R-402B)"></div>
            <p align="center"><strong>GENETRON® HP 81 (R-402B)</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod RC" data-color="c4a439" data-href="MP66.php">
            <div class="img"><img src="assets/img/renders/MP66.png" height="320" width="254" alt="GENETRON® MP 66 (R-401B)"></div>
            <p align="center"><strong>GENETRON® MP 66 (R-401B)</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod RC" data-color="907248" data-href="409A.php">
            <div class="img"><img src="assets/img/renders/409A.png" height="320" width="254" alt="GENETRON® 409A"></div>
            <p align="center"><strong>GENETRON® 409A</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod ACR ACC" data-color="82441a" data-href="407C.php">
            <div class="img"><img src="assets/img/renders/407C.png" height="320" width="254" alt="GENETRON® 407C"></div>
            <p align="center"><strong>GENETRON® 407C</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod RI" data-color="86898b" data-href="23.php">
            <div class="img"><img src="assets/img/renders/23.png" height="320" width="254" alt="GENETRON® 23"></div>
            <p align="center"><strong>GENETRON® 23</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod RI" data-color="044562" data-href="508B.php">
            <div class="img"><img src="assets/img/renders/508B.png" height="320" width="254" alt="GENETRON® 508B"></div>
            <p align="center"><strong>GENETRON® 508B</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod ACA" data-color="9e9d9d" data-href="1234yf.php">
            <div class="img"><img src="assets/img/renders/1234yf.png" height="320" width="254" alt="SOLSTICE® 1234yf"></div>
            <p align="center"><strong>SOLSTICE® 1234yf</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod RI" data-color="9e9d9d" data-href="1233zd.php">
            <div class="img"><img src="assets/img/renders/1233zd.png" height="320" width="254" alt="SOLSTICE® 1233zd "></div>
            <p align="center"><strong>SOLSTICE® 1233zd </strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod RI" data-color="035645" data-href="124.php">
            <div class="img"><img src="assets/img/renders/124.png" height="320" width="254" alt="GENETRON® 124"></div>
            <p align="center"><strong>GENETRON® 124</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod ACA ACR ACC RD RC RI" data-color="a77a04" data-href="141b.php">
            <div class="img"><img src="assets/img/renders/141b.png" height="320" width="254" alt="GENETRON® 141b"></div>
            <p align="center"><strong>GENETRON® 141b</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod ACC RI" data-color="02689c" data-href="245fa.php">
            <div class="img"><img src="assets/img/renders/245fa.png" height="320" width="254" alt="ENOVATE® (R- 245fa)"></div>
            <p align="center"><strong>ENOVATE® (R- 245fa)</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod ACC" data-color="86898b" data-href="123.php">
            <div class="img"><img src="assets/img/renders/123.png" height="320" width="254" alt="GENETRON® 123"></div>
            <p align="center"><strong>GENETRON® 123</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod RC RI" data-color="004f66" data-href="N40.php">
            <div class="img"><img src="assets/img/renders/N40.png" height="320" width="254" alt="SOLSTICE® N40"></div>
            <p align="center"><strong>SOLSTICE® N40</strong></p>
            <a class="icon icon-mas"></a>
          </div>

          <div class="col3 sm-col4 prod ACC RC RI" data-color="9c9c9c" data-href="1234ze.php">
            <div class="img"><img src="assets/img/renders/1234ze.png" height="320" width="254" alt="SOLSTICE® 1234ze"></div>
            <p align="center"><strong>SOLSTICE® 1234ze</strong></p>
            <a class="icon icon-mas"></a>
          </div>

        </div>

      </div>

    </article>

  </section>

  <?php include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>

  <script type="text/javascript" src="assets/js/dropkick.js"></script>
  <script>
    $(document).ready(function(){
      
      $('.drop').dropkick({ mobile: true, change: function(){
        $('.prod').hide(); $('.prod.' + this.value).fadeIn();
        $('#txt-cat').text($('.dk-selected').text()); 
      }});
      
      $('.prod .img').each(function(){ 
        $(this).css('background-image', 'url(' + $(this).find('img').attr('src') + ')'); 
        $(this).find('img').remove();
      });
      $('.prod').hide().click(function(){ window.location.href = $(this).data('href'); });
      $('.prod.RC').show();
      $('.prod').hover(function(){ $(this).css('background-color', '#' + $(this).data('color')); }, function(){ $(this).removeAttr('style'); });
      
      $('.tabs a').click(function(){ 
        $('.prod').hide(); $('.prod.' + $(this).data('cat')).fadeIn(); 
        $('.tabs .act').removeClass('act'); $(this).addClass('act');
        $('#txt-cat').text($(this).text());
      });

      <?php if( $_GET['cat'] != '' ) :?>
      $('.prod').hide(); $('.prod.<?=$_GET['cat'];?>').fadeIn(); 
      $('.tabs .act').removeClass('act'); $('.tabs a[data-cat="<?=$_GET['cat'];?>"]').addClass('act');
      $('#txt-cat').text($('.tabs a[data-cat="<?=$_GET['cat'];?>"]').text());
      <?php endif; ?>

    });
  </script>

</body>
</html>
