<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>Búsqueda | Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />
  <link type="text/css" rel="stylesheet" href="assets/css/dropkick.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>

</head>
<body>

  <?php include('header.php'); ?>

  <section class="grad pt140" data-bg="assets/img/stages/empresa.jpg">

    <article>

      <div class="row mb50"><div class="col10 off1 bco">

        <h1>BÚSQUEDA</h1>
        <a href="javascript:history.back();"><span class="icon icon-atras h1"></span> <span class="h2 ml10">Volver</span></a>

      </div></div>

    </article>

  </section>

  <section class="bgf1">

    <article class="p60">

      <div class="mb30">
        <h2>LA RED MÁS GRANDE DE DISTRIBUIDORES EN MÉXICO</h2>
        <p class="h4 slab"><em>LOCALIZA TU DISTRIBUIDOR MÁS CERCANO EN LA REPÚBLICA.</em></p>
      </div>

      <div><div class="tbl mb40">

        <div class="col6 vam"><a id="geolocate" class="btn bg-azul"><i class="icon icon-mapa h2 mr10"></i> USAR MI UBICACIÓN ACTUAL</a></div>

        <div class="col6 xs-mb20"><form action="ubicaend.php" method="post">

          <h3 class="bbot mb20">BÚSQUEDA AVANZADA:</h3>
          <div class="row"><div class="tbl mb20">
            <div class="col4 va xs-mb10"><label>PAÍS:</label></div>
            <div class="col8"><select name="pais" class="drop-pais">
              <option value="MX">México</option>
              <option value="Colombia">Colombia</option>
              <option value="El Salvador">El Salvador</option>
              <option value="Guatemala">Guatemala</option>
              <option value="Nicaragua">Nicaragua</option>
              <option value="Rep. Dominicana">República Dominicana</option>
            </select></div>
          </div></div>

          <div class="row"><div class="tbl mb20">
            <div class="col4 va xs-mb10"><label>ESTADO:</label></div>
            <div class="col8"><select name="edo" id="drop-edo">
              <option>Seleccionar</option>
              <option <?php if($_POST["edo"]=='AGS') echo 'selected'; ?> value="AGS">Aguascalientes</option>
              <option <?php if($_POST["edo"]=='BC') echo 'selected'; ?> value="BC">Baja California</option>
              <option <?php if($_POST["edo"]=='BCS') echo 'selected'; ?> value="BCS">Baja California Sur</option>
              <option <?php if($_POST["edo"]=='CAMP') echo 'selected'; ?> value="CAMP">Campeche</option>
              <option <?php if($_POST["edo"]=='CHIS') echo 'selected'; ?> value="CHIS">Chiapas</option>
              <option <?php if($_POST["edo"]=='CHIH') echo 'selected'; ?> value="CHIH">Chihuahua</option>
              <option <?php if($_POST["edo"]=='COAH') echo 'selected'; ?> value="COAH">Coahuila</option>
              <option <?php if($_POST["edo"]=='COL') echo 'selected'; ?> value="COL">Colima</option>
              <option <?php if($_POST["edo"]=='CDMX') echo 'selected'; ?> value="CDMX">Ciudad de México</option>
              <option <?php if($_POST["edo"]=='DGO') echo 'selected'; ?> value="DGO">Durango</option>
              <option <?php if($_POST["edo"]=='GTO') echo 'selected'; ?> value="GTO">Guanajuato</option>
              <option <?php if($_POST["edo"]=='GRO') echo 'selected'; ?> value="GRO">Guerrero</option>
              <option <?php if($_POST["edo"]=='HGO') echo 'selected'; ?> value="HGO">Hidalgo</option>
              <option <?php if($_POST["edo"]=='JAL') echo 'selected'; ?> value="JAL">Jalisco</option>
              <option <?php if($_POST["edo"]=='MEX') echo 'selected'; ?> value="MEX">Estado de México</option>
              <option <?php if($_POST["edo"]=='MICH') echo 'selected'; ?> value="MICH">Michoacán</option>
              <option <?php if($_POST["edo"]=='MOR') echo 'selected'; ?> value="MOR">Morelos</option>
              <option <?php if($_POST["edo"]=='NAY') echo 'selected'; ?> value="NAY">Nayarit</option>
              <option <?php if($_POST["edo"]=='NL') echo 'selected'; ?> value="NL">Nuevo León</option>
              <option <?php if($_POST["edo"]=='OAX') echo 'selected'; ?> value="OAX">Oaxaca</option>
              <option <?php if($_POST["edo"]=='PUE') echo 'selected'; ?> value="PUE">Puebla</option>
              <option <?php if($_POST["edo"]=='QRO') echo 'selected'; ?> value="QRO">Querétaro</option>
              <option <?php if($_POST["edo"]=='QR') echo 'selected'; ?> value="QR">Quintana Roo</option>
              <option <?php if($_POST["edo"]=='SLP') echo 'selected'; ?> value="SLP">San Luis Potosí</option>
              <option <?php if($_POST["edo"]=='SIN') echo 'selected'; ?> value="SIN">Sinaloa</option>
              <option <?php if($_POST["edo"]=='SON') echo 'selected'; ?> value="SON">Sonora</option>
              <option <?php if($_POST["edo"]=='TAB') echo 'selected'; ?> value="TAB">Tabasco</option>
              <option <?php if($_POST["edo"]=='TAMPS') echo 'selected'; ?> value="TAMPS">Tamaulipas</option>
              <option <?php if($_POST["edo"]=='TLAX') echo 'selected'; ?> value="TLAX">Tlaxcala</option>
              <option <?php if($_POST["edo"]=='VER') echo 'selected'; ?> value="VER">Veracruz</option>
              <option <?php if($_POST["edo"]=='YUC') echo 'selected'; ?> value="YUC">Yucatán</option>
              <option <?php if($_POST["edo"]=='ZAC') echo 'selected'; ?> value="ZAC">Zacatecas</option>
            </select></div>
          </div></div>

          <div class="row"><div class="tbl mb20">
            <div class="col4 va xs-mb10"><label>CIUDAD:</label></div>
            <div class="col8"><select name="ciudad" id="drop-cd">
              <option selected value="0">Seleccionar Ciudad</option>
            </select></div>
          </div></div>

          <div align="right" class="xs-center"><button type="submit" class="btn bg-azul">BUSCAR</button></div>

        </form></div>

      </div></div>

      <div id="ubicaciones" class="row">

        <div class="col5"><ul class="ubicaciones">

           <!-- <li data-loc="A">
            <h3><b>TOTALINE MONTERREY</b></h3>
            <h4><b>Serafín Peña # 123 <br>Monterrey, N.L. 64000</b></h4>
            <a href="tel:8112345678" class="btn bg-azul mr10"><span class="icon icon-tel"></span> 8112345678</a>
            <a href="#" target="_blank" class="btn bg-azul mr10">WEBSITE</a>
          </li> -->

        </ul></div>

        <div class="col7"><div id="mapcanvas"></div></div>

      </div>

    </article>

  </section>

  <?php include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>

  <script type="text/javascript" src="assets/js/dropkick.js"></script>
  <script type="text/javascript" src="assets/js/jquery.quick.pagination.min.js"></script>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAVLLFjWjZkQC170Txc6VEPLT2v1RsmOx0"></script>
  
<!-- Reemplazar el script desde aquí --> 
 
  <script type="text/javascript">

  // Set map options
  var markers = [], cont = 'A', i = 0, aux = '';
  var mapOptions = {
    zoom: 14,
    center: {lat: 25.6513438, lng: -100.370588},
    scrollwheel: false,
    styles : [
      { "elementType": "geometry.stroke", "stylers": [ { "color": "#2a70ac" }, { "lightness": "50" } ] },
      { "elementType": "geometry.fill", "stylers": [{ "saturation": -50 } ] } 
    ]
  }
  // Draw map
  var map = new google.maps.Map(document.getElementById("mapcanvas"), mapOptions);

  var url = "https://sheets.googleapis.com/v4/spreadsheets/1KPjPBfMWild7B2TURRNQpjsJZ1VPC_aTRL8KklJgQRg/values/Direcciones!A2:T999?key=AIzaSyAVLLFjWjZkQC170Txc6VEPLT2v1RsmOx0";
  
  $(document).ready(function(){

    <?php if($_POST["edo"]) : ?>
    
    $('#ubicaciones').addClass('open');
    $('html, body').stop().animate({ scrollTop: $('#mapcanvas').offset().top - 150 }, 1000);

    var cds = [], ciudades = [];
    $.getJSON(url, function(data) {
      entry = data.values;
      $(entry).each(function(){ if( this[12] == '<?php echo $_POST["edo"]; ?>' ) cds.push(this[11]); });
      $.each(cds, function(i, el){ if($.inArray(el, ciudades) === -1) ciudades.push(el); });

      if( ciudades.length > 0 ){
        $.each(ciudades, function(i, el){ $('#drop-cd').append('<option>' + el + '</option>'); });
        select.refresh();
        select.disable(false); 
        $('#drop-cd option:eq(0), #dk1-drop-cd .dk-selected').text('Seleccionar Ciudad');
      } else { select.disable(); }
      if(ciudades.length < 1) $('#ubicaciones .col5').prepend('<h3>Lo sentimos, no encontramos distribuidores en el estado seleccionado.</h3>');
    });

    // Get selected state markers
    $.getJSON(url, function(data) {
      entry = data.values;
      $(entry).each(function(){ if( this[0] != '' && this[0] != undefined ){
        var _this = this; 
        var latlng = new google.maps.LatLng(_this[0], _this[1]);
        
        <?php if($_POST["pais"] && $_POST["pais"] != 'MX') : ?>
          if( _this[13] == '<?php echo $_POST["pais"]; ?>' ){ addMarker(latlng,_this,markers,map); map.setCenter(latlng); }
        <?php else : ?>
          <?php if($_POST["edo"]) : ?>
            <?php if($_POST["ciudad"]) : ?>
            if( _this[11] == '<?php echo $_POST["ciudad"]; ?>' ){ addMarker(latlng,_this,markers,map); map.setCenter(latlng); }
            <?php else : ?>
            if( _this[12] == '<?php echo $_POST["edo"]; ?>' ){ addMarker(latlng,_this,markers,map); map.setCenter(latlng); }
            <?php endif; ?>
          <?php endif; ?>
        <?php endif; ?>
      }}); 
      $("ul.ubicaciones").quickPagination({pageSize:"4"});
    });

    // Draw pins
    setMapOnAll(map);

    <?php endif; ?>

    var select = new Dropkick("#drop-cd", { mobile: true }); select.disable();
    // $('.drop-edo').dropkick({ mobile: true, change: function(){
    var selectedo = new Dropkick("#drop-edo", { mobile: true, change: function(){
      
      var val = this.value;
      var x = $('#drop-cd option').length;
      var i = x; if( x > 1 ) while( i > 0 ){ $('#drop-cd option:eq(' + i + ')').remove(); i--; }
      select.refresh();

      var cds = [], ciudades = [];
      $.getJSON(url, function(data) {
        entry = data.values;
        $(entry).each(function(){ if( this[12] == val ) cds.push(this[11]); });
        $.each(cds, function(i, el){ if($.inArray(el, ciudades) === -1) ciudades.push(el); });

        if( ciudades.length > 0 ){
          $.each(ciudades, function(i, el){ $('#drop-cd').append('<option>' + el + '</option>'); });
          select.refresh();
          select.disable(false); 
          $('#drop-cd option:eq(0), #dk1-drop-cd .dk-selected').text('Seleccionar Ciudad');
        } else { select.disable(); }
      });
    
    }});

    $('.drop-pais').dropkick({ mobile: true, change: function(){
      if( this.value != 'MX' ){ select.disable(); selectedo.disable(); } else { selectedo.disable(false); }
    }});
          

    $('#geolocate').click(function(){ 

      var xestado = ''; $('#ubicaciones').addClass('open');
      $('html, body').stop().animate({ scrollTop: $('#mapcanvas').offset().top - 150 }, 1000);
      deleteMarkers();

      // Get location from browser
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };

          // Reverse geocoding -> get user state
          var geocoder = new google.maps.Geocoder();
          geocoder.geocode({'latLng': pos }, function(results, status) { if ( status === 'OK' ) { if (results[2]) {
            
            // Format state to match Spreadsheet
            var edo = results[2].address_components[2].short_name;
            switch(edo){
              case 'Ags.': xestado = 'AGS'; break; 
              case 'B.C.': xestado = 'BC'; break; 
              case 'B.C.S.': xestado = 'BCS'; break; 
              case 'Camp.': xestado = 'CAMP'; break; 
              case 'Chis.': xestado = 'CHIS'; break; 
              case 'Chih.': xestado = 'CHIH'; break; 
              case 'Coah.': xestado = 'COAH'; break; 
              case 'Col.': xestado = 'COL'; break; 
              case 'MX': xestado = 'CDMX'; break; 
              case 'Dgo.': xestado = 'DGO'; break; 
              case 'Méx.': xestado = 'GTO'; break; 
              case 'Gto.': xestado = 'GRO'; break; 
              case 'Gro.': xestado = 'HGO'; break; 
              case 'Hgo.': xestado = 'JAL'; break; 
              case 'Jal.': xestado = 'MEX'; break; 
              case 'Mich.': xestado = 'MICH'; break; 
              case 'Mor.': xestado = 'MOR'; break; 
              case 'Nay.': xestado = 'NAY'; break; 
              case 'N.L.': xestado = 'NL'; break; 
              case 'Oax.': xestado = 'OAX'; break; 
              case 'Pue.': xestado = 'PUE'; break; 
              case 'Qro.': xestado = 'QRO'; break; 
              case 'Q.R.': xestado = 'QR'; break; 
              case 'S.L.P.': xestado = 'SLP'; break; 
              case 'Sin.': xestado = 'SIN'; break; 
              case 'Son.': xestado = 'SON'; break; 
              case 'Tab.': xestado = 'TAB'; break; 
              case 'Tamps.': xestado = 'TAMPS'; break; 
              case 'Tlax.': xestado = 'TLAX'; break; 
              case 'Ver.': xestado = 'VER'; break; 
              case 'Yuc.': xestado = 'YUC'; break; 
              case 'Zac.': xestado = 'ZAC'; break;
            }

            // Get current state markers
            $.getJSON(url, function(data) {
              var entry = data.feed.entry;
              $(entry).each(function(){ if( this[0] != '' && this[0] != undefined ){
                var _this = this;
                var latlng = new google.maps.LatLng(_this[0], _this[1]);
                if( _this[12] === xestado ){ addMarker(latlng,_this,markers,map); }
              }}); 
              $("ul.ubicaciones").quickPagination({pageSize:"4"});
              $('html, body').stop().animate({ scrollTop: $('.ubicaciones').offset().top - 150 }, 1000);
            });

            // Draw pins
            setMapOnAll(map);

          }}});
          
          map.setCenter(pos);

        });
      }

    });

  });

  function addMarker(latlng,_this,markers,map){
    // if(_this[12] != '' && _this[12] != undefined){

      var image = { url: 'assets/img/icons/pin.png' };

      // Infowindow data
      var contentString = '<div id="content">'+
                          '<h3>' + _this[2] + '</h3>'+
                          '<h5>' + _this[3] + '</h5>'+
                          '<div id="bodyContent">'+
                          '<p>' + _this[8] + ' ' + _this[9] + '<br>'
                          + _this[10] + '<br>'
                          + _this[11] + ',' + _this[12] + '<br>'
                          + _this[14] + '<br>';
      if( _this[4] )
        contentString     + 'Tel.: ' + _this[4] + '<br>' + _this[5] + '<br>';
      if( _this[7] )
        contentString     +='Website: <a target="_blank" href="' + _this[7] + '">' + _this[7] + '</a><br>';
      if( _this[6] )
        contentString     +='E-mail: ' + _this[6] + '<br>';
      contentString       +='</p><br><p><a target="_blank" href="http://maps.google.com/maps?q=' + latlng + '">Ver en Google Maps</a></p></div></div>';
      
      var infowindow = new google.maps.InfoWindow({ content: contentString });
      
      // Create marker
      var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        icon: image,
        animation: google.maps.Animation.DROP
      });
      marker.addListener('click', function() { infowindow.open(map, marker); });
      markers.push(marker);

      // Add location to sidebar list data-loc="' + (i+1) + '"
      var li = '<li data-markerid="' + i + '" ><h3><b>' + _this[2] + '</b></h3><h4><b>' + _this[8] + ' ' + _this[9] + ' <br>';
      if ( _this[11] ) li += _this[11] + ', ';
      if ( _this[13] == 'MX' ) li +=  _this[12] + ' ' + _this[14] + '';
      if ( _this[13] != 'MX' ) li +=  _this[13];
      li += '</b></h4>';
      if ( _this[4] ) li += '<a href="tel:' + _this[4] + '" class="btn bg-azul mr10 mb10"><span class="icon icon-tel"></span> ' + _this[4] + '</a>';
      if ( _this[7] ) li += '<a href="' + _this[7] + '" target="_blank" class="btn bg-azul mr10 mb10">WEBSITE</a>';
      if ( _this[6] ) li += '<a href="mailto:' + _this[6] + '" class="btn bg-azul mr10">E-MAIL</a>';
      li += '</li>';

      i++; cont = String.fromCharCode(cont.charCodeAt(0) + 1);
      $('.ubicaciones').append(li);
      $('.ubicaciones li').click(function () { google.maps.event.trigger(markers[$(this).data('markerid')], 'click'); });

    // }
  }

  function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }

  function clearMarkers() {
    setMapOnAll(null);
  }

  function deleteMarkers() {
    clearMarkers();
    markers = [], cont = 'A', i = 0;
    $('.ubicaciones').empty();
  }
</script>

<!-- Reemplazar el script hasta aquí --> 

</body>
</html>
