<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Google tag (gtag.js) -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-50YRXBKSDQ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-50YRXBKSDQ');
  </script>
  <link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" /> 
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="Content-Language" content="es-MX" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <meta property="og:title" content="Quimobásicos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="http://www.quimobasicos.com/" />
  <meta property="og:image" content="" />

  <meta name="title" content="Quimobásicos" /> 
  <meta name="author" content="Quimobásicos" />
  <meta name="copyright" content="Quimobásicos" />
  <meta name="Language" content="Spanish" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="robots" content="all | index | follow" />
  <meta name="description" content="Quimobásicos" />
  <meta name="keywords" content="Quimobásicos" />

  <title>Premios | Quimobásicos</title>
  
  <link type="text/plain" rel="author" href="humans.txt" />

  <link type="text/css" rel="stylesheet" href="assets/css/ui.css" />
  <link type="text/css" rel="stylesheet" href="assets/css/dropkick.css" />

  <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63501207-1', 'auto');
    ga('send', 'pageview');
  </script>

</head>
<body>

  <?php include('header.php'); ?>

  <section class="stage grad" data-bg="assets/img/stages/premios.jpg">
    <article><div class="tbl"><div class="vab">
      <h1 class="bb-verde">PREMIOS</h1>
    </div></div></article>
  </section>

  <section class="bgf1"><article class="p40">
    <p class="h3 mb30">La labor de Quimobásicos ha sido consistentemente reconocida por su elevado nivel de desempeño tanto por clientes, como por organizaciones gubernamentales mexicanas e internacionales.</p>
    <p class="h3">A continuación enumeramos algunos de los premios y reconocimientos más recientes de nuestra organización:</p>
  </article></section>

  <section>
    <article class="p60">

      <div class="mb30">
        <h2 class="verdemed">ISO 9001 y 14001, Bureau Veritas</h2>
      </div>

      <p class="h4 mb20">Ambas certificaciones han sido refrendadas recientemente y son prueba tanto de nuestro compromiso en la entrega de calidad así como por el compromiso con el medio ambiente.</p>
      <p class="h4 mb20">La certificación ISO 9001:2015 indica la exitosa implementación de nuestro Sistema de Gestión de Calidad (SGC) y de cuidado de nuestros procesos para la entrega de valor de forma consistente.</p>
      <p class="h4 mb40">En el caso de la certificación ISO 14001:2015 este es un reconociendo al compromiso con el manejo apropiado de la relación nuestro Sistema de Gestión Ambiental (SGA); una de las principales prioridades de Quimobásicos.</p>

      <div style="display: flex; justify-content: center;">
        <div align="center" class="col4"><img class="bd-azul" src="assets/img/photos/QUIMOBÁSICOS-ISO 9001 2022.jpg" alt=""></div>

        <div align="center" class="col4"><img class="bd-azul" src="assets/img/photos/Certificado-ISO 14001 Marzo 2023.jpg" alt=""></div>
      </div>

    </article>
  </section>

  <section>
    <article class="bb-lima p60"><div class="row">

      <div class="col7">
        <div class="mb30">
          <h2 class="verdeosc">Premio Carrier 2018 a la Excelencia como Proveedor. Séptimo año consecutivo</h2>
        </div>

        <p class="h4 mb20">En el año 2018 Quimobásicos recibió de manos de Carrier de México S.A. de C.V., subsidiaria de United Technologies, el Reconocimiento al alto desempeño como proveedor dentro del proceso de manufactura de Equipos Originales. </p>
        <p class="h4 mb20">Este galardón, el cual nos honra recibir por séptimo año consecutivo, reconoce el desempeño y confiabilidad de nuestra empresa como proveedor de las necesidades en materia de producción y manufactura, volúmenes, tiempos de entrega, calidad y asistencia técnica con los más altos estándares a nivel global. </p>
        <p class="h4">Este reconocimiento se une a los reconocimientos similares recibidos por nuestro alto desempeño durante los períodos que comprenden del año 2011 al 2018.</p>
      </div>
      <div class="col5" align="center"><img src="assets/img/photos/carrier.jpg" alt=""></div>

    </div></article>
  </section>

  <section class="bgf1">
    <article class="bb-verde p60"><div class="row">

      <div class="col7">
        <div class="mb30">
          <h2 class="verdemed">Premio Rheem al Mejor Proveedor de Materias Primas 2014, División Química</h2>
        </div>

        <p class="h4 mb20">Durante el año 2015 Quimobásicos recibió de Rheem Manufacturing Co., el Premio que reconoce al desempeño superior de nuestra empresa como proveedor dentro de sus procesos de manufactura de Equipos Originales. </p>
        <p class="h4">Este premio reconoce el desempeño y confiabilidad de nuestra empresa como proveedor de las necesidades en materia de producción y manufactura, haciendo especial énfasis en Servicio al Cliente y Soporte sobresalientes.</p>
      </div>
      <div class="col5" align="center"><img src="assets/img/photos/rheem.png" alt=""></div>

    </div></article>
  </section>

  <section>
    <article class="bb-verde p60">

      <div class="mb30">
        <h2 class="verdemed">Reconocimiento a la Excelencia Ambiental otorgado por la PROFEPA</h2>
      </div>

      <p class="h4 mb20">Quimobásicos recibe durante el mes de Junio del año 2015 este reconocimiento el cual le refrenda su compromiso, desempeño y esfuerzo en materia de sustentabilidad y cuidado del medio ambiente.</p>
      <p class="h4 mb20">En el evento especial llevado a cabo en el Parque Bicentenaro de la Ciudad de México el día 10 de Junio del 2015, el Ing. Tomás González Sada, Presidente del Consejo de Administración de Grupo Cydsa, recibió a nombre de Quimobásicos el galardón que reconoce la labor de la compañía en aspectos de eficiencia en desempeño ambiental, compromiso con la preservación del medio ambiente así como la responsabilidad social de las empresas con la preservación del entorno ecológico Mexicano. </p>
      <p class="h4 mb40">El Certificado de Excelencia Ambiental 2014 fue recibido directamente de manos del entonces Secretario de Medio Ambiente y Recursos Naturales, el Ing. Juan José Guerra Abud, y constituye un hito para nuestra organización pues con el en nuestras vitrinas somos la primera empresa dentro de la industria química Mexicana en recibirlo en dos ocasiones.</p>

      <div class="row">
        <div class="col4" align="center"><img src="assets/img/photos/ambiental.png" class="bd-azul" alt=""></div>
        <div class="col4" align="center"><img src="assets/img/photos/premios02.jpg" class="bd-azul" alt=""></div>
        <div class="col4" align="center"><img src="assets/img/photos/premios03.jpg" class="bd-azul" alt=""></div>
      </div>

    </article>
  </section>

  <section class="bgf1">
    <article class="bb-lima p60">

      <div class="mb30">
        <h2 class="verdeosc">Reconocimiento UNIDO y PROFEPA 2016. Contribución al Manejo y Destrucción de Sustancias Agotadoras del Ozono</h2>
      </div>

      <div class="row">

        <div class="col8 sm-col12">
          <p class="h4 mb20">El día martes 26 de enero del año en curso se llevó a cabo el evento denominado ‘Cierre de la 1ª Fase del proyecto de Manejo y destrucción de sustancias agotadoras de la capa de ozono (SAO) residuales en México’. Este evento tuvo a bien realizarse en la planta de destrucción de <strong>Quimobásicos®</strong> en la ciudad de Monterrey, Nuevo León; lugar donde se realizó la destrucción de dichos residuos.</p>
          <p class="h4 mb20">El evento tuvo como objetivo difundir los resultados de la implementación del “Proyecto de Manejo y Destrucción de SAO residuales”, este es un parteaguas en temas de cumplimiento a los compromisos de cuidado al medio ambiente y climáticos pues anuncia oficialmente que en nuestro país contamos con la capacidad para disponer de manera ambientalmente responsable de las sustancias agotadoras de la capa de ozono.</p>
          <p class="h4 mb20">El reconocimiento a Quimobásicos se dá por el desarrollo y utilización de la tecnología usada en nuestra Unidad de Destrucción de Residuos Agotadores de la Capa de Ozono la cual utiliza de tecnología de arco de plasma. El proceso de destrucción a través de arco de plasma es una tecnología que ha sido probada y aprobada a nivel internacional para la correcta disposición de estas sustancias debido a que presenta las mejores ventajas ambientales dada su prácticamente nula emisión de contaminantes a la atmósfera.</p>
          <p class="h4 mb20">A través de las actividades realizadas en el marco del proyecto en esta primera fase se han obtenido co-beneficios en términos climáticos equivalentes a evitar la emisión de 512 mil toneladas de CO2 equivalente.</p>
          <p class="h4 mb20">El proyecto lo gestionó la Secretaría del Medio Ambiente y Recursos Naturales (SEMARNAT) con el apoyo de la Organización de las Naciones Unidas para el Desarrollo Industrial (ONUDI) y sirvió de marco para la visita a México de funcionarios de este organismo así como representantes de los gobiernos de países donantes al Fondo Multilateral del Protocolo de Montreal, quienes muy atentamente hicieron entrega del reconocimiento al Ing. Tomás G. Sada, Director del Grupo Cydsa. </p>
          <p class="h4 mb40">Con este premio se confirma el apoyo y compromiso de Quimobásicos para atender a las necesidades de los diferentes sectores industriales tales como: refrigeración, aire acondicionado y farmacéutico; además de que se contribuye a mejorar la gestión para la correcta disposición de este tipo de materiales.</p>
        </div>

        <div class="col4 sm-col12" align="center">
          <img src="assets/img/photos/premios04.jpg" alt="" class="mb20 bd-azul">
          <img src="assets/img/photos/Enmarcado-1.png" alt="" class="bd-azul">
        </div>

      </div>

    </article>
  </section>

  <?php include('footer.php'); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="assets/js/site.js"></script>

  <script type="text/javascript" src="assets/js/dropkick.js"></script>
  <script>
    $(document).ready(function(){
      $('.tabs.t-verde a').click(function(){ $('.tabs a').removeClass('act'); $(this).addClass('act'); $('.tab-cont').hide(); $($(this).attr('href')).show(); return false; });
      $('.t-mercados a').click(function(){ $('html, body').stop().animate({ scrollTop: $($(this).attr('href')).offset().top }, 1000); return false; });
      $('.tab-cont:eq(0)').show();

      $('.drop').dropkick({ mobile: true, change: function(){
        $('html, body').stop().animate({ scrollTop: $('#' + this.value).offset().top }, 1000); 
      }});
    });
  </script>

</body>
</html>
